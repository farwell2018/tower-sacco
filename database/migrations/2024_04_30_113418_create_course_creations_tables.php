<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseCreationsTables extends Migration
{
    public function up()
    {
        Schema::create('course_creations', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);

            $table->string('course_id')->unique();
            $table->string('display_name')->nullable();
            $table->string('short_name')->nullable();
            $table->text('short_description')->nullable();
            $table->text('overview')->nullable();
            $table->text('more_info')->nullable();
            $table->string('effort')->nullable();
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->dateTime('enrol_start')->nullable();
            $table->dateTime('enrol_end')->nullable();
            $table->float('price')->default(0);
            $table->string('course_image_uri')->nullable();
            $table->string('course_video_uri')->nullable();
            $table->bigInteger('course_category_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->string('slug')->nullable();
            $table->bigInteger('order_id')->unsigned();
            $table->string('course_video')->nullable();
            $table->string('self_paced')->nullable();
            $table->string('mobile_available')->nullable();
            $table->string('certificates_display_behavior')->nullabale();
            $table->string('_pre_requisite_courses_json')->nullable();
            $table->text('advanced_modules')->nullable();
            $table->text('pass_mark')->nullable();
            $table->text('enable_subsection_gating')->nullable();
            $table->integer('position')->unsigned()->nullable();


            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });






    }

    public function down()
    {

        Schema::dropIfExists('course_creations');
    }
}

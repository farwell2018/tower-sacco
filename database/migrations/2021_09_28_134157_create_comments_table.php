<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
           $table->id();
           $table->integer('user_id')->unsigned();
           $table->integer('session_meeting_id');
           $table->text('comment');
           $table->string('emoji');//not added into db..belongs to emojis
           $table->timestamps();

        //    $table->foreign('session_meetings_id')->refences('id')->on('session_meetings')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

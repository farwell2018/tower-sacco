<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_resources', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->string('cover_image')->nullable();
            $table->string('audio_file')->nullable();
            $table->string('video_file')->nullable();
            $table->string('downloable_file')->nullable();
            $table->string('external_link')->nullable();
            $table->bigInteger('resource_type_id')->unsigned()->nullable();
            $table->bigInteger('resource_theme_id')->unsigned()->nullable();
			$table->string('resource_category')->nullable();
			$table->string('resource_country')->nullable();
            $table->string('group_id');
            $table->string('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_resources');
    }
}

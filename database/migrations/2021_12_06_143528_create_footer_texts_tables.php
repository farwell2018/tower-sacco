<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFooterTextsTables extends Migration
{
    public function up()
    {
        Schema::create('footer_texts', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('footer_text_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'footer_text');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('footer_text_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'footer_text');
        });

        Schema::create('footer_text_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'footer_text');
        });
    }

    public function down()
    {
        Schema::dropIfExists('footer_text_revisions');
        Schema::dropIfExists('footer_text_translations');
        Schema::dropIfExists('footer_text_slugs');
        Schema::dropIfExists('footer_texts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolicyDocumentsTables extends Migration
{
    public function up()
    {
        Schema::create('policy_documents', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('policy_document_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'policy_document');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('policy_document_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'policy_document');
        });

        Schema::create('policy_document_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'policy_document');
        });
    }

    public function down()
    {
        Schema::dropIfExists('policy_document_revisions');
        Schema::dropIfExists('policy_document_translations');
        Schema::dropIfExists('policy_document_slugs');
        Schema::dropIfExists('policy_documents');
    }
}

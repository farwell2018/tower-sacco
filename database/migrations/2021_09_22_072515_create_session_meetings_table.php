<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_meetings', function (Blueprint $table) {
            $table->id();
            $table->string('meeting_id');
            $table->string('host_email');
            $table->text('topic');
            $table->string('status');
            $table->string('start_time');
            $table->text('agenda');
            $table->longText('join_url');
            $table->string('meeting_password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_meetings');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocialmediaTables extends Migration
{
    public function up()
    {
        Schema::create('socialmedia', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('key', 200)->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('socialmedia_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'socialmedia');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('socialmedia_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'socialmedia');
        });

        Schema::create('socialmedia_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'socialmedia');
        });
    }

    public function down()
    {
        Schema::dropIfExists('socialmedia_revisions');
        Schema::dropIfExists('socialmedia_translations');
        Schema::dropIfExists('socialmedia_slugs');
        Schema::dropIfExists('socialmedia');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsCoursecompletion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('course_completions', function (Blueprint $table) {
            $table->string('job_role_id')->nullable();
            $table->string('country_id')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('course_completions', function (Blueprint $table) {
            $table->dropColumn('job_role_id');
            $table->dropColumn('country_id');
           
        });
    }
}

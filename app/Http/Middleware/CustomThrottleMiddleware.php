<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Response;

class CustomThrottleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     protected $limiter;

    public function __construct(RateLimiter $limiter) {
        $this->limiter = $limiter;
    }
    public function handle($request, Closure $next, $maxAttempts = 5, $decayMinutes = 1)
    {
        $key = $this->resolveThrottleKey($request);

        if ($this->limiter->tooManyAttempts($key, $maxAttempts)) {
            $seconds = $this->limiter->availableIn($key);
            $message = trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]);
            return Redirect::route('login')->with('login_error', $message);
        }

        $this->limiter->hit($key, $decayMinutes * 60);

        return $next($request);
    }

    protected function resolveThrottleKey($request)
    {
       
        return $request->ip(); 
    }

}

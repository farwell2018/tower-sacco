<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class PolicyDocumentController extends BaseModuleController
{
    protected $moduleName = 'policyDocuments';

    protected $indexOptions = [
        'permalink' => false,
    ];
}

<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class CourseCategoryController extends BaseModuleController
{
    protected $moduleName = 'courseCategories';

    protected $indexOptions = [
    ];
}

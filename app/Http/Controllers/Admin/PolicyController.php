<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\ResourceTypeRepository;
use App\Repositories\ContentBucketRepository;
use App\Repositories\CourseRepository;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Support\Constants;

class PolicyController extends BaseModuleController
{
    protected $moduleName = 'policies';

    protected $indexWith = ['type'];


    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        
        'resource_type_id' => [ // relation column
            'title' => 'Resource Type',
            'sort' => true,
            'relationship' => 'type',
            'field' => 'title'
        ],
        'created_at' => [
            'title' => 'Uploaded On',
            'field' => 'created_at',
            'present' => true, 
        ],
    ];


    protected function formData($request)
    {

    
        return [
			'resourceTypeList' => app(ResourceTypeRepository::class)->listAll('title'),
			'contentBucketList' => app(ContentBucketRepository::class)->listAll('title'),
        ];
}
}
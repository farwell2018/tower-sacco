<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class SocialmediaController extends ModuleController
{
    protected $moduleName = 'socialmedia';
    
}

<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;


class SessionResourceController extends ModuleController
{
    //
    protected $moduleName = 'sessionResources';


    protected $indexWith = ['session','user'];

    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
    ];
    
    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'description' => [
            'title' => 'description',
            'field' => 'description',
            'sort' => true, // column is sortable
        ],
        'session_id' => [ // relation column
            'title' => 'Session',
            'sort' => true,
            'relationship' => 'session',
            'field' => 'title'
        ],
        
        'user_id' => [ // relation column
            'title' => 'Uploader',
            'sort' => true,
            'relationship' => 'user',
            'field' => 'name'
        ],

        'filename' => [
            'title' => 'Resource',
            'field' => 'filename',
            'present' => true, 
        ],
        'created_at' => [
            'title' => 'Uploaded On',
            'field' => 'created_at',
            'present' => true, 
        ],
    ];

}

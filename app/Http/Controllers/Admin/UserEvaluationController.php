<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Course;
use Illuminate\Http\Request;

class UserEvaluationController extends ModuleController
{
    protected $moduleName = 'userEvaluations';
    
    protected $titleColumnKey = 'username';

    protected $filters = [
        'course_id' => 'course_id',
    ];

    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
        'delete' => false,
    ];


    protected $indexColumns = [
        
        'username' => [ // field column
            'title' => 'User Name',
            'field' => 'username'
        ],

        'course_id' => [ // relation column
            'title' => 'Course',
            'relationship' => 'courses',
            'field' => 'name'
        ],

        'answer1' => [
            'title' => 'Question One',
            'field' => 'answer1',
           
        ],
        'answer2' => [ // relation column
            'title' => 'Question Two',
            'field' => 'answer2'
        ],
        'answer3' => [ // relation column
            'title' => 'Question Three',
            'field' => 'answer3'
        ],
        'answer4' => [ // relation column
            'title' => 'Question four',
            'field' => 'answer4'
        ],
        'answer5' => [ // relation column
            'title' => 'Question five',
            'field' => 'answer5'
        ],
        'answer6' => [ // relation column
            'title' => 'Question six',
            'field' => 'answer6'
        ],
        'answer7' => [ // relation column
            'title' => 'Question seven',
            'field' => 'answer7'
        ],
        'answer8' => [ // relation column
            'title' => 'Question eight',
            'field' => 'answer8'
        ],
        'answer9' => [ // relation column
            'title' => 'Question nine',
            'field' => 'answer9'
        ],
        'answer10' => [ // relation column
            'title' => 'Question ten',
            'field' => 'answer10'
        ],
       
    ];


    

    protected function indexData($request)
    {
        return [
            'course_idList' =>  Course::all(),
           
        ];
    }

}

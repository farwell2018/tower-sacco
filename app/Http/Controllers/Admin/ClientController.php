<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ClientController extends ModuleController
{
    protected $moduleName = 'clients';
}

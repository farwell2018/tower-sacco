<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\MenuTypeRepository;
use App\Repositories\ContentBucketRepository;

class PageController extends ModuleController
{
    protected $moduleName = 'pages';

    protected $permalinkBase = '';

    protected $indexOptions = [
        'reorder' => true,
    ];

    protected $indexColumns = [
        'title' => [
            'title' => 'Page',
            'field' => 'title',
        ],
    ];

    protected $previewView = 'site.pages.show';


    protected function formData($request)
    {

        return [
			'menuTypeList' => app(MenuTypeRepository::class)->listAll('title'),
			'contentBucketList' => app(ContentBucketRepository::class)->listAll('title'),
        ];

	}
}

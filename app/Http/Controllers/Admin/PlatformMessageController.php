<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\JobRoleRepository;

class PlatformMessageController extends ModuleController
{
    protected $moduleName = 'platformMessages';



    protected function formData($request)
{
   
    $collection = collect(['All']);
    $merged = $collection->merge(app(JobRoleRepository::class)->listAll('title'));
    $merged->all();

    return [
        'roleList' => $merged,
        
    ];

}
    
}

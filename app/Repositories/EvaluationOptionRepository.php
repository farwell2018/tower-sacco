<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\EvaluationOption;

class EvaluationOptionRepository extends ModuleRepository
{
    

    public function __construct(EvaluationOption $model)
    {
        $this->model = $model;
    }
}

<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Client;

class ClientRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

    public function __construct(Client $model)
    {
        $this->model = $model;
    }

    // public function getItems($scopes = [], $sorts =[], $perPage = -1, $with = [], $page =null)
    // {
    //     $query = $this->model->with($with)->isSponsor()
	// 		->published();

    //     if (isset($scopes['exceptId'])) {
    //         $query->where('id', "<>", $scopes['exceptId']);
    //     }

    //     if (isset($scopes['ids']) && !empty($scopes['ids'])) {
    //         $query->whereIn('id', $scopes['ids']);
    //     }

    //     if (isset($scopes['ids']) && !empty($scopes['ids'])) {
    //         $query->orderBy(DB::raw('FIELD(id,'.implode(',',$scopes['ids']).')'));
    //     } elseif (!empty($sorts)) {
    //         foreach($sorts as $field => $dir) {
    //             $query->orderBy($field, $dir);
    //         }
    //     }

    //     return $perPage > 0 ? $query->paginate($perPage, ['*'], 'page', $page) :
    //         (($perPage != -1) ? $query->limit(abs($perPage))->get() : $query->get())
    //     ;
    // }
}

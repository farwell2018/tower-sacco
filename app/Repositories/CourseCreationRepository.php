<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseCreation;
use A17\Twill\Models\Block;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Edx\EdxAuthUser;
use App\Edx\CourseOverviewsCourseoverview;
use App\Edx\StudentCourseAccessRole;
use App\Edx\StudentCourseEnrollment;
use App\Edx\CourseModesCoursemode;
use Carbon\Carbon;

class CourseCreationRepository extends ModuleRepository
{
    use HandleBlocks, HandleMedias, HandleFiles;

    public function __construct(CourseCreation $model)
    {
        $this->model = $model;
    }


    /**
     * @param $attributes
     * @param $fields
     * @return \A17\Twill\Models\Model
     */
    public function firstOrCreate($attributes, $fields = [])
    {
        return $this->model->where($attributes)->first() ?? $this->create($attributes + $fields);
    }


    /**
     * @param array $fields
     * @return \A17\Twill\Models\Model
     */
    public function createForPreview($fields)
    {
        $fields = $this->prepareFieldsBeforeCreate($fields);

        $object = $this->model->newInstance(Arr::except($fields, $this->getReservedFields()));

        return $this->hydrate($object, $fields);
    }

    /**
     * @param array $attributes
     * @param array $fields
     * @return \A17\Twill\Models\Model
     */
    public function updateOrCreate($attributes, $fields)
    {
        $object = $this->model->where($attributes)->first();

        if (!$object) {
            return $this->create($fields);
        }

        return $this->update($object->id, $fields);
    }

    /**
     * @param mixed $id
     * @param array $fields
     * @return \A17\Twill\Models\Model
     */
    public function update($id, $fields)
    {
        return DB::transaction(function () use ($id, $fields) {
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            $this->afterSave($object, $fields);

            return $object->fresh();
        }, 3);
    }

    /**
     * @param mixed $id
     * @param array $values
     * @param array $scopes
     * @return mixed
     */
    public function updateBasic($id, $values, $scopes = [])
    {
        return DB::transaction(function () use ($id, $values, $scopes) {
            // apply scopes if no id provided
            if (is_null($id)) {
                $query = $this->model->query();

                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }

                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            // apply to all ids if array of ids provided
            if (is_array($id)) {
                $query = $this->model->whereIn('id', $id);
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            if (($object = $this->model->find($id)) != null) {
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                return true;
            }

            return false;
        }, 3);
    }

    /**
     * @param string[] $fields
     * @return \A17\Twill\Models\Model
     */
    public function create($fields)
    {
        $course = courseEdxCreate($fields);

      

        if ($course['status'] == false) {
            return $course;
        } else {

            $course_id = $course['data'];

            $course = CourseOverviewsCourseoverview::where('id', $course_id)->first();

            $edxUser = EdxAuthUser::where('username', Auth::user()->username)->first();

            StudentCourseAccessRole::create(['org' => $course->org, 'course_id' => $course->id, 'role' => StudentCourseAccessRole::INSTRUCTOR, 'user_id' => $edxUser->id]);

            StudentCourseAccessRole::create(['org' => $course->org, 'course_id' => $course->id, 'role' => StudentCourseAccessRole::STAFF, 'user_id' => $edxUser->id]);

            StudentCourseEnrollment::create(['course_id' => $course->id, 'created' => date('Y-m-d H:i:s'), 'is_active' => 1, 'mode' => CourseModesCoursemode::AUDIT, 'user_id' => $edxUser->id]);

            CourseModesCoursemode::create([
                'course_id' => $course->id, 'mode_slug' => CourseModesCoursemode::HONOR, 'mode_display_name' => $course->display_name,
                'currency' => 'usd', 'expiration_datetime_is_explicit' => '0', 'min_price' => 0, 'suggested_prices' => 0
            ]);


            return DB::transaction(function () use ($fields, $course) {

                $slug = str_replace(' ', '-', $course->display_name);

                $object = new CourseCreation();
                $object->course_id = $course->id;
                $object->display_name = $course->display_name;
                $object->short_name = $slug;

                $object->save();

                return $object;
            }, 3);
        }
    }

    public function updateSettings($fields)
    {


        $fields['medias']['cover_image'][0] = json_decode($fields['medias']['cover_image'][0], true);
        $id = $fields['item_id'];
        return DB::transaction(function () use ($id, $fields) {
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            $this->afterSave($object, $fields);

            // dd($object);



            $course = courseEdxSettings($fields);


            if ($course['status'] == false) {
                return $course;
            } else {

                $course = $course['data'];

                $object->overview = $course['overview'];
                $object->enrol_start = $course['enrollment_start'];
                $object->enrol_end = $course['enrollment_end'];
                $object->start = $course['start_date'];
                $object->end = $course['end_date'];
                $object->self_paced = $course['self_paced'];
                $object->short_description = $course['short_description'];
                $object->effort = $course['effort'];
                $object->course_image_uri = $course['course_image_asset_path'];
                $object->save();

                return $object->fresh();
            }
        }, 3);

        //  if(!empty(json_decode($fields['medias']['cover_image'][0])->original)){

        //       $image_url = json_decode($fields['medias']['cover_image'][0])->original;

        //  }else{
        //     $image_url = '';
        //  }




    }


    public function updateAdvanced($fields)
    {
        $course = courseEdxAdvanced($fields);

        if ($course['status'] == false) {
            return $course;
        } else {

            $course = $course['data'];

            return DB::transaction(function () use ($fields, $course) {

                $object = $this->model->findOrFail($fields['item_id']);

                $object->display_name = $course['display_name']['value'];
                $object->certificates_display_behavior =  $course['certificates_display_behavior']['value'];
                $object->mobile_available = $course['mobile_available']['value'];
                $object->enable_subsection_gating = $course['enable_subsection_gating']['value'];
                $object->advanced_modules = $fields['advanced_modules'];
                $object->save();

                return $object;
            }, 3);
        }
    }


    public function updateGrading($fields)
    {
        $course = courseEdxGrading($fields);

        if ($course['status'] == false) {
            return $course;
        } else {

            $course = $course['data'];

            if (isset($fields['blocks'])) {
                $block = Block::where('blockable_id', '=', $fields['item_id'])->get();
                if ($block) {
                    foreach ($block as $b) {
                        $b->delete();
                    }
                }
                $first = new Block();
                $first->blockable_id = $fields['item_id'];
                $first->blockable_type = 'App\Models\CourseCreation';
                $first->position = 1;
                $first->type = "course_grade";
                $first->editor_name = "default";
                $first->save();

                $position = 1;

                foreach ($fields['blocks'] as $key => $value) {

                    $second = new Block();
                    $second->blockable_id = $fields['item_id'];
                    $second->blockable_type = 'App\Models\CourseCreation';
                    $second->position = $position++;
                    $second->content = $value;
                    $second->type = "course_grade_repeater";
                    $second->child_key = "course_grade_repeater";
                    $second->parent_id = $first->id;
                    $second->editor_name = "default";
                    $second->save();
                }
            }

            DB::transaction(function () use ($fields, $course) {

                $object = $this->model->findOrFail($fields['item_id']);

                $object->pass_mark = $course['grade_cutoffs']['pass'];
                $object->save();

                return $object;
            }, 3);
        }
    }



    public function updateBlocks($fields)
    {

        $blocks = createCourseChapters($fields);

        if ($blocks['status'] == false) {
            return  $blocks;
        } else {


            $object = $this->model->findOrFail($fields['item_id']);

            if ($fields['type'] === 'edit' && $fields['category'] === 'vertical') {

                return [
                    $fields['item_id'],
                    $blocks['data']['id']
                ];
            } else {
                if (isset($blocks['data']['locator'])) {
                    return [
                        $object,
                        $blocks['data']['locator']
                    ];
                } else {
                    return $object;
                }
            }
        }
    }


    public function createUnitComponent($fields)
    {

        $libs = courseUnitComponents($fields);

        if ($libs['status'] == false) {
            return false;
        } else {

            return true;
        }
    }


    public function updateUnitBlocks($fields)
    {

        $blocks = createUnitBlocks($fields);

        if ($blocks['status'] == false) {
            return  $blocks;
        } else {
            $object = $this->model->findOrFail($fields['item_id']);

            return [
                $fields['item_id'],
                $fields['unit_id']
            ];
        }
    }



    public function updateBlocksLocal($fields)
    {
    }



    public function createLocal($fields)
    {
        return DB::transaction(function () use ($fields) {

            $slug = str_replace(' ', '-', $fields['short_name']);

            $object = new CourseCreation();
            $object->course_id = 'test_id';
            $object->display_name = $fields['short_name'];
            $object->short_name = $slug;

            $object->save();

            return $object;
        }, 3);
        // }
    }

    public function updateGradingLocal($fields)
    {

        dd($fields);
    }

    public function updateSettingsLocal($fields)
    {

        dd($fields);
    }


    public function updateAdvancedLocal($fields)
    {

        dd($fields);
    }
}

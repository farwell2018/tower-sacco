<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class PolicySlug extends Model
{
    protected $table = "policy_slugs";
}

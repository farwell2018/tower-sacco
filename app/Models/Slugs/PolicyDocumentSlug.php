<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class PolicyDocumentSlug extends Model
{
    protected $table = "policy_document_slugs";
}

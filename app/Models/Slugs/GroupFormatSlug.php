<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GroupFormatSlug extends Model
{
    protected $table = "group_format_slugs";
}

<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class MenuTypeSlug extends Model
{
    protected $table = "menu_type_slugs";
}

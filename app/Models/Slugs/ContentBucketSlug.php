<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ContentBucketSlug extends Model
{
    protected $table = "content_bucket_slugs";
}

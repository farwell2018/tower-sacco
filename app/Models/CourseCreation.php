<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class CourseCreation extends Model implements Sortable
{
    use HasBlocks, HasMedias, HasFiles, HasPosition;

    protected $fillable = [
        'course_id',
        'display_name',
        'short_name',
        'published',
        'position',
    ];

    protected $guarded = [];

    public $mediasParams = [
        'cover_image' => [
            'default' => [
                [
                    'name' => 'landscape',

                ]
            ]
        ],

        'hero_image' => [
            'default' => [
                [
                    'name' => 'landscape',

                ]
            ]
        ],

        'mobile_hero_image' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],

            ]
        ]
    ];

    public function getEffortSelectedAttribute()
    {
        $time = explode(":", $this->effort);
        if (sizeof($time) == 3) {
            $duration = CarbonInterval::hours($time[1]);
            $duration = $duration->minutes($time[2]);
            return $duration;
        }
    }


    public function getLinkSelectedAttribute()
    {

        return $this->short_name;
    }

    public function getEnrollmentAttribute()
    {


        /** use this when live */

        // if($this->course_id === 'course-v1:TowerSacco+TS101+2022_Q1'){

        //    $enrolled = Auth::check() ? $this->checkEnrollmentStatus($this->course_id) : false;

        //   }else{

        /** Local instance */
        $enrolled = Auth::check() ? $this->checkLicenseStatus($this->course_id) : false;
        //   }


        return $enrolled;
    }


    public function getCompletionAttribute()
    {

        $completed = Auth::check() ? $this->checkCompletionStatus($this->course_id) : false;

        return $completed['completionStatus'];
    }

    public function getRequiredAttribute()
    {
        // dd(Auth::user());
        $role = JobroleCourse::where('course_id', $this->id)->where('job_role_id', Auth::user()->job_role_id)->first();

        if (!empty($role)) {

            return true;
        } else {

            return false;
        }
    }




    public function getMandatoryAttribute()
    {
        // dd(Auth::user());
        $role = JobroleCourse::where('course_id', $this->id)->where('job_role_id', Auth::user()->job_role_id)->first();

        if (!empty($role)) {

            return true;
        } else {

            return false;
        }
    }

    public function getRecommendedAttribute()
    {
        // dd(Auth::user());
        $role = UserCourses::where('course_id', $this->id)->where('user_id', Auth::user()->id)->first();

        if (!empty($role)) {

            return true;
        } else {

            return false;
        }
    }



    public static function getNotMandatory()
    {
        $compulsories = array();

        $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)->get();


        $all = CourseCreation::published()->orderBy('created_at', 'desc')->simplePaginate(12);


        foreach ($role as $comp) {
            $compulsories[] = $comp->course_id;
        }
        foreach ($all as $key => $l) {
            if (in_array($l->id, $compulsories)) {
                unset($all[$key]);
            }
        }


        return $all;
    }


    public static function paginate($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    private function checkLicenseStatus($course_id)
    {

        $enrollment = UserLicense::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();
        if ($enrollment) {
            $enrollmentStatus = true;
        } else {
            $enrollmentStatus = false;
        }

        return $enrollmentStatus;
    }



    private function checkCompletionStatus($course_id)
    {

        $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();

        if ($completion) {

            if ($completion->score >= 0.80) {

                $completionStatus = 1;
                $status = "Completed";
            } else {

                $completionStatus = 2;
                $status = 'Completed but failed';
            }
        } else {
            $completionStatus = false;
            $status = 'In Progress';
        }

        return [
            'status' => $status,
            'completionStatus' => $completionStatus
        ];
    }





    private function checkEnrollmentStatus($course_id)
    {
        $configLms = config()->get("settings.lms.live");
        //dd(isset($_COOKIE['edinstancexid']));
        if (!isset($_COOKIE['edinstancexid'])) {
            Auth::logout();
            return false;
        }
        $client = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );
        $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment/' . Auth::user()->username . ',' . $course_id);

        $response = json_decode($request->getBody()->getContents());
        //dd($response);
        if ($response && $response->is_active == true) {
            $enrollmentStatus = true;
        } else {
            $enrollmentStatus = false;
        }
        //dd($enrollmentStatus);
        return $enrollmentStatus;
    }
}

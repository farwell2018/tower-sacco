<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ResourceRevision extends Revision
{
    protected $table = "resource_revisions";
}

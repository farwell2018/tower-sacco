<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SessionResourceRevision extends Revision
{
    protected $table = "session_resource_revisions";
}

<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class PolicyDocumentRevision extends Revision
{
    protected $table = "policy_document_revisions";
}

<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class FooterTextRevision extends Revision
{
    protected $table = "footer_text_revisions";
}

<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class FeatureWeekRevision extends Revision
{
    protected $table = "feature_week_revisions";
}

<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ResourceDocumentRevision extends Revision
{
    protected $table = "resource_document_revisions";
}

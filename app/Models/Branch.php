<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Auth;
use Artisan;

class Branch extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];


   public function users(){

    return $this->hasMany(User::class);

   }


   public function completions(){

    return $this->hasMany(CourseCompletion::class);

   }

   public function  getRegisteredAttribute(){
    $users = array();
    foreach($this->users as $user){
       
        $users[] = $user->id;
    }

    return count($users);
   
   }



   public function  getCompletedAttribute(){
    // Artisan::call('view:clear');
    // Artisan::call('optimize:clear');
    $compulsories = array();
    $complete = array();
    $total = array();
// dd('yes');
    $users = $this->users;
    $cu = [];
    $passmark = 0.69;

    foreach( $users as $key => $user){
        $compulsory = JobroleCourse::where('job_role_id', $user->job_role_id)->get();
        $completed =  CourseCompletion::where('user_id','=', $user->id)->where('score','>', $passmark)->get();
        $compulsoryIds = $compulsory->pluck('course_id')->toArray();
        $completedIds = $completed->pluck('course_id')->toArray();
        if (count(array_intersect($compulsoryIds, $completedIds)) ==  count($compulsoryIds)){
            $total[] += 1;
        }
        // dd(count(array_intersect($compulsoryIds, $completedIds)));
        // if(count($completed) > 0){
        //     foreach($compulsory as $comp){
        //         $compulsories[] = $comp->course_id;
        //        }
        //     foreach($completed as $compl){
        //          $complete[] = $compl->course_id;
        
        //     }
            
        //      if(count($compulsories) != count($complete)){
        //         unset($users[$key]);

        //      }
             
        //      else{

        //         $total[] += 1;
        //      }
             
    
        // }else{

        //     unset($users[$key]);
        // }

       
    }
    return count($total);
   
   }


   public function  getTotalAttribute(){
    // Artisan::call('view:clear');
    // Artisan::call('optimize:clear');
    $compulsories = array();
    $complete = array();
    $total = array();
// dd('yes');
    $users = $this->users;
    $cu = [];
    $passmark = 0.69;

    foreach( $users as $key => $user){
        $compulsory = JobroleCourse::where('job_role_id', $user->job_role_id)->get();
        $completed =  CourseCompletion::where('user_id','=', $user->id)->where('score','>', $passmark)->get();
        $compulsoryIds = $compulsory->pluck('course_id')->toArray();
        $completedIds = $completed->pluck('course_id')->toArray();
        if (count(array_intersect($compulsoryIds, $completedIds)) ==  count($compulsoryIds)){
            $cu[] = $user->email;
        }
       
    }
    foreach ($cu as $email) {
    echo $email . "<br>";
}

   
   }
   

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MyCommunity extends Model
{
    public function scopeMyFollowers($query)
    {
        return $query->where('followd_id', Auth::id());
    }

    public function scopeFollowing($query)
    {
        return $query->where('user_id', Auth::id());
    }
}

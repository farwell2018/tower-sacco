<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Session;

class SessionTranslation extends Model
{
    protected $baseModuleModel = Session::class;
}

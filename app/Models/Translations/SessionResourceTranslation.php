<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\SessionResource;

class SessionResourceTranslation extends Model
{
    protected $baseModuleModel = SessionResource::class;
}

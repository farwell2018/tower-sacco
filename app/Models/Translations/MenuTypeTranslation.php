<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\MenuType;

class MenuTypeTranslation extends Model
{
    protected $baseModuleModel = MenuType::class;
}

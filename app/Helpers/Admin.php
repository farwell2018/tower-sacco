<?php
use App\Models\News;
use App\Support\Constants;
use App\Models\Sponsor;

if(!function_exists('getTextsFromBlocks')) {
    function getTextsFromBlocks($blocks, $locale)
    {
        $body = "";
        foreach($blocks as $block) {
            $body .= !empty($t=$block->translatedInput('title',$locale)) ? " {$t}" : "";
            $body .= !empty($t=$block->translatedInput('description',$locale)) ? " {$t}" : "";
            $body .= !empty($t=$block->translatedInput('text',$locale)) ? " ".strip_tags($t) : "";
        }

        return $body;
    }
}

if(!function_exists('getTextsOfTags')) {
    function getTextsOfTags($tags)
    {
        return !empty($tags) ? $tags->map(function ($tag) {
            return " {$tag->name}";
        })->implode('') : "";
    }
}

if(!function_exists('getTextsOfRelation')) {
    function getTextsOfRelation($items)
    {
        return !empty($items) ? $items->map(function ($item) {
            return " {$item->title}";
        })->implode('') : "";
    }
}

if(!function_exists('getTextsOfSingleRelation')) {
    function getTextsOfSingleRelation($item)
    {
        return " {$item->title}";
    }
}

if (!function_exists('newsTypesSelectOptions')) {
    function newsTypesSelectOptions()
    {
        return collect(News::NEWS_TYPES)->map(function($item){
			return $item;
		});
    }
}

if (!function_exists('sponsorTypesSelectOptions')) {
    function sponsorTypesSelectOptions()
    {
        return collect(Sponsor::SPONSOR_TYPES)->map(function($item){
			return $item;
		});
    }
}

if (!function_exists('documentReviewStatusList')) {
    function documentReviewStatusList()
    {
        $adminRoles = ['Country-Administrator','Administrator'];
		$providerRoles = ['Provider', 'Registered-User'];

		if(auth()->guard('twill_users')->user() && (auth()->guard('twill_users')->user()->is_superadmin || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			return collect(Constants::ADMIN_PUBLISH_STATUS);
		}

		if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$providerRoles)){
			return collect(Constants::ADMIN_PUBLISH_STATUS);
		}
		return collect();
    }
}

if (!function_exists('isDocumentProvider')) {
    function isDocumentProvider()
    {
        $adminRoles = ['Country-Administrator','Administrator'];
		$providerRoles = ['Provider', 'Registered-User'];


		if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$providerRoles)){
			return true;
		}
		return false;
    }
}

if (!function_exists('isDocumentManager')) {
    function isDocumentManager()
    {
        $adminRoles = ['Country-Administrator','Administrator'];
		$providerRoles = ['Provider', 'Registered-User'];


		if(auth()->guard('twill_users')->user() && (auth()->guard('twill_users')->user()->is_superadmin || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			return true;
		}
		return false;
    }
}

if (!function_exists('hasFinancingProvidersPermissions')) {
    function hasFinancingProvidersPermissions()
    {
		$user = auth()->guard('twill_users')->user();
		if(!$user->permissions){
			return false;
		}
		if (in_array('financingProviders', $user->permissions->pluck(['permissionable_module'])->toArray())) {
			return true;
		}
		return false;
    }
}

if (!function_exists('hasBdsProvidersPermissions')) {
    function hasBdsProvidersPermissions()
    {
        $user = auth()->guard('twill_users')->user();
		if(!$user->permissions){
			return false;
		}
		if (in_array('bdsProviders', $user->permissions->pluck(['permissionable_module'])->toArray())) {
			return true;
		}
		return false;
    }
}

if (!function_exists('has2faenabled')) {
    function has2faenabled()
    {
        $user = auth()->guard('twill_users')->user();
		return $user->google_2fa_enabled;
    }
}




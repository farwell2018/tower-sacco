<table>
    <thead>
        <tr></tr>

        <tr>
       @foreach($topic[0] as $key => $value)
     
       <th style="text-align:center"><b>{{ ucfirst($value) }}</b></th>
     
         @endforeach
        </tr>
        <tr></tr>
      <tr>
          @foreach($headings as $key => $value)
          
          <th><b>{{ ucfirst($value) }}</b></th>
  
          @endforeach
          </tr>
  
      </thead>
    <tbody>

    @foreach($data as $key=> $row)
    
    	<tr>
       <td>{{ $row->name }}</td>
       <td>{{ $row->coursesName }}</td>
       <td>{{ $row->reflection }}</td>
	</tr>
    @endforeach
    </tbody>
</table>

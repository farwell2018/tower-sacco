@extends('layouts.dashboard')

@section('title','Session Resources')


@section('content')

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
      <div class="kt-portlet__head-label">
        <span class="kt-portlet__head-icon">
          <i class="kt-font-brand flaticon2-line-chart"></i>
        </span>
        <h3 class="kt-portlet__head-title">
          All Resources
        </h3>
      </div>
      
    </div>

    <div class="kt-form" id="filterForm"  >
        <div class="kt-portlet__body">
    
          <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="session">Sessions</label>
            <select class="form-control" name="session">
              <option value="">All Sessions</option>
              @foreach($sessions as $session)
          <option value="{{$session->id}}">{{$session->topic}}</option>
          @endforeach
            </select>
          </div>
        </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              <div class="form-group">
                <label for="daterange">Upload Date Range</label>
                <input type="text" name="daterange" id="daterange" class="form-control" value="" placeholder="Click to Select Date Range"/>
              </div>
            </div>
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 "><p style="margin-top: 2rem;"></p>
          <button class="btn btn-primary pull-right" onclick="filterReports()">Submit</button>
        </div>
          </div>
    
        </div>
    
    
    </div>
  
    <div class="kt-portlet__body">
  
      <table class="table" id="session_resources" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>Session </th>
            <th>Learner </th>
            <th>Title</th>
            <th>Description</th>
            <th>Document</th>
            <th>Date of Upload</th>
            
          </tr>
        </thead>
       
      </table>
  
    </div>
  </div>
  
  @endsection

  
@section('footer-js')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>

  var dataTable;


  //Set ajax baseURl
  let baseUrl = "{!! route('sessionsResource.json') !!}?";

 
  //Query data
  function encodeQueryData(data) {
   const ret = [];
   for (let d in data)
     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
   return ret.join('&');
}

  //Filter method
  function filterReports(){
    //Get session value
    const session = document.querySelector('#filterForm select[name=session]').value
    //Get date range
    const expiry_range = document.querySelector('#filterForm input[name=daterange]').value
    //Add to URL

    
    const url = baseUrl + "&" + encodeQueryData({session,expiry_range})
  
    //console.log(url);
    dataTable.ajax.url( url ).load();
  }

$(document).ready(function(){
  $('input#daterange').daterangepicker({autoUpdateInput: false});

  $('input#daterange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input#daterange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

  dataTable = $('#session_resources').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable coluFirstmn ordering
    "lengthMenu": [
      [5, 10, 15, 20, 25, -1],
      [5, 10, 15, 20, 25, "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax": {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    // "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
//     buttons: [
//       { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
//       {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
//       {extend: 'excel', title: '{{ config('app.name', 'Innovativkonzept') }} - Subscribed users list',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
//       {extend: 'pdf', title: '{{ config('app.name', 'Innovativkonzept') }} - Subscribed users list',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
//       {extend: 'print',
//       customize: function (win){
//         $(win.document.body).addClass('white-bg');
//         $(win.document.body).css('font-size', '10px');
//         $(win.document.body).find('table')
//         .addClass('compact')
//         .css('font-size', 'inherit');
//       }
//     }
//   ],
  columns: [
    {data: 'id', name: 'id', orderable: true, searchable: true},
    {data: 'session', name: 'session', orderable: true, searchable: true},
    {data: 'user', name: 'user', orderable: true, searchable: true},
    {data: 'title', name: 'title', orderable: true, searchable: false},
    {data: 'description', name: 'description', orderable: true, searchable: false},
    {data: 'document', name: 'document', orderable: true, searchable: false},
    {data: 'created_at', name: 'created_at', orderable: true, searchable: false},
  
  ],
});
});
</script>


@endsection

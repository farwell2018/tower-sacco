@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#F18A21;text-align:center;">Group Invite</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
    A new group titled "{{$title}}" has been uploaded to {{ config('app.name', 'The Challenging Patriarchy Program') }} platform by {{$creater}}.</p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
      In order to accept the invite, please login to the platform then click on "Accept Invite" below .</p>
  
      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:16px;font-weight: bold;">
        <a href="{{$url}}" class="btn-drk-left" style=" background: #F87522;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Login
        </a>
      </p>
 

      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:16px;font-weight: bold;">
        <a href="{{$accept_url}}" class="btn-drk-left" style=" background: #F87522;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Accept Invite
        </a>
      </p>

      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#fff;font-size:16px;font-weight: bold;">
        <a href="{{$decline_url}}" class="btn-drk-left" style=" background: #F2203E;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Decline Invite
        </a>
      </p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#404040;text-align:center;">
  The Tower Sacco Academy
</p>

@endsection

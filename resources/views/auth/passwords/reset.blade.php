
@extends('layouts.app')

@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection


@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login">

                <div class="card-body">
                     <div class="col-md-12">
                      <div class="row">

                       <div class="col-md-1"></div>

                       <div class="col-md-10">
                    
                        <p >Please enter registration email address</p>
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="form-login">
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
        
                                <input type="hidden" name="token" value="{{ $token }}">
        
                                <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
        
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label ">{{ __('Password') }}</label>
        
                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label ">{{ __('Confirm Password') }}</label>
        
                                    <div class="col-md-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
        
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-overall btn_register">
                                            {{ __('Reset Password') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                       </div>
                       </div>
                      

                       <div class="col-md-1"></div>

                      </div>

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
               $('#course_information').val('1');
            }
            else if($(this).prop("checked") == false){
                $('#course_information').val(' '); 
            }
        });
    });
</script>


@endsection
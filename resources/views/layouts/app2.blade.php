<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') - {{ config('app.name', 'Sustainability.Training') }}</title>
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <!-- Scripts -->
  <script src="{{ asset('js/app.js?t='.time()) }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/ngunyimacharia/js/edx-logout.js?t='.time()) }}" type="text/javascript"></script>
  <!-- Styles -->
  <link href="{{ asset('css/app.css?t='.time()) }}" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  @yield('css')
</head>
<body>
  <div id="app">
    <div class="preloader">
      <img src="{{asset('images/spinner.png')}}" alt="" />
    </div>
    <div class="content-container">
      @yield('content')
    </div>
    <!-- Scroll to top button -->
    <button id="scrollTotopBtn" title="Go to top">
      <i class="fas fa-chevron-up"></i>
    </button>
    @guest
    @include('includes.modals.register-login')
    @endguest
    @include('includes.modals.share')
    @include('includes.modals.company-not-purchased')
  </div>
  </div>
  @include('includes.general.footer')
  @include('includes.modals.notification')
  @include('includes.modals.youtube')
  @yield('js')
@include('cookieConsent::index')
<script language="javascript">
    /* IE11 Fix for SP2010 */
    if (typeof(UserAgentInfo) != 'undefined' && !window.addEventListener)
    {
        UserAgentInfo.strBrowser=1;
    }
</script>
<script>
function hideCookieDialogs() {
    const dialogs = document.getElementsByClassName('js-cookie-consent');
    const value = 0;
    const COOKIE_DOMAIN = '{{ config('session.domain') ?? request()->getHost() }}';
    const name ='laravel_cookie_consent';
    var expirationInDays;

    const date = new Date();
    date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
    document.cookie = name + '=' + value
        + ';expires=' + date.toUTCString()
        + ';domain=' + COOKIE_DOMAIN
        + ';path=/{{ config('session.secure') ? ';secure' : null }}';

    for (let i = 0; i < dialogs.length; ++i) {
        dialogs[i].style.display = 'none';
    }
}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110061267-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110061267-1');
</script>

</body>
</html>

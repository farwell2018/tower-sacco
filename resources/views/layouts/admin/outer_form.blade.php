@extends('twill::layouts.main')

@section('appTypeClass', 'body--form')

@push('extra_css')
@if(app()->isProduction())
<link href="{{ twillAsset('main-form.css') }}" rel="preload" as="style" crossorigin />
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
@endif

@unless(config('twill.dev_mode', false))
<link href="{{ twillAsset('main-form.css') }}" rel="stylesheet" crossorigin />
@endunless

<style>
.col--primary-main {
width: 98%;
margin-left: 20px;
margin-right:20px;
}
.custom-tabs{
    border-radius: 2px;
    border: 1px solid #e5e5e5;
    margin-bottom: 20px;
    background: #fff;
}

.custom-tabs .box__filter{
position: relative;
    height: 50px;
    margin: -1px;
    white-space: nowrap;
    background: #e5e5e5;
    border-radius: 2px;
   padding-left:10px;
}

.custom-tabs .box__filter a {
display: inline-block;
height: 35px;
line-height: 35px;
text-decoration: none;
padding: 0 20px;
border-radius: 17px;
color: #3278b8;
background: none;
}


.custom-tabs .box__filter a.s--on {
background: #ffffff;
color: #262626;
}

.custom-tabs .custom-tab-content{
    padding: 0 20px 20px 20px;
}

    .container {
        width: 100% !important;
        padding-right: 50px;
        padding-left: 50px;
    }

    .col-12 {
        flex: 0 0 auto;
        width: 100%;
        display: flex;
    }

    .col-sm-6 {
        flex: 0 0 auto;
        width: 50%;
        padding-right: 20px;
    }

    .input[data-v-e9557df4] {
    margin-top: 50px;
    position: relative;
    }



    @media screen and (min-width: 1540px) {
        .col--primary {
            width: 1330px;
            margin-left: 20px;
        }
    }

    @media screen and (min-width: 600px) {
        .input__note[data-v-e9557df4] {
        display: inline !important;
        left: unset !important;
        right: 0 !important;
        top: 1px !important;
        position: absolute;
        font-size: 11px;
        }
        .multiselectorOuter .input__note[data-v-e9557df4] {
            display: inline;
            left: unset !important;
            width: 70%;
            float: right;
            top: 0px !important;
            position: absolute;
            font-size: 11px;
        }
    }
.navbar {
width: 100%;
background-color: #f2f2f2;
overflow: hidden;
 margin-bottom: 0px;
}

.wrapper-inner {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: horizontal;
-webkit-box-direction: normal;
-ms-flex-flow: row ;
flex-flow: row;
}
.button{
    -webkit-appearance: none;
    cursor: pointer;
    font-size: 1em;
    outline: none;
    margin: 0;
    border: 0 none;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    letter-spacing: inherit;
    display: inline-block;
    border-radius: 2px;
    padding: 0 30px;
    height: 40px;
    line-height: 38px;
    text-align: center;
    -webkit-transition: color .2s linear, border-color .2s linear, background-color .2s linear;
    transition: color .2s linear, border-color .2s linear, background-color .2s linear;
    text-decoration: none;
    }
.button--validate {
background: #1d9f3c;
color: #fff;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.modal {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1050;
    display: none;
    width: 100%;
    height: 100%;
    overflow: hidden;
    outline: 0;
     background: rgba(0, 0, 0, .66);
}

.modal__window {
    background: #fff;
    width: calc(100vw - 40px);
    max-width: 650px;
    position: relative;
    border-radius: 2px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-flow: column nowrap;
    flex-flow: column nowrap;
    margin: auto;
    top:20%;
}

.modal__header {
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    background: #d9d9d9;
    padding: 0 20px;
    height: 50px;
    line-height: 50px;
    position: relative;
    font-weight: 600;
}
.modal__content{
    overflow: hidden;
    overflow-y: auto;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    max-height: 100%;
}
.modal--form .modal__content {
    padding-bottom: 20px;
}

.accordion.accordion-flush{
    margin: 0 20px 10px 20px;
    border: 1px solid #e5e5e5;
    border-radius: 5px;
}

.accordion-flush .accordion-item {
    border-right: 0;
    border-left: 0;
    border-radius: 0;
}

.accordion-item {
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, .125);
}
.accordion-header{
    display:flex;
}
.accordion-flush .accordion-item:last-child {
    border-bottom: 0;
}
.accordion-flush .accordion-item:first-child {
    border-top: 0;
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease;
}
.accordion-item:last-of-type .accordion-button.collapsed {
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px);
}
.accordion-flush .accordion-item .accordion-button {
    border-radius: 0;
}
.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: '\02795';
    transition: transform .2s ease-in-out;
}

.accordion-button:not(.collapsed)::after {
    content: "\2796";
}

.accordion-button:not(.collapsed) {
    color: #0c63e4;
    background-color: #e7f1ff;
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.125);
}

.accordion-item:last-of-type .accordion-collapse {
    border-bottom-right-radius: .25rem;
    border-bottom-left-radius: .25rem;
}

.button-subsection{
    background: white;
    border: 1px solid #1d9f3c;
    color: #000;
}

.accordion-body {
    padding: 1rem 1.25rem;
}

.modal__content .input[data-v-e9557df4] {
    margin-top: 10px;
    position: relative;
    }

.subsection-spacing{
    margin: 0 40px 10px 40px;
}
.collapse:not(.show) {
    display: none;
}

.button-sectionEdit, .button-subsectionEdit{
    background: none;
    position: absolute;
    margin-left: 7%;
    margin-top: 5px;
}
.button-modalDelete{
 right: 4%;
 left: auto;
 margin-top: 7px;
}

 .button-sectionEdit.button-subsectionEdit.button-modalDelete{
    right:6%;
 }
  .button-sectionEdit.button-unitEdit.button-modalDelete{
    right:8%;
 }
.modalDelete .modal__content{
    padding:20px;
}

.modalDelete .modal__content form{
    text-align: center;
}
.button--validate.button-destroy{
 background: #e31a22;
}
     .filter__more {
            display: block !important;
            overflow: visible !important;
        }

        .filter__toggle.button.button--ghost {
            display: none !important;
        }


        .container {
            width: 100% !important;
        }

        .a17 .vselect__field .dropdown-toggle {
            height: 43px !important;
        }

        .date_range_picker {
            height: 43px !important;
        }

        .filter-select {
            height: 43px !important;
            margin-bottom: 2rem;
            border: 1px solid #a6a6a6;
    width: 100%;
    border-color: #a6a6a6;
        }

        .filter .filter__moreInner>div {
            display: contents !important;
        }

        .filter .filter__moreInner>div>* {
            margin-right: 20px;
            width: 14%;
        }

        .filter .filter__moreInner>select {
            margin-right: 20px;
            width: 9%;
        }

        .filter .filter__moreInner>div>label {
            margin-right: 20px;
            width: unset;
        }

.grader-show{
    padding-left: 20px;
    margin-bottom: 10px;
    font-size: 12px;
    color:#a6a6a6;
}

@media(max-width:1440px){
    .button-sectionEdit, .button-subsectionEdit{
    background: none;
    position: absolute;
    margin-left: 12%;
    margin-top: 5px;
}
}
</style>
@endpush

@push('extra_js_head')
@if(app()->isProduction())
<link href="{{ twillAsset('main-form.js') }}" rel="preload" as="script" crossorigin />

@endif


 <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@endpush

@php
$editor = $editor ?? false;
$translate = $translate ?? false;
$translateTitle = $translateTitle ?? $translate ?? false;
$titleFormKey = $titleFormKey ?? 'title';
$customForm = $customForm ?? false;
$controlLanguagesPublication = $controlLanguagesPublication ?? true;
$disableContentFieldset = $disableContentFieldset ?? false;
$editModalTitle = ($createWithoutModal ?? false) ? twillTrans('twill::lang.modal.create.title') : null;
$translate = $translate ?? false;
    $translateTitle = $translateTitle ?? $translate ?? false;
    $reorder = $reorder ?? false;
    $nested = $nested ?? false;
    $bulkEdit = $bulkEdit ?? true;
    $create = true;
    $skipCreateModal = $skipCreateModal ?? false;
    $controlLanguagesPublication = $controlLanguagesPublication ?? true;
@endphp

@section('content')
<div class="form" v-sticky data-sticky-id="navbar" data-sticky-offset="0" data-sticky-topoffset="12">
    <div class="navbar navbar--sticky" data-sticky-top="navbar">
        @php
        $additionalFieldsets = $additionalFieldsets ?? [];
        if(!$disableContentFieldset) {
        array_unshift($additionalFieldsets, [
        'fieldset' => 'content',
        'label' => $contentFieldsetLabel ?? twillTrans('twill::lang.form.content')
        ]);
        }
        @endphp
        <a17-sticky-nav data-sticky-target="navbar" :items="{{ json_encode($additionalFieldsets) }}">
            <a17-title-editor name="{{ $titleFormKey }}" :editable-title="{{ json_encode($editableTitle ?? true) }}"
                :control-languages-publication="{{ json_encode($controlLanguagesPublication) }}"
                custom-title="{{ $customTitle ?? '' }}" custom-permalink="{{ $customPermalink ?? '' }}"
                localized-permalinkbase="{{ json_encode($localizedPermalinkBase ?? '') }}"
                localized-custom-permalink="{{ json_encode($localizedCustomPermalink ?? '') }}" slot="title"
                @if($createWithoutModal ?? false) :show-modal="true" @endif @if(isset($editModalTitle))
                modal-title="{{ $editModalTitle }}" @endif>
                <template slot="modal-form">
                    @partialView(($moduleName ?? null), 'create')
                </template>
            </a17-title-editor>
            <div slot="actions">
                <a17-langswitcher :all-published="{{ json_encode(!$controlLanguagesPublication) }}"></a17-langswitcher>
                <a17-button v-if="editor" type="button" variant="editor" size="small" @click="openEditor(-1)">
                    <span v-svg symbol="editor"></span>{{ twillTrans('twill::lang.form.editor') }}
                </a17-button>
            </div>
        </a17-sticky-nav>
    </div>
    <section class="col col--primary-main" data-sticky-top="publisher">
        @unless($disableContentFieldset)
        @yield('contentFields')
        @if (config('twill.enabled.media-library') || config('twill.enabled.file-library'))
                        <a17-medialibrary ref="mediaLibrary"
                                          :authorized="{{ json_encode(auth('twill_users')->user()->can('upload')) }}" :extra-metadatas="{{ json_encode(array_values(config('twill.media_library.extra_metadatas_fields', []))) }}"
                                          :translatable-metadatas="{{ json_encode(array_values(config('twill.media_library.translatable_metadatas_fields', []))) }}"
                        ></a17-medialibrary>
                        <a17-dialog ref="deleteWarningMediaLibrary" modal-title="{{ twillTrans("twill::lang.media-library.dialogs.delete.delete-media-title") }}" confirm-label="{{ twillTrans("twill::lang.media-library.dialogs.delete.delete-media-confirm") }}">
                            <p class="modal--tiny-title"><strong>{{ twillTrans("twill::lang.media-library.dialogs.delete.delete-media-title") }}</strong></p>
                            <p>{!! twillTrans("twill::lang.media-library.dialogs.delete.delete-media-desc") !!}</p>
                        </a17-dialog>
                        <a17-dialog ref="replaceWarningMediaLibrary" modal-title="{{ twillTrans("twill::lang.media-library.dialogs.replace.replace-media-title") }}" confirm-label="{{ twillTrans("twill::lang.media-library.dialogs.replace.replace-media-confirm") }}">
                            <p class="modal--tiny-title"><strong>{{ twillTrans("twill::lang.media-library.dialogs.replace.replace-media-title") }}</strong></p>
                            <p>{!! twillTrans("twill::lang.media-library.dialogs.replace.replace-media-desc") !!}</p>
                        </a17-dialog>
                    @endif
                    <a17-notif variant="success"></a17-notif>
                    <a17-notif variant="error"></a17-notif>
                    <a17-notif variant="info" :auto-hide="false" :important="false"></a17-notif>
                    <a17-notif variant="warning" :auto-hide="false" :important="false"></a17-notif>
        {{-- <a17-fieldset title="{{ $contentFieldsetLabel ?? twillTrans('twill::lang.form.content') }}" id="content">

        </a17-fieldset> --}}
        @endunless

        @yield('fieldsets')
    </section>
    {{-- <form action="{{ $saveUrl }}" novalidate method="POST" @if($customForm) ref="customForm" @else
        v-on:submit.prevent="submitForm" @endif>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="wrapper wrapper--reverse" v-sticky data-sticky-id="publisher" data-sticky-offset="80">
                <aside class="col col--aside">
                    <div class="publisher" data-sticky-target="publisher">
                        <a17-publisher {!! !empty($publishDateDisplayFormat)
                            ? "date-display-format='{$publishDateDisplayFormat}'" : '' !!} {!!
                            !empty($publishDateFormat) ? "date-format='{$publishDateFormat}'" : '' !!} {!!
                            !empty($publishDate24Hr) && $publishDate24Hr ? ':date_24h="true"' : '' !!}
                            :show-languages="{{ json_encode($controlLanguagesPublication) }}">
                            @yield('publisherRows')
                        </a17-publisher>
                        <a17-page-nav placeholder="Go to page" previous-url="{{ $parentPreviousUrl ?? '' }}"
                            next-url="{{ $parentNextUrl ?? '' }}"></a17-page-nav>
                        @hasSection('sideFieldset')
                        <a17-fieldset title="{{ $sideFieldsetLabel ?? 'Options' }}" id="options">
                            @yield('sideFieldset')
                        </a17-fieldset>
                        @endif
                        @yield('sideFieldsets')
                    </div>
                </aside>

            </div>
        </div>
        <a17-spinner v-if="loading"></a17-spinner>
    </form> --}}
</div>
<a17-modal class="modal--browser" ref="browser" mode="medium" :force-close="true">
    <a17-browser></a17-browser>
</a17-modal>
<a17-modal class="modal--browser" ref="browserWide" mode="wide" :force-close="true">
    <a17-browser></a17-browser>
</a17-modal>
<a17-editor v-if="editor" ref="editor" bg-color="{{ config('twill.block_editor.background_color') ?? '#FFFFFF' }}">
</a17-editor>
<a17-previewer ref="preview"></a17-previewer>
<a17-dialog ref="warningContentEditor" modal-title="{{ twillTrans('twill::lang.form.dialogs.delete.title') }}"
    confirm-label="{{ twillTrans('twill::lang.form.dialogs.delete.confirm') }}">
    <p class="modal--tiny-title"><strong>{{ twillTrans('twill::lang.form.dialogs.delete.delete-content') }}</strong></p>
    <p>{!! twillTrans('twill::lang.form.dialogs.delete.confirmation') !!}</p>
</a17-dialog>
@stop

@section('initialStore')
window['{{ config('twill.js_namespace') }}'].STORE.form = {
baseUrl: '{{ $baseUrl ?? '' }}',
saveUrl: '{{ $saveUrl }}',
previewUrl: '{{ $previewUrl ?? '' }}',
restoreUrl: '{{ $restoreUrl ?? '' }}',
availableBlocks: {},
blocks: {},
blockPreviewUrl: '{{ $blockPreviewUrl ?? '' }}',
availableRepeaters: {!! $availableRepeaters ?? '{}' !!},
repeaters: {!! json_encode(($form_fields['repeaters'] ?? []) + ($form_fields['blocksRepeaters'] ?? [])) !!},
fields: [],
editor: {{ $editor ? 'true' : 'false' }},
isCustom: {{ $customForm ? 'true' : 'false' }},
reloadOnSuccess: {{ ($reloadOnSuccess ?? false) ? 'true' : 'false' }},
editorNames: [],
create: '{{ $createUrl ?? '' }}',
}

window['{{ config('twill.js_namespace') }}'].STORE.publication = {
{{-- withPublicationToggle: {{ json_encode(($publish ?? true) && isset($item) && $item->isFillable('published')) }}, --}}
published: {{ isset($item) && $item->published ? 'true' : 'false' }},
createWithoutModal: {{ isset($createWithoutModal) && $createWithoutModal ? 'true' : 'false' }},
withPublicationTimeframe: {{ json_encode(($schedule ?? true) && isset($item) && $item->isFillable('publish_start_date'))
}},
publishedLabel: '{{ $customPublishedLabel ?? twillTrans('twill::lang.main.published') }}',
expiredLabel: '{{twillTrans('twill::lang.publisher.expired')}}',
scheduledLabel: '{{twillTrans('twill::lang.publisher.scheduled')}}',
draftLabel: '{{ $customDraftLabel ?? twillTrans('twill::lang.main.draft') }}',
submitDisableMessage: '{{ $submitDisableMessage ?? '' }}',
startDate: '{{ $item->publish_start_date ?? '' }}',
endDate: '{{ $item->publish_end_date ?? '' }}',
visibility: '{{ isset($item) && $item->isFillable('public') ? ($item->public ? 'public' : 'private') : false }}',
reviewProcess: {!! isset($reviewProcess) ? json_encode($reviewProcess) : '[]' !!},
submitOptions: {!! isset($submitOptions) ? json_encode($submitOptions) : 'null' !!}
}

window['{{ config('twill.js_namespace') }}'].STORE.revisions = {!! json_encode($revisions ?? []) !!}

window['{{ config('twill.js_namespace') }}'].STORE.parentId = {{ $item->parent_id ?? 0 }}
window['{{ config('twill.js_namespace') }}'].STORE.parents = {!! json_encode($parents ?? []) !!}

window['{{ config('twill.js_namespace') }}'].STORE.medias.crops = {!! json_encode(($item->mediasParams ?? []) +
config('twill.block_editor.crops') + (config('twill.settings.crops') ?? [])) !!}
window['{{ config('twill.js_namespace') }}'].STORE.medias.selected = {}

window['{{ config('twill.js_namespace') }}'].STORE.browser = {}
window['{{ config('twill.js_namespace') }}'].STORE.browser.selected = {}

window['{{ config('twill.js_namespace') }}'].APIKEYS = {
'googleMapApi': '{{ config('twill.google_maps_api_key') }}'
}

 @if ($create && ($openCreate ?? false))
        window['{{ config('twill.js_namespace') }}'].openCreate = {!! json_encode($openCreate) !!}
    @endif
@stop

@prepend('extra_js')

@includeWhen(config('twill.block_editor.inline_blocks_templates', true), 'twill::partials.form.utils._blocks_templates')
<script src="{{ twillAsset('main-form.js') }}" ></script>
<script>
$(document).on("click", ".button-subsection", function () {
     var myBookId = $(this).data('id');
     $(".subsectionModal .modal__content #parentLocator").val( myBookId );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});





$(document).on("click", ".button-sectionEdit", function () {
     var myBookId = $(this).data('id');
     var myName = $(this).data('name');
     var myType = $(this).data('type');
     $(".sectionModal .modal__content #parentLocator").val( myBookId );
    $(".sectionModal .modal__content input[name=section_name]").val( myName );
    $(".sectionModal .modal__content #creationType").val( myType );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});

$(document).on("click", ".button-subsectionEdit", function () {
     var myBookId = $(this).data('id');
     var myName = $(this).data('name');
     var myType = $(this).data('type');

    var format = $(this).data('format');
    var is_prereq = $(this).data('is_prereq');
    var graders = $(this).data('graders');
    var prereqs = $(this).data('prereqs');
    var enabled = $(this).data('enabled');
    var prerequisite = $(this).data('prerequisite');
    var prereqScore = $(this).data('prereqscore');
    var prereqCompletion = $(this).data('prereqcompletion');


console.log(prereqScore)

    $("#subsectionGrader").empty();
    $('#subsectionPrerequisite').empty();

    if(format !== ''){
        $.each(graders, function(key, value) {
        if (value.type ==  format) {
            $("#subsectionGrader").append('<option value="' + value.type + '" selected>' + value.type + '</option>');
            } else {

            $("#subsectionGrader").append('<option value="' + value.type + '" >' + value.type +'</option>');
            }

        });
    }else{
    $("#subsectionGrader").append('<option value="" >Select Grading</option>');

    $.each(graders, function(key, value) {
        $("#subsectionGrader").append('<option value="' + value.type + '" >' + value.type +'</option>');
        });

    }

    console.log(prereqs);
    if(prereqs !== ''){

     if(prerequisite !== ''){
       $.each(prereqs, function(key, value) {
          if(value.block_usage_key === prerequisite){
            $("#subsectionPrerequisite").append('<option value="' + value.block_usage_key + '" selected>' + value.block_display_name + '</option>');
          }else{
            $("#subsectionPrerequisite").append('<option value="' + value.block_usage_key + '" >' + value.block_display_name + '</option>');
          }
        });

    }else{

       $("#subsectionPrerequisite").append('<option value="" >Select Prerequisite</option>');

    $.each(prereqs, function(key, value) {
        $("#subsectionPrerequisite").append('<option value="' + value.block_usage_key + '" >' + value.block_display_name + '</option>');
        });

    }
}

console.log(is_prereq)

    if(is_prereq == 1){
        $("#subsection_is_prereq").prop("checked", true);
    }else{
        $("#subsection_is_prereq").prop("checked", false);
    }

     $(".subsectionModal .modal__content #parentLocator").val( myBookId );
    $(".subsectionModal .modal__content input[name=section_name]").val( myName );
      $(".subsectionModal .modal__content input[name=prereq_min_completion]").val(prereqCompletion);
        $(".subsectionModal .modal__content input[name=prereq_min_score]").val(prereqScore );
    $(".subsectionModal .modal__content #creationType").val( myType );



    if( myType ==="edit"){

        if(enabled == true){
         $(".subsectionModal .modal__content #preSection").css("display","block");
        }

         $(".subsectionModal .modal__content #graderSection").css("display","block");

    }else{
          $(".subsectionModal .modal__content #graderSection").css("display","none");
           $(".subsectionModal .modal__content #preSection").css("display","none");
    }


     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});



$(document).on("click", ".button-subsection.button-unit", function () {
     var myBookId = $(this).data('id');
     $(".unitModal .modal__content #parentLocator").val( myBookId );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});



$(document).on("click", ".button-sectionEdit.button-modalDelete", function () {
     var myBookId = $(this).data('id');
     var myType = $(this).data('type');
      var myCategory = $(this).data('category');

     $(".modalDelete #categoryTitle").html(myCategory);
     $(".modalDelete .modal__content #parentLocator").val( myBookId );
      $(".modalDelete .modal__content #creationType").val( myType );
       $(".modalDelete .modal__content #categoryType").val( myCategory );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});


function handleSubsection(data){

    console.log(data);
}


</script>


<script>
$(document).ready(function () {


      $('.custom-tabs .box__filter li a').each(function () {
           var tab = "{{ old('tab') }}";
           var text;

          if (tab === "settings"){
            text = "Schedule & details";
          }else if(tab === "advanced"){
            text = "Advanced Settings";
          }else if(tab === "grading"){
            text = "Grading";
          }else if(tab === "content"){
            text = "Content";
          }
           if(tab){
              $(this).removeClass('s--on')

            if ($(this).text().trim() === text) {
                // Add active class
               $(this).addClass('s--on');
            }
           }
        });


      $('.custom-tabs div').each(function () {

        var tab = "{{ old('tab') }}"
        if(tab){

             $(this).removeClass('is-active');

            if ($(this).hasClass('custom-tab--'+tab)) {
                // Add active class
            $(this).addClass('is-active');
           }

        }

        });
});


$(document).ready(function () {

    // Get current page URL
      $('.custom-tabs .box__filter li a').each(function () {
            var url = window.location.href;
            var tab = url.split('#')[1];
            var text;

          if (tab === "settings"){
            text = "Schedule & details";
          }else if(tab === "advanced"){
            text = "Advanced Settings";
          }else if(tab === "grading"){
            text = "Grading";
          }else if(tab === "content"){
            text = "Content";
          }
           if(tab){
              $(this).removeClass('s--on')

            if ($(this).text().trim() === text) {
                // Add active class
               $(this).addClass('s--on');
            }
           }
        });


      $('.custom-tabs div').each(function () {

         var url = window.location.href;
         var tab = url.split('#')[1];
        if(tab){

             $(this).removeClass('is-active');

            if ($(this).hasClass('custom-tab--'+tab)) {
                // Add active class
            $(this).addClass('is-active');
           }

        }

        });
});
</script>

@endprepend

@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.course.courseCreation.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.course.courseCreation.index') }}" class="">
                {{ __('Course Structure') }}
            </a>
        </li>

        {{-- <li class="nav__item {{$activeRoute == 'admin.course.courseLibraries.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.course.courseLibraries.index') }}">
                {{ __('Course Libraries') }}
            </a>
        </li> --}}

    </ul>
	</div>
</nav>

@extends('twill::layouts.free')

@section('appTypeClass', 'body--custom-page')

@push('extra_css')
	<style>

		a17-dialog {
			display: none;
		}
.custom-page {
    padding-top: 0px;
}
.col--primary-main {
width: 82%;
margin-left: auto;
margin-right:auto;
}
.custom-tabs{
    border-radius: 2px;
    border: 1px solid #e5e5e5;
    margin-bottom: 20px;
    background: #fff;
    margin-top:30px;
}

.custom-tabs .box__filter{
position: relative;
    height: 50px;
    margin: -1px;
    white-space: nowrap;
    background: #e5e5e5;
    border-radius: 2px;
   padding-left:10px;
}

.custom-tabs .box__filter a {
display: inline-block;
height: 35px;
line-height: 35px;
text-decoration: none;
padding: 0 20px;
border-radius: 17px;
color: #3278b8;
background: none;
}


.custom-tabs .box__filter a.s--on {
background: #ffffff;
color: #262626;
}

.custom-tabs .custom-tab-content{
    padding: 0 20px 20px 20px;
}
    .modal {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1050;
    display: none;
    width: 100%;
    height: 100%;
    overflow: hidden;
    outline: 0;
     background: rgba(0, 0, 0, .66);
}

.modal__window {
    background: #fff;
    width: calc(100vw - 40px);
    max-width: 650px;
    position: relative;
    border-radius: 2px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-flow: column nowrap;
    flex-flow: column nowrap;
    margin: auto;
    top:20%;
}

.modal__header {
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    background: #d9d9d9;
    padding: 0 20px;
    height: 50px;
    line-height: 50px;
    position: relative;
    font-weight: 600;
}
.modal__content{
    overflow: hidden;
    overflow-y: auto;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    max-height: 100%;
}
.modal--form .modal__content {
    padding-bottom: 20px;
}
.button-unitEdit{
    background: none;
    position: absolute;
   border: none;
   margin-left:0.5%;
}
.input__field {
    position: relative;
    overflow: hidden;
    padding: 0 15px;
    height: 45px;
    line-height: 45px;
    border-radius: 2px;
    -webkit-box-shadow: inset 0 0 1px #f9f9f9;
    box-shadow: inset 0 0 1px #f9f9f9;
    width: 100%;
    border: 0 none;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    font-size: 15px;
    caret-color: #3278b8;
    background-color: #fbfbfb;
    border: 1px solid #d9d9d9;
    color: #666;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -ms-flex-wrap: no-wrap;
    flex-wrap: no-wrap;
}
.input__field:hover {
    border-color: #a6a6a6;
    color: #666;
    outline: 0;
    background-color: #fff;
}
.button{
    -webkit-appearance: none;
    cursor: pointer;
    font-size: 1em;
    outline: none;
    margin: 0;
    border: 0 none;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    letter-spacing: inherit;
    display: inline-block;
    border-radius: 2px;
    padding: 0 30px;
    height: 40px;
    line-height: 38px;
    text-align: center;
    -webkit-transition: color .2s linear, border-color .2s linear, background-color .2s linear;
    transition: color .2s linear, border-color .2s linear, background-color .2s linear;
    text-decoration: none;
    }
.button--validate {
background: #1d9f3c;
color: #fff;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}
.accordion.accordion-flush{
    margin: 0 20px 10px 20px;
    border: 1px solid #e5e5e5;
    border-radius: 5px;
}

.accordion-flush .accordion-item {
    border-right: 0;
    border-left: 0;
    border-radius: 0;
}

.accordion-item {
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, .125);
}
.accordion-header{
    display:flex;
}
.accordion-flush .accordion-item:last-child {
    border-bottom: 0;
}
.accordion-flush .accordion-item:first-child {
    border-top: 0;
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, border-radius .15s ease;
}
.accordion-item:last-of-type .accordion-button.collapsed {
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px);
}
.accordion-flush .accordion-item .accordion-button {
    border-radius: 0;
}
.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: '\02795';
    transition: transform .2s ease-in-out;
}

.accordion-button:not(.collapsed)::after {
    content: "\2796";
}

.accordion-button:not(.collapsed) {
    color: #0c63e4;
    background-color: #e7f1ff;
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.125);
}

.accordion-item:last-of-type .accordion-collapse {
    border-bottom-right-radius: .25rem;
    border-bottom-left-radius: .25rem;
}



.accordion-body {
    padding: 1rem 1.25rem;
}

.modal__content .input[data-v-74021712] {
    margin-top: 10px;
    position: relative;
    }
.collapse:not(.show) {
    display: none;
}

.filter-select {
    height: 43px !important;
    margin-bottom: 2rem;
    border: 1px solid #a6a6a6;
    width: 100%;
    border-color: #a6a6a6;
}
.custom-tab.custom-tab-content .container{
    margin-top: 20px;
}

.button-sectionEdit.button-unitEdit.button-modalComponentDelete {
   right: 15%;
    margin-top: 9px;
}

.modalComponentDelete .modal__content form {
    text-align: center;
}
.button--validate.button-destroy {
    background: #e31a22;
}
.breadcrumb {
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 0;
    list-style: none;
    background-color: #e9ecef;
    border-radius: 0.25rem;
    -webkit-border-radius: 0.25rem;
    -moz-border-radius: 0.25rem;
}
.breadcrumb {
    margin-top: 20px;
    background-color: transparent;
}
.breadcrumb {
    background-color: none;
    overflow: hidden;
    height: 45px;
}
.breadcrumb-item a {
    color: #262626;
    text-decoration: none;
    font-size:13px;
}
.breadcrumb-item + .breadcrumb-item::before {
    float: left;
    padding-right: 5px;
    color: #262626;
    content: "|";
    padding-left: 5px;
}
.breadcrumb-item.active a {
    color: #262626;
    text-decoration: none;
}
.button-subsection.button-unitEdit i{
    font-size:14px;
}
.button--validate.button-publish{
    font-size:14px;
}
	</style>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@push('extra_js_head')
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@endpush

@section('content')
	@include('layouts.admin._admin_partials.units_nav')
	@include('twill::partials.navigation._primary_navigation')
	@include('twill::partials.navigation._secondary_navigation')
	@include('twill::partials.navigation._breadcrumb')
  <div class="custom-page">
    <div class="col col--primary-main">
      @yield('customPageContent')
    </div>
  </div>

@stop

@section('initialStore')
    window['{{ config('twill.js_namespace') }}'].STORE.medias.crops = {!! json_encode(config('twill.settings.crops') ?? []) !!}
    window['{{ config('twill.js_namespace') }}'].STORE.medias.selected = {}

    window['{{ config('twill.js_namespace') }}'].STORE.browser = {}
    window['{{ config('twill.js_namespace') }}'].STORE.browser.selected = {}
@stop

@push('extra_js')

@includeWhen(config('twill.block_editor.inline_blocks_templates', true), 'twill::partials.form.utils._blocks_templates')
<script src="{{ twillAsset('main-form.js') }}" ></script>
{{-- <script src="{{asset('/assets/tinymce/tinymce.min.js')}}" ></script> --}}
<script src="https://cdn.tiny.cloud/1/6bsc1mvohbwizj16uzo1qzoapmx277n4uwyq62qsupx1tt3a/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
$(document).on("click", ".button-subsection.button-unitEdit", function () {
     var myBookId = $(this).data('id');
     var myName = $(this).data('name');
     var myType = $(this).data('type');


     console.log(myBookId, myName, myType);
     $(".unitModal .modal__content #parentLocator").val( myBookId );
    $(".unitModal .modal__content input[name=section_name]").val( myName );
    $(".unitModal .modal__content #creationType").val( myType );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
</script>

{{-- images_upload_url: '/postAcceptor.php',
images_reuse_filename: true, --}}
<script>

  tinymce.init({
    selector: 'textarea#editor',
    plugins: 'anchor autolink charmap emoticons image media link lists table wordcount code template',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons code | removeformat template',
    file_picker_types: 'file image media',
    height:"600px",
    content_css: '../../../../../assets/admin/css/image_custom.css',
    image_title: true,
    automatic_uploads: true,
    convert_urls:false,
    image_class_list: [
    { title: 'Left', value: '' },
    { title: 'Right', value: 'w-full md:float-right' }
],
    file_picker_callback: (cb, value, meta) => {
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    input.addEventListener('change', (e) => {
      const file = e.target.files[0];

      const reader = new FileReader();
      reader.addEventListener('load', () => {
        /*
          Note: Now we need to register the blob in TinyMCEs image blob
          registry. In the next release this part hopefully won't be
          necessary, as we are looking to handle it internally.
        */
        const id = 'blobid' + (new Date()).getTime();
        const blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        const base64 = reader.result.split(',')[1];
        const blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        /* call the callback and populate the Title field with the file name */
        cb(blobInfo.blobUri(), { title: file.name });
      });
      reader.readAsDataURL(file);
    });

    input.click();
  },
    templates: [
    {title: 'Image Left Text right', description: 'Image left Text right', content: '<div class="row" style="display:flex;width:100%;max-width:100%"><div class="col-md-3 how-img"><img src="https://image.ibb.co/dDW27U/Work_Section2_freelance_img1.png" class="img-fluid" alt=""/></div><div class="col-md-9"><h4>Title here</h4><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div></div><p style="line-height: 1.5;">&nbsp;</p>'},
    {title: 'Image Right Text left', description: 'Image right rext left', content:' <div class="row" style="display:flex;width:100%max-width:100%"><div class="col-md-9"><h4>Title here</h4><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div class="col-md-3 how-img"><img src="https://image.ibb.co/dDW27U/Work_Section2_freelance_img1.png" class="img-fluid" alt=""/></div></div><p style="line-height: 1.5;">&nbsp;</p>'},
    {title: 'Course Wrap Up', description: 'Course conclusion instructions', content:' <h2><b>Congratulations on completing the module!</b></h2><p style="margin-top:23px">Please click on the <a id="myProgressLink" href="//academy.towersacco.co.ke/profile/myProfile"><span style="color:#00A8C1"><b>My Progress</b></span></a> icon to view your scores and evaluate the course </p><br/><p>Please note that you may only download a certificate if you have attained the overall passmark of 80%.</p>'}
  ]
  });
</script>


<script>
$(document).on("change", "#blockOptions", function () {

           var data_id = $(this).val();
           var block_id = $('#block_id').val();

           var url = "{{ route('admin.courseUnit.createUnitComponent') }}";
            var token = "{{ csrf_token() }}";

                var formData = {
                    'type_id': data_id,
                    'block_id': block_id,
                    '_token': token,
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: formData,
                    success: function(res) {
                        if (res.success) {

                            sessionStorage.setItem("status", res.success);
                               location.reload();

                        } else if (res.error) {
                            $('#my_modal').modal('hide');
                            $('.modal-backdrop').remove();

                             sessionStorage.setItem("status",res.error);
                            setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                    },

                });

});

</script>

<script>
$(document).on("click", ".button-sectionEdit.button-modalComponentDelete", function () {
     var myBookId = $(this).data('id');
     var myType = $(this).data('type');
      var myCategory = $(this).data('category');

     $(".modalComponentDelete #categoryTitle").html(myCategory);
     $(".modalComponentDelete .modal__content #parentLocator").val( myBookId );
      $(".modalComponentDelete .modal__content #creationType").val( myType );
       $(".modalComponentDelete .modal__content #categoryType").val( myCategory );
     // As pointed out in comments,
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});


</script>


@endpush

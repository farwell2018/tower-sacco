@twillRepeaterTitle('Downloable Resource')
@twillRepeaterTrigger('Add a Downloable Resource')
@twillRepeaterGroup('app')

@formField('input', [
    'name' => 'document_title',
    'label' => 'Document Title',
    'required' => true,
])

@formField('files', [
		'name' => 'download_resource',
		'label' => 'Downloadable PDF',
		'note' => 'This applies for resources with downloadable resources',
])
@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 100
    ])

@formField('checkboxes', [
    'name' => 'roles',
    'label' => 'Roles',
    'note' => 'At least 1 roles',
    'min' => 1,
    'inline' => true,
    'options' => collect($roleTypeList ?? '')
])
@stop

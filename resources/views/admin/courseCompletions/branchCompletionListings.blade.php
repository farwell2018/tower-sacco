@extends('layouts.admin.reports')

@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }

        .container {
            width: 100% !important;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('customPageContent')
    <div class="listing">
        <div class="row">

            <div class="col-12">
                <form action="{{ route('admin.branch.completionList', ['branch_id' => $branched]) }}" method="POST" class="homeForm">
                    @csrf
                    <div class="row justify-content-center">

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <div class="col-9 no-padding-right ">
                            <div class="row">

                                {{-- <div class="col-4">
                                    <select name="country" id="" class="form-control mt-4 mb-4 ">
                                        <option value=''>Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                <div class="col-5">

                                    <input type="text" placeholder="Free text search ... eg email" class="form-control mt-4 mb-4 " name="search_list">

                                </div>

                            </div>

                        </div>

                        <div class="col-3 ">
                            <button type="submit"
                                class="mt-4 mb-4" style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;">Search</button>

                            <button type="reset" class=" mt-4 mb-4" style="
                            margin-top: 40px;
                            margin-left: 20px;
                            color: rgb(255, 255, 255);
                            background: rgb(255, 0, 0);
                            border:1px solid rgb(255, 0, 0);
                            padding: 10px;
                            text-decoration: none;"
                                onClick="window.location.href=window.location.href">Reset</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div class="row">
            <div class="col">
                <div class="text-right mb-3">
                    <button class="btn btn-primary" onclick="exportTableToExcel()">Export</button>
                </div>
                <table class="table" style="text-align: center">
                    <thead>
                        <tr class="tablehead">
                            <th>#</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Courses Completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($users as $user)
                            <tr class="tablerow">
                                <td class="tablecell">{{ $i++ }}</td>
                                <td class="tablecell">{{ $user->email }}</td>
                                <td class="tablecell">{{ $user->CompletedStatus }}</td>
                                <td>{{ $user->findComplete() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{-- Add pagination links --}}
                {{ $users->links() }}
            </div>
        </div>
    </div>
@stop

@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.0/xlsx.full.min.js"></script>
    <script>
        function exportTableToExcel() {
            // Get table element
            var table = document.querySelector('.table');

            // Clone the table to preserve the original formatting
            var clonedTable = table.cloneNode(true);

            // Remove unnecessary columns or modify the table as needed

            // Convert table to worksheet
            var ws = XLSX.utils.table_to_sheet(clonedTable);

            // Create a workbook and add a worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

            var timestamp = new Date().toISOString().replace(/[-T:\.Z]/g, '');
            var filename = 'TowerSacco_Completions_' + '{{ $branchName }}_Branch_' + timestamp + '.xlsx';

            // Save the workbook as an Excel file with the specified filename
            XLSX.writeFile(wb, filename);
        }
    </script>
@endpush
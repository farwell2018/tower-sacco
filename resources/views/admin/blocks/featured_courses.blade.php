@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])

@formField('input', [
    'name' => 'course_number',
    'label' => 'Number of courses to show',
    
])

@formField('input', [
    'name' => 'block_background_color',
    'label' => 'Block Background Color',
    
])

@formField('input', [
    'name' => 'block_text_color',
    'label' => 'Block text Color',
    
])


@formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    
])
@formField('input', [
    'name' => 'url',
    'label' => 'URL',
	
])

@formField('select', [
    'name' => 'block_variant',
    'label' => 'Variant',
    'placeholder' => 'Select a variant',
    'options' => [
        [
            'value' => 'orange_bg',
            'label' => 'Orange Background'
        ],
        [
            'value' => 'white_bg',
            'label' => 'White Background'
        ],
        [
            'value' => 'white_bg_orange_border',
            'label' => 'White Background Orange Border'
        ]
    ]
])
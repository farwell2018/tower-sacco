@formField('medias', [
    'name' => 'cta_text_image',  //role
    'label' => 'Image',
	'withVideoUrl' => false,
	'max' => 1,
])

@formField('wysiwyg', [
    'translated' => true,
    'name' => 'image_subtitle',
    'label' => 'Text',
    'maxlength' => 1000,
    'editSource' => true,
    'note' => 'You can edit the source.',
])
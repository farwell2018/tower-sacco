
<form action="{{ route('admin.courseCreation.advanced') }}" method="POST" @if($customForm) ref="customForm" @else
    v-on:submit.prevent="submitForm" @endif>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <div class="container">
        <div class="wrapper-inner wrapper--reverse" v-sticky data-sticky-id="publisher" data-sticky-offset="80">

            <section class="col col--primary" data-sticky-top="publisher" style="margin-bottom:30px">

                @formField('input', [
                'name' => 'advanced_modules',
                'label' => 'Advanced Module List',
                'note' => 'Enter the names of the advanced modules to use in your course. Default values are openassessment,scormxblock,library_content,freetextresponse,videoalpha',
                'type' => 'textarea',
                'rows' => 3,
                "placeholder" => 'openassessment,scormxblock,library_content,freetextresponse,videoalpha',
                'required'  => true
                ])

                @formField('input', [
                'name' => 'display_name',
                'label' => 'Course Display Name',
                'required' => true,
                'note' => 'Edit the name of the course as it should appear.',
                ])
                  @formField('select', [
                'name' => 'certificates_display_behavior',
                'label' => 'Certificates Display Behavior',
                'note' => 'To display only the links to passing students as soon as certificates are generated, enter early_no_info.',
                 'required' => true,
                'options' => [
                    [
                        'value' => 'early_no_info',
                        'label' => 'Early'
                    ],
                    [
                        'value' => 'end',
                        'label' => 'End'
                    ]
                ]
                ])

               @formField('select', [
                'name' => 'mobile_available',
                'label' => 'Mobile Course Available',
                'note' => 'Enter true or false. If true, the course will be available to mobile devices.',
                 'required' => true,
                'options' => [
                    [
                        'value' => 'true',
                        'label' => 'True'
                    ],
                    [
                        'value' => 'false',
                        'label' => 'False'
                    ]
                ]
                ])

                 @formField('select', [
                'name' => 'enable_subsection_gating',
                'label' => 'Enable Subsection Prerequisites',
                'note' => 'Enter true or false. If this value is true, you can hide a subsection until learners earn a minimum score in another, prerequisite subsection.',
                'required' => true,
                 'options' => [
                    [
                        'value' => 'true',
                        'label' => 'True'
                    ],
                    [
                        'value' => 'false',
                        'label' => 'False'
                    ]
                ]
                ])



            </section>

            <aside class="col col--aside">
                <div class="publisher" data-sticky-target="publisher">
                    <a17-publisher>
                        @yield('publisherRows')
                    </a17-publisher>
                    <a17-page-nav placeholder="Go to page" previous-url="{{ $parentPreviousUrl ?? '' }}"
                        next-url="{{ $parentNextUrl ?? '' }}"></a17-page-nav>
                    @hasSection('sideFieldset')
                    <a17-fieldset title="{{ $sideFieldsetLabel ?? 'Options' }}" id="options">
                        @yield('sideFieldset')
                    </a17-fieldset>
                    @endif
                    @yield('sideFieldsets')
                </div>
            </aside>
        </div>
    </div>
    <a17-spinner v-if="loading"></a17-spinner>
</form>

@extends("layouts.admin.outer_form")

@push("extra_css")
<style>

</style>
@endpush

@php
$customForm = true;


@endphp




@section("contentFields")
<a17-custom-tabs :tabs="[
            { name: 'settings', label: 'Schedule & details' },
            { name: 'advanced', label: 'Advanced Settings' },
            { name: 'grading', label: 'Grading' },
            { name: 'content', label: 'Content' },
        ]">


    <div class="custom-tab custom-tab-content custom-tab--settings">

      @include('admin.courseCreations.settings', [$item])

    </div>

    <div class="custom-tab custom-tab-content custom-tab--advanced">

        @include('admin.courseCreations.advanced', [$item])

    </div>

    <div class="custom-tab custom-tab-content custom-tab--grading">
        @include('admin.courseCreations.grading', [$item])
    </div>

    <div class="custom-tab custom-tab-content custom-tab--content">
        @include('admin.courseCreations.content', [$item])
    </div>

</a17-custom-tabs>
@stop




<form action="{{ route('admin.courseCreation.grading') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <div class="container">
        <div class="wrapper-inner wrapper--reverse" v-sticky data-sticky-id="publisher" data-sticky-offset="80">

            <section class="col col--primary" data-sticky-top="publisher">
                        @formField('input', [
                        "name" => "pass_mark",
                        "class" => "form-control spacing-input",
                        "label" => "Overall Pass Mark Grade",
                        'note' => "The overall pass mark as a percentage of the total grade, for example, 70. Do not include the percent symbol.",
                        ])

            @formField('block_editor', [

            'blocks' => ['course_grade']

            ])

            </section>

            <aside class="col col--aside">
                <div class="publisher" data-sticky-target="publisher">
                    <a17-publisher>
                        @yield('publisherRows')
                    </a17-publisher>
                    <a17-page-nav placeholder="Go to page" previous-url="{{ $parentPreviousUrl ?? '' }}"
                        next-url="{{ $parentNextUrl ?? '' }}"></a17-page-nav>
                    @hasSection('sideFieldset')
                    <a17-fieldset title="{{ $sideFieldsetLabel ?? 'Options' }}" id="options">
                        @yield('sideFieldset')
                    </a17-fieldset>
                    @endif
                    @yield('sideFieldsets')
                </div>
            </aside>
        </div>
    </div>
    <a17-spinner v-if="loading"></a17-spinner>
</form>

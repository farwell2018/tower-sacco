@php
$chapterList = $chapters['child_info']['children']
@endphp


@foreach($chapterList as $key => $chapter)

<div class="accordion accordion-flush" id="accordionFlushExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-heading-{{$key}}">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-{{$key}}" aria-expanded="false" aria-controls="flush-{{$key}}">
        {{$chapter['display_name']}}
      </button>
      <button class="button button-sectionEdit" data-toggle="modal" data-target="#modalSection" data-id="{{$chapter['id']}}" data-name="{{$chapter['display_name']}}" data-type="edit" ><i class="fa fa-pencil" aria-hidden="true"></i></button>
       <button class="button button-sectionEdit button-modalDelete" data-toggle="modal" data-target="#modalDelete" data-id="{{$chapter['id']}}" data-type="delete" data-category="Section"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </h2>
    <div id="flush-{{$key}}" class="accordion-collapse collapse" aria-labelledby="flush-heading-{{$key}}" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">

         @if(count($chapter['child_info']['children']) > 0)
          @include('admin.courseCreations.components.subsectionPanels', ['chapter' =>$chapter])
        @else

            <p style="text-align:center;padding:10px"> <button class="button button--small button--validate button-subsection" data-toggle="modal" data-target="#modalSubsection" data-id="{{$chapter['id']}}">New Subsection</button> </p>

        @endif
      </div>
    </div>
  </div>
</div>
@endforeach

<p style="text-align:center;padding:10px"> <button class="button button--small button--validate" data-toggle="modal" data-target="#modalSection">New Section</button> </p>



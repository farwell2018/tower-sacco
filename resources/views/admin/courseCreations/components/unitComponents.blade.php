@extends("layouts.admin.outer_unit")


@php
$unit = getUnitOutline($locator);
$configLms = config()->get("settings.lms.live");


$components = $unit['child_info']['children'];

@endphp

@section('customPageContent')
<ol class="breadcrumb">

    <li class="breadcrumb-item">
        <a href="{{ url('admin/course/courseCreation/'.$item.'/edit#content') }}">{{$structure->display_name}}</a>
    </li>

    <li class="breadcrumb-item active">
        <a href="{{ route('admin.unit.creation', ['item' => $item, 'locator'=> $locator]) }}"> {{$unit['display_name']}}</a>
    </li>

</ol>
<h1 style="font-size:18px">
{{$unit['display_name']}}
<button class="button button-subsection button-unitEdit" data-toggle="modal" data-target="#modalUnit" data-id="{{$unit['id']}}" data-name="{{$unit['display_name']}}" data-type="edit" style="padding:0 10px; height:unset; line-height:unset" ><i class="fa fa-pencil" aria-hidden="true"></i></button>

<form action="{{ route('admin.courseUnit.publish') }}" novalidate method="POST" style="float: right;margin-top: -20px;">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="item_id" value="{{ $item}}">
  <input type="hidden" name="unit_id" value="{{$unit['id']}}">

<button class="button button--small button--validate button-publish" type="submit" style="margin-top:10px">Publish</button>
</form>

@if($unit['published'] == true)

 <a href="{{$configLms['LMS_BASE'].'/courses/'.$structure->course_id.'/jump_to/'.$unit['id']}}" class="button button--small button--validate button-publish" style="float: right; margin-top: -10px; margin-right:10px" target="_blank">View Live Version</a>
@endif

</h1>


<a17-custom-tabs :tabs="[
            { name: 'settings', label: 'Unit Components' },
        ]">

 <div class="custom-tab custom-tab-content custom-tab--settings">
      <div class="container">
       @if($components)
        @foreach ($components as $key => $value )

         @if ($value['category'] === 'html')

          @include('admin.courseCreations.components.singleComponentHtml', ['component' => getSingleComponents($value['id']), 'item' => $item, 'unit'=>$unit['id']])

        @elseif ($value['category'] === 'scormxblock')

            @include('admin.courseCreations.components.singleComponentScorm', ['component' => getSingleComponents($value['id']), 'item' => $item, 'unit'=>$unit['id']])

        @elseif ($value['category'] === 'library_content')

              @include('admin.courseCreations.components.singleComponentLibrary', ['component' => getSingleComponents($value['id']), 'item' => $item, 'unit'=>$unit['id']])

        @endif

        @endforeach
       @endif
       <div id="blockSection"  @if(isset($components)) style="margin-top:80px;"  @else style="margin-top:20px;" @endif>
       <input type="hidden" id="block_id" value="{{$unit['id']}}" />
        <label >Add New Component </label><br><br>
        <select class="form-control filter-select" name="blockOptions" id="blockOptions" >
        <option value="">Select Option</option>
        <option value="html">Text Block</option>
        <option value="scormxblock">Scorm Block</option>
        <option value="library_content">Randomized Content Block</option>
        </select>
        </div>
    </div>
    <div>


<div class="modal modal--form unitModal" id="modalUnit" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Create Unit
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseStructure.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item}}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" value="vertical">
    <div style="padding:10px">

    <input type="text" class="form-control spacing-input input__field" placeholder="Display Name" id="sectionName" name="section_name" required/>

    <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
                    </div>
    </form>

     </div>
     </div>
</div>


<div class="modal modal--form modalComponentDelete" id="modalComponentDelete" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Delete this <span id="categoryTitle"></span>
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseUnit.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item}}">
    <input type="hidden" name="unit_id" value="{{ $unit['id']}}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" id="categoryType">

   <p style="margin-top:20px">Deleting this <span id="categoryTitle"></span> is permanent and cannot be undone.</p>

    <button class="button button--small button--validate button-destroy" type="submit" style="margin-top:10px">Yes, delete this <span id="categoryTitle"></span></button>
                    </div>
    </form>

     </div>
     </div>
</div>

@stop

@push('extra_css')
<style>
 .input[data-v-74021712] {
    margin-top: 0 !important;
    position: relative;
    }
.input__label{
    display:none !important;
}

</style>

@endpush

<div class="accordion accordion-flush" id="accordionFlushExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-heading-{{$key}}">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-{{$key}}" aria-expanded="false" aria-controls="flush-{{$key}}">
        {{$component['display_name']}}
      </button>
       <button class="button button-unitEdit button-sectionEdit button-modalComponentDelete" data-toggle="modal" data-target="#modalComponentDelete" data-id="{{$component['id']}}" data-type="delete" data-category="{{$component['display_name']}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </h2>
    <div id="flush-{{$key}}" class="accordion-collapse collapse" aria-labelledby="flush-heading-{{$key}}" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
         <form action="{{ route('admin.courseUnit.blocks') }}"  method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="item_id" value="{{ $item}}">
             <input type="hidden" name="unit_id" value="{{ $unit}}">
            <input type="hidden" name="parent" value="{{ $component['id'] }}">
            <input type="hidden" name="type" id="edit" value="edit">
            <input type="hidden" name="category" value="{{$component['category']}}">
            
               <div style="margin-top:20px;">
                <label >Display Name</label><br>
                <input type="text" name="display_name" value="{{$component['display_name']}}" placeholder="Display Name" class="input__field"/>
            </div>


             <div style="margin-top:20px;">
                <label >Display Height(px)</label><br>
                <input type="text" name="height" value="{{(isset($component['metadata']['height'])) ? $component['metadata']['height'] : ''}}" placeholder="Display Height" class="input__field"/>
            </div>

             <div style="margin-top:20px;">
                <label >Upload Scorm File</label><br>
                <input type="file" name="scorm_file"  placeholder="Scorm file" class="input__field" accept="application/zip"/>
            </div>
           
         
          <br>
            <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
         
         </form>
      </div>
    </div>
  </div>
</div> 
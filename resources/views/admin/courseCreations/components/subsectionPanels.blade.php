@php
$subsectionList = $chapter['child_info']['children'];
$graders = getCourseGrader($item->course_id);

//dd($subsectionList)
@endphp


@foreach($subsectionList as $key => $section)



<div class="accordion accordion-flush" id="accordionSubsection">
  <div class="accordion-item">
    <h2  id="flush-heading-subsection-{{$key}}">
      <div class="accordion-header">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-subsection-{{$key}}" aria-expanded="false" aria-controls="flush-subsection-{{$key}}">
        {{$section['display_name']}}
      </button>
     @if($item->enable_subsection_gating == true)
      <button class="button button-subsectionEdit" data-toggle="modal" data-target="#modalSubsection" data-id="{{$section['id']}}" data-name="{{$section['display_name']}}" data-type="edit"
       data-format="{{$section['format']}}" data-is_prereq="{{$section['is_prereq']}}" data-prereqs="{{json_encode($section['prereqs'])}}" data-graders="{{json_encode($graders["graders"])}}" data-enabled="{{$item->enable_subsection_gating}}"
       data-prerequisite="{{$section['prereq']}}" data-prereqScore="{{$section['prereq_min_score']}}" data-prereqCompletion="{{$section['prereq_min_completion']}}"><i class="fa fa-pencil" aria-hidden="true"></i></button>

       @else
       <button class="button button-subsectionEdit" data-toggle="modal" data-target="#modalSubsection" data-id="{{$section['id']}}" data-name="{{$section['display_name']}}" data-type="edit"
       data-format="{{$section['format']}}" data-graders="{{json_encode($graders["graders"])}}" data-enabled="false"><i class="fa fa-pencil" aria-hidden="true"></i></button>


       @endif

      <button class="button button-subsectionEdit button-sectionEdit button-modalDelete" data-toggle="modal" data-target="#modalDelete" data-id="{{$section['id']}}" data-type="delete" data-category="Subsection"><i class="fa fa-trash" aria-hidden="true"></i></button>
       </div>
      @if($section['graded'])
       <p class="grader-show"> Grader: {{$section['format']}} <p>
       @endif
    </h2>
    <div id="flush-subsection-{{$key}}" class="accordion-collapse collapse" aria-labelledby="flush-heading-subsection-{{$key}}" data-bs-parent="#accordionSubsection">
      <div class="accordion-body">



         @if(count($section['child_info']['children']) > 0)
           @include('admin.courseCreations.components.unitPanel', ['section' =>$section])

            <p style="text-align:center;padding:10px"> <button class="button button--small button--validate button-subsection button-unit" data-toggle="modal" data-target="#modalUnit" data-id="{{$section['id']}}">New Unit</button> </p>

        @else

            <p style="text-align:center;padding:10px"> <button class="button button--small button--validate button-subsection button-unit" data-toggle="modal" data-target="#modalUnit" data-id="{{$section['id']}}">New Unit</button> </p>

        @endif
      </div>
    </div>
  </div>
</div>



@endforeach

<p style="text-align:center;padding:10px"> <button class="button button--small button--validate button-subsection" data-toggle="modal" data-target="#modalSubsection" data-id="{{$chapter['id']}}">New Subsection</button> </p>









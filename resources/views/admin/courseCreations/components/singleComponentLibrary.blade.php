@php

$libaries = getLibraries();



@endphp

 @push('extra_css')
<style>
 .input[data-v-74021712] {
    margin-top: 0 !important;
    position: relative;
    }
.input__label{
    display:none !important;
}

</style>

@endpush

<div class="accordion accordion-flush" id="accordionFlushExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-heading-{{$key}}">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-{{$key}}" aria-expanded="false" aria-controls="flush-{{$key}}">
        {{$component['display_name']}}
      </button>
       <button class="button button-unitEdit button-sectionEdit button-modalComponentDelete" data-toggle="modal" data-target="#modalComponentDelete" data-id="{{$component['id']}}" data-type="delete" data-category="{{$component['display_name']}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </h2>
    <div id="flush-{{$key}}" class="accordion-collapse collapse" aria-labelledby="flush-heading-{{$key}}" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
         <form action="{{ route('admin.courseUnit.blocks') }}" novalidate method="POST">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="item_id" value="{{ $item}}">
             <input type="hidden" name="unit_id" value="{{ $unit}}">
            <input type="hidden" name="parent" value="{{ $component['id'] }}">
            <input type="hidden" name="type" id="edit" value="edit">
            <input type="hidden" name="category" value="{{$component['category']}}">

            <div style="margin-top:20px;">
                <label >Count</label> <br><small style="font-size:12px;">Enter the number of components to display to each student.</small><br>
                <input type="text" name="count" value="{{(isset($component['metadata']['max_count'])) ? $component['metadata']['max_count'] : ''}}" placeholder="Enter the number of components to display to each student." class="input__field"/>
            </div>

             <div style="margin-top:20px;">
                <label >Display Name</label><br>
                <input type="text" name="display_name" value="{{$component['display_name']}}" placeholder="count" class="input__field"/>
            </div>
           
            <div id="blockSection" style="margin-top:20px;">
        <label >Library </label><br><small style="font-size:12px;">Select the library from which you want to draw content.</small><br>
        <select class="form-control filter-select" name="blockOptions" >
        <option value="">Select the library from which you want to draw content.</option>
         @foreach ($libaries as $library)
            @if($library['display_name'] != "Test")
              
              @if(isset($component['metadata']['source_library_id']) && $component['metadata']['source_library_id'] === $library['library_key'])
                 <option value="{{$library['library_key']}}" selected>{{$library['display_name']}}</option>
              @else

             <option value="{{$library['library_key']}}">{{$library['display_name']}}</option>
             @endif
             @endif
         @endforeach
        </select>
        </div>
            
          <br>
            <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
         
         </form>
      </div>
    </div>
  </div>
</div> 
@extends('twill::layouts.form')

@section('contentFields')
@formField('input', [
    'name' => 'icon_name',
    'label' => 'Icon Name',
])
@stop

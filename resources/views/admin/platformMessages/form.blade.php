@extends('twill::layouts.form')

@section('contentFields')
    @formField('multi_select', [
    'name' => 'role',
    'label' => "Select Role to send message to (if it's everyone select All)",
    'placeholder' => 'Select Course',
    'min'=> 1,
    'unpack' => false,
    'options' => collect($roleList ?? ''),
    ])


    @formField('input', [
    'name' => 'subject',
    'label' => 'Subject',
    'maxlength' => 100
    ])

    @formField('wysiwyg', [
    'name' => 'message',
    'label' => 'Message',
    'toolbarOptions' => [
    ['header' => [2, 3, 4, 5, 6, false]],
    'bold',
    'italic',
    'underline',
    'strike',
    ["script" => "super"],
    ["script" => "sub"],
    "blockquote",
    "code-block",
    ['list' => 'ordered'],
    ['list' => 'bullet'],
    ['indent' => '-1'],
    ['indent' => '+1'],
    ["align" => []],
    ["direction" => "rtl"],
    'link',
    'image',
    "clean",
    ],
    'placeholder' => 'Message',
    'maxlength' => 2000,

    ])
@stop

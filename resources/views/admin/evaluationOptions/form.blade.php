@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
    'name' => 'type',
    'label' => 'Type',
    'placeholder' => 'Select Type',
    'options' => [
        [
            'value' => 'text',
            'label' => 'Text'
        ],
        [
            'value' => 'bar',
            'label' => 'Bar'
        ],
        [
            'value' => 'radio',
            'label' => 'Radio'
        ]
    ]
])


@formConnectedFields([
    'fieldName' => 'type',
    'fieldValues' => 'radio',
    
])
    @formField('input', [
        'name' => 'value1',
        'label' => 'Value 1'
    ])
     @formField('input', [
        'name' => 'value2',
        'label' => 'Value 2'
    ])
     @formField('input', [
        'name' => 'value3',
        'label' => 'Value 3'
    ])
     @formField('input', [
        'name' => 'value4',
        'label' => 'Value 4'
    ])
     @formField('input', [
        'name' => 'value5',
        'label' => 'Value 5'
    ])
@endformConnectedFields
@stop

<section class="main-page track-record">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
    
                <div class="header-section">
                <h1>{{ $block->translatedinput('track_title') }}</h1>
                <h5 class="text-purple">{{ $block->translatedinput('track_description') }}</h5>
                </div>
    
                <div class="body-section">
                    <div class="row">
                    <div class="col-md-4 counter-section">
                      <h1><span class="count" id="count">{{ $block->translatedinput('number') }}</span> </h1>
                      <h5>{{ $block->translatedinput('number_title') }}</h5>
                    </div>
                    <div class="col-md-8">
                    {!! $block->translatedinput('description') !!}
                    </div>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
    </section>
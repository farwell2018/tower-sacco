<section id="whatYouGet" class="whatYouGet text_section bgImg">
	<div class="container">
		<div class="hrHeading">
			<h2 class="line-header">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>

		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="whatYouGetListing">
					{!! $block->translatedinput('description') !!}
					@foreach ($block->children as $block)
						@include('site.blocks.cta_link',
						 [
							'block'=>$block,
							'isBanner'=>true,
						])
					@endforeach
				</div>
			</div>
		</div>

	</div>
</section>

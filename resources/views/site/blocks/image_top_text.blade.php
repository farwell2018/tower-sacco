
@php

$image = $block->imageObject('cta_text_image');
$ratio = $image->ratio ?? 2/3;
$ratioPercentage = ($ratio * 100);
@endphp
<section >

    <div class="col-md-12 ">

        <div class="image-section"> 
            <img src="{{ $block->image('cover', 'default') }}" alt="">
            {!!  (new \App\Helpers\Front)->responsiveImage([
              'options' => [
                'src' => $block->image('cta_text_image', 'default') ?? '',
                'w' => $image->width ?? 600,
                'h' => $image->height ?? 400,
                'ratio' => $ratio,
                'fit' => 'crop',
              ],
              'html' => [
                'alt' => $image->altText ?? ''
              ],
              'sizes' => [
                [ 'media' => 'small', 'w' => 730 ],
                [ 'media' => 'medium',  'w' => 426 ],
                [ 'media' => 'large', 'w' => 500 ],
                [ 'media' => 'xlarge',  'w' => 600 ],
                [ 'media' => 'xxlarge',  'w' => 680 ]
              ]
            ]) !!}

        
          </div>
		<div class="row">
			<div class="col-lg-12">
				<div class="whatYouGetListing">
                    <div class="clearfix">
                        <br />
                    </div>
					{!! $block->translatedinput('image_subtitle') !!}
					
				</div>
			</div>
		</div>
    
	</div>
</section>

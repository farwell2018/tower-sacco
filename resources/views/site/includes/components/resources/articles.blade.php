
<div class="container-fluid mb-3 mt-3">
 
    <div class="card resources mb-3">
      
      
        @if( $item->hasImage('resource_image'))
        <div class="card-img" style="background-image:url('{{$item->image("resource_image", "default")}}');"></div> 
        @else 
        @if(!empty($item->cover_image))
        <div class="card-img" style="background-image:url('{{asset('resources/cover_image/'.$item->cover_image)}}');"></div>
       
         @else 
         <div class="card-img" style="background-image:url('{{asset('resources/cover_image/default-thumbnail.png')}}');"></div>
         
         @endif
       
        @endif
        <div class="card-img-overlay d-flex flex-column  justify-content-center">
            <h4 class="card-title"><a href="{{route('resource.article',['id'=> $item->id])}}">{{$item->title}}</a> </h4>
        </div>
        </div>
        @if (Route::current()->getName() === 'home')
      
        @else
        <div class="container">
     <form action="{{ route('rate.resource') }}" method="POST">
     {{ csrf_field() }}
        <div class="rating">
        <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="{{ $item->userAverageRating }}" data-size="xs">
          <input type="hidden" name="id" required="" value="{{ $item->id }}">
           
          <button class="btn btn-success btn-sm">Submit Rating</button>
          </div>
    </form>
          
        </div>
    @endif
    
    </div>
    
    
    
    <script>
     $("#input-id").rating();
    
        </script>
    

    
    <div class="course-card mt-3 sessions-card webinar-card">
        <article>
          <div class="thumbnail">
    
            @if( $item->hasImage('resource_image'))
            <img src="{{$item->image("resource_image", "default")}}" alt="{{$item->title}}" />
           
            @endif
              <div class="overlay">
                <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->title}}">
                  <i class="fas fa-share"></i>
                </div>
              </div>
          </div>
      
          <div class="course-card-content session-card-content">
           
    
            <p class=" text-grey session-card-calendar">
              
              <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($item->created_at)->isoFormat('MMMM Do YYYY')}} </span>
              <span class="session-time"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($item->created_at)->format('H:i A')}} </span>
             
            </p>
            <p class="title ">{{$item->title}}</p>
            
            
            <p class="readmore ">
                <a href="{{$item->external_link}}" class="btn btn-overall btn_more_details" target="_blank">More Details </a>
            </p>
           
          </div>
    
        </article>
      </div>
      
    
      
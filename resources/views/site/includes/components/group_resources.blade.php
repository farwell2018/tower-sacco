<div class="col-md-12 no-padding-left">
    <div class="panel-group principle_content" id="accordion" role="tablist" aria-multiselectable="true">
        <?php
          $expand = (int)(app('request')->input('collapse') ?: 0);
        ?>
        @foreach($resourceForm as $section)

        <div id="panel{{$loop->index}}" class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading{{$loop->index}}">
            <h6 class="panel-title">
              <a
              role="button"
              data-parent="#accordion"
              href="{{url()->current()}}/?collapse={{$loop->index}}"
              name="#collapse{{$loop->index}}"
              aria-expanded="{{$loop->first ? 'true' : 'false'}}"
              aria-controls="collapse{{$loop->index}}"
              class = "{{($loop->index === $expand) ? '' : 'collapsed'}}"
              >
              {{$section->title}}
            </a>
          </h6>
          <div style="text-align:right;margin:0 1%;">

          </div>
        </div>
        <div id="collapse{{$loop->index}}" class="panel-collapse collapse {{($loop->index === $expand) ? 'in' : null}}" role="tabpanel" aria-labelledby="heading{{$loop->index}}">
          <div class="panel-body">
            <br/>
            {!! ($loop->index === $expand) ? $section->title : null !!}
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

<div class="course-card mt-3 sessions-card branch-card">
    <article>
        <div class="course-card-content session-card-content">
            <h4 class="text-center mt-3"> {{ $branch->title }}</h4>

            <table width="100%" class="mt-3 pl-3 pr-3">
                <tr>
                    <td><img src="{{ asset('images/icons/progress_registered.png') }}" /></td>
                    <td>Registered</td>
                    <td>{{ $branch->registered }}</td>
                </tr>

                <tr>
                    <td><img src="{{ asset('images/icons/progress_completed.png') }}" /></td>
                    <td>Completed</td>
                    <td>{{ $branch->completed }}</td>
                </tr>
            </table>


        </div>
    </article>
</div>

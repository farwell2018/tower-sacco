<div class="course-card mt-3 groups-card">
    <article>
        <div class="thumbnail">

            @if ($group->hasImage('cover_image'))
                <img src="{{ $group->image('cover_image', 'default') }}" alt="{{ $group->title }}" />
            @else
                @if (!empty($group->cover_image))
                    <img src="{{ asset('Connect_Groups/' . $group->cover_image) }}" alt="{{ $group->title }}" />
                @else
                    <img src="{{ asset('resources/cover_image/default-thumbnail.png') }}"
                        alt="{{ $group->title }}" />
                @endif

            @endif
            <div class="overlay">

            </div>
        </div>

        <div class="course-card-content session-card-content group-card-content">

            <p class="title ">
                <a href="#" class=" text-purple"><b>{{ $group->title }}</b></a>
            </p>

            <?php
            $str = $group['description'];
            if (strlen($str) > 60) {
                $str = substr($str, 0, 90) . '...';
            }
            
            ?>
            {!! $str !!}

            <p class=" text-grey session-card-calendar">
                @if ($group->group_format == 1)
                    <span class="session-calendar"> <img
                            src="{{ asset('images/icons/private-group-community.svg') }}" />
                        {{ $group->format->title }} </span>
                @else
                    <span class="session-calendar"> <img
                            src="{{ asset('images/icons/public-group-community.svg') }}" />
                        {{ $group->format->title }} </span>
                @endif

            </p>


        </div>


        <div class="card-footer ">
            <p class="readmore " style="padding-left:0">

                @if ($group->group_format == 1)

                    @if (empty(Auth::user()->getGroupStatus($group->id)))
                        <a href="{{ route('group.request', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_not_inline btn_join_group"> Join Group </a>
                    @elseif(Auth::user()->getGroupStatus($group->id) == 2)
                        <a href="#" class="btn btn-overall btn_join_requested btn_not_inline"> Requested</a>
                    @elseif(Auth::user()->getGroupStatus($group->id) == 3)
                        <a href="{{ route('group.activate', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_inline_block btn_save"> Accept Invite </a>

                        <a href="{{ route('group.decline', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_inline_block btn_cancel"> Decline </a>
                    @elseif(Auth::user()->getGroupStatus($group->id) == 1)
                        <a href="{{ route('group.details', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_not_inline btn_save"> Joined </a>
                    @endif
                @elseif($group->group_format == 2)
                    @if (empty(Auth::user()->getGroupStatus($group->id)))
                        <a href="{{ route('group.request', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_not_inline btn_join_group"> Join Group </a>
                    @elseif(Auth::user()->getGroupStatus($group->id) == 3)
                        <a href="{{ route('group.activate', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_inline_block btn_save"> Accept Invite </a>

                        <a href="{{ route('group.decline', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_inline_block btn_cancel"> Decline </a>
                    @elseif(Auth::user()->getGroupStatus($group->id) == 1)
                        <a href="{{ route('group.details', ['id' => $group->id, 'key' => \Request::segment(1)]) }}"
                            class="btn btn-overall btn_not_inline btn_save"> Joined </a>
                    @endif


                @endif

            </p>

        </div>

    </article>
</div>


<div class="course-card mt-3 sessions-card">
    <article>
      <div class="thumbnail">

        @if( $item->cop->hasImage('cover_image'))
        <img src="{{$item->cop->image("cover_image", "default")}}" alt="{{$item->cop->title}}" />
        @else 
          @if(!empty($item->cop->cover_image))
            <img src="{{asset('Groups/'.$item->cop->cover_image)}}" alt="{{$item->cop->title}}" />
             @else 
             <img src="{{asset('resources/cover_image/default-thumbnail.png')}}" alt="{{$item->cop->title}}" />
             @endif
       
        @endif
          <div class="overlay">
            <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->cop->title}}">
              <i class="fas fa-share"></i>
            </div>
          </div>
      </div>
  
      <div class="course-card-content session-card-content group-card-content">
       
        <p class="title ">
          <a href="#" class=" text-purple"><b>{{ $item->cop->title}}</b></a></p>
        
          <?php
          $str = $item->cop['description'];
          if (strlen($str) > 60) {
            $str = substr($str, 0, 150) . '...';
          }
          
          ?>
        {!!$str!!}
        
        <p class=" text-grey session-card-calendar">
            @if($item->cop->group_format == 1)

            <span class="session-calendar"> <img src="{{asset('images/icons/private-group-community.svg')}}"/> {{$item->cop->format->title}} </span>
        @else 

        <span class="session-calendar"> <img src="{{asset('images/icons/public-group-community.svg')}}"/> {{$item->cop->format->title}} </span>
        @endif
            
        </p>
    
        <p class="readmore " style="padding-left:0">
           
            @if($item->cop->group_format == 1)
            
            @if(empty(Auth::user()->getGroupStatus($item->cop->id)))

            <a href="{{route('group.request',['id'=>$item->cop->id])}}" class="btn btn-overall btn_join_community btn_not_inline" > Join Community </a>

            @elseif(Auth::user()->getGroupStatus($item->cop->id) == 2)

            <a href="#" class="btn btn-overall btn_join_requested btn_not_inline"> Requested</a>

            @elseif(Auth::user()->getGroupStatus($item->cop->id) == 3)

            <a href="{{route('group.activate',['id'=>$item->cop->id])}}" class="btn btn-overall btn_join_community btn_inline_block"> Accept Invite </a>

            <a href="{{route('group.decline',['id'=>$item->cop->id])}}" class="btn btn-overall btn_register btn_inline_block"> Decline </a>

            @elseif(Auth::user()->getGroupStatus($item->cop->id) == 1)

            <a href="{{route('group.details',['id'=>$item->cop->id])}}" class="btn btn-overall btn_register btn_not_inline"> Joined </a>

           @endif

           @elseif($item->cop->group_format == 2) 

           @if(empty(Auth::user()->getGroupStatus($item->cop->id)))

            <a href="{{route('group.request',['id'=>$item->cop->id])}}" class="btn btn-overall btn_join_community btn_not_inline"> Join Community </a>

            @elseif(Auth::user()->getGroupStatus($item->cop->id) == 3)

            <a href="{{route('group.activate',['id'=>$item->cop->id])}}" class="btn btn-overall btn_join_community btn_inline_block"> Accept Invite </a>

            <a href="{{route('group.decline',['id'=>$item->cop->id])}}" class="btn btn-overall btn_register btn_inline_block"> Decline </a>

            @elseif(Auth::user()->getGroupStatus($item->cop->id) == 1)

            <a href="{{route('group.details',['id'=>$item->cop->id])}}" class="btn btn-overall btn_register btn_not_inline"> Joined </a>

           @endif


           @endif

        </p>
        
      </div>

    </article>
  </div>
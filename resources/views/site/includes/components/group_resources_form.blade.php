<form action="" method="POST" enctype="multipart/form-data" id="group-upload-form" class="resource-upload-form">

   
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="resource_category" type="hidden" name="resource_creator" value="{{Auth::user()->id}}" />
            <input id="group_id" type="hidden" name="group_id" value="{{$group->id}}" />


            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Resource Type</label>
                        <select name="resource_type_id" id="resource_type" class="form-control">
                            <option>Select Resource Type</option>
                            @foreach($resourceTypes as $type)
                             <option value="{{$type->id}}"> {{$type->title}}</option>
                            @endforeach
                          </select>
                    @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
            
            <div id="articles_additional" style="display:none">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="article_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description" type="text"  name="article_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


            </div>


            <div id="videos_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="video_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description2" type="text"  name="video_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Video &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: mp4</small></label>
                            <input type="file" name="video_file" class="form-control" id="file-upload">
                      
                        @error('video_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>



            <div id="audios_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="audio_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description3" type="text"  name="audio_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Audio &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: mp3</small></label>
                            <input type="file" name="audio_file" class="form-control" id="file-upload">
                      
                        @error('audio_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>

           

            <div id="publications_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="publication_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description4" type="text"  name="publication_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Upload Resource File &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: pdf | word</small> </label>
                            <input type="file" name="downloable_file" class="form-control" id="file-upload">
                      
                        @error('downloable_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

              
         

            </div>

            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-overall btn_upload" id="saveLevels" style="left: 38%;">Submit Resource</button>
               
              </div>
            </div>
          
        </form>
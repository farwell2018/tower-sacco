@if((new \Jenssegers\Agent\Agent())->isDesktop())

<section class="general-section">
    <div class="container-fluid">
        <div class="row alignment-class-sessions">

<div class="col-12">
    <ul class="nav nav-tabs resource-tabs" role="tablist" id="resource-nav-tabs">
        @foreach ($resourceType as $key => $item)
            @if($key  === $resourceType->keys()->last())
            <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} resource-tabs-spacing " data-id="{{ $item->id }}">
                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab" >{{ $item->title }}</a>
              </li>
            @elseif($key === $resourceType->keys()->first())
            <li role="presentation" class="active border-right resource-tabs-spacing" data-id="{{ $item->id }}">
                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab" >{{ $item->title }}</a>
              </li>

            @else
          <li role="presentation" class="border-right resource-tabs-spacing" data-id="{{ $item->id }}">
            <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab" >{{ $item->title }}</a>
          </li>
          @endif
        @endforeach
    </ul></div>
</div>


<div class="col-12">
<div class="row mt-4 alignment-class-sessions">
    @include('site.includes.components.resources.resources_filter',[ 'countries' => $countries,'resourceTheme' =>$resourceTheme])

</div>

</div>


 <div class="row alignment-class-sessions">
     <div class="col-12">
        <div class="tab-content">
         
            @foreach ($resourceType as $key => $item)
             <div role="tabpanel" class="tab-pane {{ $key === $resourceType->keys()->first() ? 'active' : '' }}" id="home{{ $item->id }}" class="active">
                
                @if($item->id == 6)
                 @if($post == 1)

                 @if(count($articles) > 0)
                 <div class="row mt-4">
 
                     <h3 class="total_ressource_title"> Total Resources: {{count($resources_all)}} </h3>
                     
                     <p class="all_ressource_title"><span class="all_resource_title" >Articles </span>
                       
                      @if(count($articles) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(1)">See All Articles > </a></span>
                       @endif
                     </p>
                     @foreach($articles as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.articles', ['item'=>$item])
                     </div>
                    
                     @endforeach
                
                 </div>
                 @endif
                 @if(count($videos) > 0)
                 <div class="row mt-5">
                    
                     <p class="all_ressource_title"><span class="all_resource_title" >Videos </span>
                         @if(count($videos) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home3" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(3)">See All Videos ></a></span>
                       @endif
 
                     </p>
                     @foreach($videos as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.videos', ['item'=>$item])
                     </div>
                    
                     @endforeach
                   
                 </div>
                 @endif
                 @if(count($audios) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title"><span class="all_resource_title" >Audios</span>
                      
                         @if(count($audios) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home2" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(2)">See All Audios > </a></span>
                       @endif
 
                     </p>
                     @foreach($audios as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.audio', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                  @endif
                  @if(count($webinars) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title"><span class="all_resource_title" >Webinars</span>
                     
                         @if(count($webinars) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home5" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(5)">See All Webinars > </a></span>
                       @endif
 
                     </p>
                     @foreach($webinars as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.webinar', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                   @endif
                   @if(count($publications) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title"><span class="all_resource_title" >Reports and Publications</span>
 
                         @if(count($publications) > 4)
                         <span style="float: right;margin-right: 3%;"><a href="#home4" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(4)">See All Reports and Publications ></a></span>
                          @endif
                     </p>
                     @foreach($publications as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.publication', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                 @endif

                @else

                @if(count($start_articles) > 0)
                <div class="row mt-4">

                    <h3 class="total_ressource_title"> Total Resources: {{count($resources_all)}} </h3>
                    
                    <p class="all_ressource_title"><span class="all_resource_title" >Articles </span>
                      
                     @if(count($all_articles) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab" class="see-full-list" onclick="changeTab(1)">See All Articles > </a></span>
                      @endif
                    </p>
                    @foreach($start_articles as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.articles', ['item'=>$item])
                    </div>
                   
                    @endforeach
                    <div class="divider div-transparent div-dot"></div>
                </div>
                @endif
                @if(count($start_videos) > 0)
                <div class="row mt-3">
                   
                    <p class="all_ressource_title"><span class="all_resource_title" >Videos</span> 
                        @if(count($all_videos) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home3" aria-controls="home" role="tab" data-toggle="tab"  class="see-full-list" onclick="changeTab(3)">See All Videos > </a></span>
                      @endif

                    </p>
                    @foreach($start_videos as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.videos', ['item'=>$item])
                    </div>
                   
                    @endforeach
                    <div class="divider div-transparent div-dot"></div>
                </div>
                @endif
                @if(count($start_audios) > 0)
                <div class="row mt-3">
                    <p class="all_ressource_title"><span class="all_resource_title" >Audios</span>
                     
                        @if(count($all_audios) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home2" aria-controls="home" role="tab" data-toggle="tab"  class="see-full-list" onclick="changeTab(2)">See All Audios > </a></span>
                      @endif

                    </p>
                    @foreach($start_audios as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.audio', ['item'=>$item])
                    </div>
                   
                    @endforeach
                    <div class="divider div-transparent div-dot"></div>
                </div>
                 @endif
                 @if(count($start_webinars) > 0)
                <div class="row mt-3">
                    <p class="all_ressource_title"> <span class="all_resource_title" >Webinars</span>
                    
                        @if(count($all_webinars) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home5" aria-controls="home" role="tab" data-toggle="tab"  class="see-full-list" onclick="changeTab(5)">See All Webinars > </a></span>
                      @endif

                    </p>
                    @foreach($start_webinars as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.webinar', ['item'=>$item])
                    </div>
                   
                    @endforeach
                    <div class="divider div-transparent div-dot"></div>
                </div>
                  @endif
                  @if(count($start_publications) > 0)
                <div class="row mt-3">
                    <p class="all_ressource_title"> <span class="all_resource_title" >Reports and Publications </span>

                        @if(count($all_publications) > 4)
                        <span style="float: right;margin-right: 3%;"><a href="#home4" aria-controls="home" role="tab" data-toggle="tab"  class="see-full-list" onclick="changeTab(4)">See All Reports and Publications ></a></span>
                         @endif
                    </p>
                    @foreach($start_publications as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.publication', ['item'=>$item])
                    </div>
                   
                    @endforeach
                   
                </div>
                @endif
                @endif
               @endif

               @if($item->id == 1)
                <div class="row">
                    @foreach($articles as $article)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.articles', ['item'=>$article])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 2)
                <div class="row">
                    @foreach($audios as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.audio', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 3)
                <div class="row">
                    @foreach($videos as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.videos', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 4)
                <div class="row">
                    @foreach($publications as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.publication', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif
               
               @if($item->id == 5)
                <div class="row">
                    @foreach($webinars as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.webinar', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif
                 </div>
            @endforeach
           </div>
         </div>

     </div>

 </div>
 <div class="clearfix">
    <br /> <br />
</div>
<div class="container-fluid">
    <div class="col-12">
        <div class="row justify-content-center">
         <h1 class="text-center">Share Resources</h1>
         <h4 class="text-center">Share tools or resources that you think will be useful to other participants</h4>
           
         <button type="button" class="btn btn-overall btn_upload mt-3 mb-3" id="uploadResource" onclick="showUpload()" style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8"> <i class="fa fa-upload" aria-hidden="true"></i> Upload Resources</button>
        </div>
        <div class="row justify-content-center">
         <form action="{{route('resource.participants')}}" method="POST" enctype="multipart/form-data" id="file-upload-form" style="display:none" class="resource-upload-form">

        
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="resource_category" type="hidden" name="resource_category" value="{{Auth::user()->role}}" />

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Resource Theme  </label>
                        <select name="resource_theme_id" id="theme" class="form-control " >
                            <option>Select Resource Theme</option>
                            @foreach($resourceTheme as $theme)
                            <option value="{{$theme->id}}"> {{$theme->title}}</option>
                           @endforeach
                          </select>
                    @error('theme')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label>Resource Country</label>
                        <select name="resource_country" id="country" class="form-control ">
                            <option>Select Resource Country</option>
                            @foreach($countries as $key => $country)
                             <option value="{{$key}}"> {{$country}}</option>
                            @endforeach
                          </select>
                    @error('country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Resource Type</label>
                        <select name="resource_type_id" id="resource_type" class="form-control">
                            <option>Select Resource Type</option>
                            @foreach($resourceForm as $type)
                             <option value="{{$type->id}}"> {{$type->title}}</option>
                            @endforeach
                          </select>
                    @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
            
            <div id="articles_additional" style="display:none">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="article_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description" type="text"  name="article_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Article Cover Image &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: png | jpg | jpeg</small></label>
                            <input type="file" name="resource_image" class="form-control" id="file-upload">
                      
                        @error('fileUpload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>


            <div id="videos_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="video_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description2" type="text"  name="video_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Video &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: mp4</small></label>
                            <input type="file" name="video_file" class="form-control" id="file-upload">
                      
                        @error('video_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>



            <div id="audios_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="audio_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description3" type="text"  name="audio_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Audio &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: mp3</small></label>
                            <input type="file" name="audio_file" class="form-control" id="file-upload">
                      
                        @error('audio_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>

           

            <div id="publications_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="publication_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description4" type="text"  name="publication_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Upload Resource File &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: pdf | word</small> </label>
                            <input type="file" name="downloable_file" class="form-control" id="file-upload">
                      
                        @error('downloable_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Publication Cover Image  &nbsp;&nbsp;&nbsp;<small style="font-size:11px">supports: png | jpg |jpeg</small></label>
                            <input type="file" name="publications_resource_image" class="form-control" id="file-upload">
                      
                        @error('resource_image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

         

            </div>




{{-- 
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Title </label>
                    <input id="title" type="text" class="form-control" name="title" autocomplete="off" value="{{ old('title') }}" autofocus>
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
        
        
            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Description </label>
                        <textarea id="description" type="text"  name="description" class="md-textarea form-control" rows="5"></textarea>
                  
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
              
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                    <div class="file-background">
                        <div class="preview-zone hidden">
                            <div class="box box-solid">
                              <div class="box-header with-border">
                                <div><b>Preview</b></div>
                                <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-overall btn_take_course remove-preview">
                                     Remove upload
                                  </button>
                                </div>
                              </div>
                              <div class="box-body"></div>
                            </div>
                          </div>
                  <div class="dropzone-wrapper">
    
                    <label class="dropzone-desc" for="file-upload">
                        <h3> Upload your files </h3>
                         Supports: PDF and Word files
                        <br /><br />Drag and drop files here
                        <br />or
                        <br /><br /><span id="file-upload-btn" class="button">Browse files</span>
                        <br /><br />Maximum file size: 8Mb
                    </label>
                   
                    <input type="file" name="fileUpload" class="dropzone" id="file-upload">
                  </div>
                </div>
    
                <span class="invalid-feedback" role="alert" id="image_error" style="display:none">
                    
                </span>
           
              </div>
            </div> --}}
           
            
      
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-overall btn_upload" id="saveLevels" style="left: 45%;">Submit Resource</button>
              </div>
            </div>
          
        </form>
        </div>
    </div>
</div>
</section>

@else 

<section class="general-section">
    

<div class="col-12">
    <ul class="nav nav-tabs resource-tabs" role="tablist">
        @foreach ($resourceType as $key => $item)
            @if($key  === $resourceType->keys()->last())
            <li role="presentation" class="{{ $item->id == 1 ? 'active' : '' }} resource-tabs-spacing ">
                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->title }}</a>
              </li>
            @elseif($key === $resourceType->keys()->first())
            <li role="presentation" class="active border-right resource-tabs-spacing">
                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->title }}</a>
              </li>

            @else
          <li role="presentation" class="border-right resource-tabs-spacing">
            <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $item->title }}</a>
          </li>
          @endif
        @endforeach
    </ul></div>
</div>


<div class="col-12">
<div class="row mt-4 alignment-class-sessions">
    @include('site.includes.components.resources.resources_filter',[ 'countries' => $countries,'resourceTheme' =>$resourceTheme])

</div>

</div>


 <div class="row alignment-class-sessions">
     <div class="col-12">
        <div class="tab-content">
         
            @foreach ($resourceType as $key => $item)
             <div role="tabpanel" class="tab-pane {{ $key === $resourceType->keys()->first() ? 'active' : '' }}" id="home{{ $item->id }}" class="active">
                
                @if($item->id == 6)
                 @if($post == 1)

                   {{$post}}
                 @if(count($articles) > 0)
                 <div class="row mt-4">
 
                     <h3 class="total_ressource_title"> Total Resources: {{count($resources_all)}} </h3>
                     
                     <p class="all_ressource_title">Articles 
                       
                      @if(count($articles) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">See All Articles </a></span>
                       @endif
                     </p>
                     @foreach($articles as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.articles', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 
                 </div>
                 @endif
                 @if(count($videos) > 0)
                 <div class="row mt-5">
                    
                     <p class="all_ressource_title">Videos 
                         @if(count($videos) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home3" aria-controls="home" role="tab" data-toggle="tab">See All Videos</a></span>
                       @endif
 
                     </p>
                     @foreach($videos as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.videos', ['item'=>$item])
                     </div>
                    
                     @endforeach
                   
                 </div>
                 @endif
                 @if(count($audios) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title">Audios
                      
                         @if(count($audios) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home2" aria-controls="home" role="tab" data-toggle="tab">See All Audios</a></span>
                       @endif
 
                     </p>
                     @foreach($audios as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.audio', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                  @endif
                  @if(count($webinars) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title">Webinars
                     
                         @if(count($webinars) > 4)
                      <span style="float: right;margin-right: 3%;"><a href="#home5" aria-controls="home" role="tab" data-toggle="tab">See All Webinars </a></span>
                       @endif
 
                     </p>
                     @foreach($webinars as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.webinar', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                   @endif
                   @if(count($publications) > 0)
                 <div class="row mt-4">
                     <p class="all_ressource_title">Reports and Publications
 
                         @if(count($publications) > 4)
                         <span style="float: right;margin-right: 3%;"><a href="#home4" aria-controls="home" role="tab" data-toggle="tab">See All Reports and Publications</a></span>
                          @endif
                     </p>
                     @foreach($publications as $item)
                     <div class="col-md-3 col-12">
                         @include('site.includes.components.resources.publication', ['item'=>$item])
                     </div>
                    
                     @endforeach
                 </div>
                 @endif

                @else

                @if(count($start_articles) > 0)
                <div class="row mt-4">

                    <h3 class="total_ressource_title"> Total Resources: {{count($resources_all)}} </h3>
                    
                    <p class="all_ressource_title">Articles 
                      
                     @if(count($all_articles) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">See All Articles </a></span>
                      @endif
                    </p>
                    @foreach($start_articles as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.articles', ['item'=>$item])
                    </div>
                   
                    @endforeach
                
                </div>
                @endif
                @if(count($start_videos) > 0)
                <div class="row mt-5">
                   
                    <p class="all_ressource_title">Videos 
                        @if(count($all_videos) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home3" aria-controls="home" role="tab" data-toggle="tab">See All Videos</a></span>
                      @endif

                    </p>
                    @foreach($start_videos as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.videos', ['item'=>$item])
                    </div>
                   
                    @endforeach
                  
                </div>
                @endif
                @if(count($start_audios) > 0)
                <div class="row mt-4">
                    <p class="all_ressource_title">Audios
                     
                        @if(count($all_audios) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home2" aria-controls="home" role="tab" data-toggle="tab">See All Audios</a></span>
                      @endif

                    </p>
                    @foreach($start_audios as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.audio', ['item'=>$item])
                    </div>
                   
                    @endforeach
                </div>
                 @endif
                 @if(count($start_webinars) > 0)
                <div class="row mt-4">
                    <p class="all_ressource_title">Webinars
                    
                        @if(count($all_webinars) > 4)
                     <span style="float: right;margin-right: 3%;"><a href="#home5" aria-controls="home" role="tab" data-toggle="tab">See All Webinars </a></span>
                      @endif

                    </p>
                    @foreach($start_webinars as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.webinar', ['item'=>$item])
                    </div>
                   
                    @endforeach
                </div>
                  @endif
                  @if(count($start_publications) > 0)
                <div class="row mt-4">
                    <p class="all_ressource_title">Reports and Publications

                        @if(count($all_publications) > 4)
                        <span style="float: right;margin-right: 3%;"><a href="#home4" aria-controls="home" role="tab" data-toggle="tab">See All Reports and Publications</a></span>
                         @endif
                    </p>
                    @foreach($start_publications as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.publication', ['item'=>$item])
                    </div>
                   
                    @endforeach
                </div>
                @endif
                @endif
               @endif

               @if($item->id == 1)
                <div class="row">
                    @foreach($articles as $article)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.articles', ['item'=>$article])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 2)
                <div class="row">
                    @foreach($audios as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.audio', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 3)
                <div class="row">
                    @foreach($videos as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.videos', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif

               
               @if($item->id == 4)
                <div class="row">
                    @foreach($publications as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.publication', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif
               
               @if($item->id == 5)
                <div class="row">
                    @foreach($webinars as $item)
                    <div class="col-md-3 col-12">
                        @include('site.includes.components.resources.webinar', ['item'=>$item])
                    </div>
                   
                    @endforeach
               </div>
               @endif
                 </div>
            @endforeach
           </div>
         </div>

     </div>

 </div>
 <div class="clearfix">
    <br /> <br />
</div>
<div class="container-fluid">
    <div class="col-12">
        <div class="row justify-content-center">
         <h1 class="text-center">Share Resources</h1>
         <h4 class="text-center">Share tools or resources that you think will be useful to other participants</h4>
           
         <button type="button" class="btn btn-overall btn_upload" id="uploadResource" onclick="showUpload()" style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8"> <i class="fa fa-upload" aria-hidden="true"></i> Upload Resources</button>
        </div>
        <div class="row justify-content-center">
         <form action="{{route('resource.participants')}}" method="POST" enctype="multipart/form-data" id="file-upload-form" style="display:none" class="resource-upload-form">

        
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="resource_category" type="hidden" name="resource_category" value="{{Auth::user()->role}}" />

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Resource Theme  </label>
                        <select name="resource_theme_id" id="theme" class="form-control ">
                            <option>Select Resource Theme</option>
                            @foreach($resourceTheme as $theme)
                            <option value="{{$theme->id}}"> {{$theme->title}}</option>
                           @endforeach
                          </select>
                    @error('theme')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label>Resource Country</label>
                        <select name="resource_country" id="country" class="form-control ">
                            <option>Select Resource Country</option>
                            @foreach($countries as $key => $country)
                             <option value="{{$key}}"> {{$country}}</option>
                            @endforeach
                          </select>
                    @error('country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-12">
                    <div class="form-check">
                        <label> Resource Type</label>
                        <select name="resource_type_id" id="resource_type" class="form-control">
                            <option>Select Resource Type</option>
                            @foreach($resourceForm as $type)
                             <option value="{{$type->id}}"> {{$type->title}}</option>
                            @endforeach
                          </select>
                    @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
            
            <div id="articles_additional" style="display:none">
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="article_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description" type="text"  name="article_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Article Cover Image </label>
                            <input type="file" name="resource_image" class="form-control" id="file-upload">
                      
                        @error('fileUpload')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>


            <div id="videos_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="video_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description2" type="text"  name="video_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Video </label>
                            <input type="file" name="video_file" class="form-control" id="file-upload">
                      
                        @error('video_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>



            <div id="audios_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="audio_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description3" type="text"  name="audio_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Audio </label>
                            <input type="file" name="audio_file" class="form-control" id="file-upload">
                      
                        @error('audio_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>

           

            <div id="publications_additional" style="display:none">

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Title </label>
                        <input id="title" type="text" class="form-control" name="publication_title" autocomplete="off" value="{{ old('title') }}" autofocus>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Description </label>
                            <textarea id="description4" type="text"  name="publication_description" class="md-textarea form-control" rows="5"></textarea>
                      
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>


                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Publication Cover Image </label>
                            <input type="file" name="publications_resource_image" class="form-control" id="file-upload">
                      
                        @error('resource_image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-check">
                            <label> Downloadable Resource </label>
                            <input type="file" name="downloable_file" class="form-control" id="file-upload">
                      
                        @error('downloable_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>

            </div>





      
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-overall btn_upload" id="saveLevels" style="left: 45%;">Submit Resource</button>
              </div>
            </div>
          
        </form>
        </div>
  
</section>


@endif


@php
use Carbon\Carbon;
    $month = [];
    for ($m = 1; $m <= 12; $m++) {
        $month[] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
    }
    
    $years = range(now()->year, now()->year + 1);
@endphp

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid ">
        <div class="row justify-content-center">
            <div class="card resource-filter mb-3 mt-4">
                <div class="col-12">
                    <div class="row justify-content-center">

                        <div class="col-12">
                            <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-1  mt-4 mb-4 col-spacing">
                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                            style="float: right;" />
                                    </div>

                                    <div class="col-7 no-padding-right ">
                                        <div class="row">

                                            <div class="col-4 mt-4 mb-4 ">
                                                <label class="filter_by"> Find policies by</label>
                                            </div>
                                            <div class="col-3">
                                                <select name="year" id="" class="form-control mt-4 mb-4 ">
                                                    <option value=""> Year</option>
                                                    @foreach ($years as $key=>$theme)
                                                        <option value="{{ $theme }}"> {{ $theme }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                              
                                              


                                            </div>
                                            
                                            <div class="col-4">
                                                <input type="text" name="search" class="form-control mt-4 mb-4" placeholder=" Search resource">

                                            </div>
                                            
                                            

                                        </div>


                                    </div>

                                    <div class="col-4 ">
                                        <button type="submit"
                                            class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                        <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                            onClick="window.location.href=window.location.href">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@else
    <div class="container-fluid">

        <div class="card resource-filter mb-3 mt-3">
            <div class="col-12">
                <div class="row">

                    <div class="col-12">
                        <form action="{{ route('pages', ['key' => $key]) }}" method="POST">
                            @csrf
                            <div class="row">

                                <div class="col-12">
                                    <select name="category" id="" class="form-control mt-4 mb-2 minimal" required>
                                        <option value="">Filter by Resource Category</option>
                                        @foreach ($jobRoles as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                    <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>


@endif

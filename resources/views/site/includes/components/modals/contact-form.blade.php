<div class="modal fade bd-example-modal-lg" id="ModalCoporateForm" role="dialog" aria-labelledby="ModalCoporateForm"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card form-modal">
                    <div class="header-section">
                        <h1 class="text-white">Contact Us</h1><br />
                        <p class="text-white">Contact us on how we can partner with you in bringing your training to
                            life.</p>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('general_messages.store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <input id="name" type="text" placeholder="Your Name (required)"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            required>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <input id="email" type="email" placeholder="Your Email (required)"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            required>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <input id="phone" type="tel" placeholder="Your Phone (required)"
                                            class="form-control @error('phone') is-invalid @enderror" name="phone"
                                            required>

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <input id="subject" type="text" placeholder="Subject (required)"
                                            class="form-control @error('subject') is-invalid @enderror" name="subject"
                                            required>

                                        @error('subject')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <textarea class="form-control" placeholder="What are your training needs" rows="5" id="comment"
                                            name="comment"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check">
                                        <button type="submit" class="btn btn-overall white">
                                            {{ __('Send') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

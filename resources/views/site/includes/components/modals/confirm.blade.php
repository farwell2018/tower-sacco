
@php
    $flashMessage = session()->get('confirm_success');
@endphp

<div class="modal fade" id="ModalConfirm" tabindex="-1" role="dialog" aria-labelledby="ModalConfirm" aria-hidden="true" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog notification-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">

          <div class="row">
            <div class="col-8">
              <div class="noti-header"><h4 class="text-center  noti-success-header" > {{ $flashMessage['title'] }} </h4></div>

              <div class="col light">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-text noti-success" >
                     {!! $flashMessage['content'] !!}
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-sbutt" >
                  @if(isset($flashMessage['action']))
                  <form action="{{route('ipay.status')}}" method="post" id="transaction">
                    @csrf
                  <input type="hidden" name="order" value="{{$flashMessage['order']}}" id="confirmID">
                  <button type="button" class="btn btn-overall purple"  onclick="confirmTransaction()" >{{ __('Confirm') }}</button>
                  <img src="{{asset('images/icons/giphy.gif')}}" id="img" style="display:none; width:30px"/ >
                  <form>
                  @else
                  <button type="button" class="btn dismiss-modal modal-accept noti-sbutton"  data-dismiss="modal"  onclick="document.getElementById('ModalMessage').style.display='none'" >Dismiss</button>
                  @endif
                </div>
              </div>
            </div>

            <div class="col-4 bg-teal-error dark">
            <div class="col ">

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>



@section('js')

@endsection




<!-- Modal -->
<div class="modal fade notification" id="courseUpcoming" tabindex="-1" role="dialog" aria-labelledby="courseUpcomingLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content error-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationModalLabel">Course Enrollment Issue</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">{{$poptxt}}
            </div>
            <div class="modal-footer">
          @guest
                <a href="{{route('user.login')}}" class="btn btn-confirm dismiss-modal modal-accept">Got it</a>
                @else
                <button type="button" class="btn btn-confirm dismiss-modal modal-accept" id="closeBtn" data-dismiss="modal">OK</button>
          @endguest      
            </div>
        </div>
    </div>
</div>
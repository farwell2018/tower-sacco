@extends('layouts.app')
@section('css')
    <style>
        @media (max-width: 480px) {
            .jarallax-img {
                width: 100%;
                height: 283px;
                object-fit: cover;
            }

            .inline-video-jarallax .content,
            .jarallax .content {
                margin-top: -43%;
                margin-left: 12px;
            }
        }

    </style>
@endsection
@section('title', 'Courses')
@php

 $configLms = config()->get("settings.lms.live");   
 
$details = [['id' => '1', 'name' => 'Description'], ['id' => '2', 'name' => 'Course Outline']];
@endphp
@section('content')
    <div class="courses-page">

        @if (Session::has('course_success'))
        <script>
            jQuery(document).ready(function($) {
                console.log("it has");
                $("#CourseSuccess").addClass('show');
            });
        </script>
    @elseif(Session::has('course_errors'))
        <script>
            jQuery(document).ready(function($) {
                $("#CourseErrors").addClass('show');
            });
        </script>
        @else
        <script>
   window.addEventListener('load', function() {
        if(!window.location.hash) {
            window.location = window.location + '#/';
            window.location.reload();
        }
    });
  </script>
    @endif
        @if ($pageItem->hasImage('hero_image'))
            @php
                $image = $pageItem->image('hero_image', 'default');
                $text = $course->name;
            @endphp
            @include('site.includes.components.parallax', [
                'image' => $image,
                'text' => $text,
            ])
        @endif
        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('pages', ['key' => 'courses']) }}"> Courses</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{!! $course->name !!}</a>
                </li>
            </ol>

            <div class="d-block d-sm-none justify-content-center" style="margin-left:15px">
                @guest
                    <p>
                        <a href="{{ route('user.login') }}" class="btn btn-overall purple" style="font-size: 14px;">Enrol for this
                            course <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        </a>
                    </p>
                @else
                    @if (!$licensed)
                        <p><a class="btn btn-overall purple" href="{{ route('course.enroll', $course->course_id) }}/"
                                style="font-size: 14px;">Enroll into
                                Course</a></p>
                    @else
                        @if ($enrolled)
                            <p><a class="btn btn-overall purple"
                                    href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                    target="_blank" style="font-size: 14px;">Take Course</a></p>
                        @else
                            <p><a class="btn btn-overall purple" href="{{ route('course.enroll', $course->course_id) }}/"
                                    style="font-size: 14px;">Enroll
                                    into
                                    Course</a></p>
                        @endif
                    @endif
                @endguest


            </div>
        @endcomponent


     
        <div class="row courses-detail-row">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                <ul class="nav nav-pills course-tabs" role="tablist">
                    @foreach ($details as $key => $item)
                        @if ($key === array_key_first($details))
                            <li class="nav-item active">
                                <a class="nav-link first-tab" data-toggle="pill" href="#home">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#menu1">Course Outline</a>
                            </li>
                        @endif
                    @endforeach
                </ul>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row column-reverse">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                    @include('site.includes.components.course-actions')


                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="course-card detail-card">
                                        <article>
                                            <div class="course-card-content">
                                                <div class="row-description">
                                                    {!! $course->overview !!}
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="menu1" class="container-fluid tab-pane fade">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="course-card detail-card">
                                        <article>
                                            <div class="course-card-content">
                                                <div class="row-description">
                                                    {!! $course->more_info !!}
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
   

                    </div>

                    <div class="row general-section no-margin-top ml-4">
                        
                            <!-- @if ($course->course_video)
                                <div class="video-wrapper">
                                    <video controls playsinline muted id="bgvid">
                                        <source src="{{ asset('course_images/' . $course->course_video) }}"
                                            type="video/mp4">
                                    </video>
                                </div>
                            @endif -->
                        
                        <div>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('.show-table').removeClass('hidden-xs');

        });


        $(document).ready(function() {

            // Get current page URL
            var url = window.location.href;



            // remove # from URL
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

            // remove parameters from URL
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

            // select file name
            url = url.split('/')[4];




            // Loop all menu items
            $('.navbar-nav .nav-item').each(function() {

                // select href
                var href = $(this).find('a').attr('href');

                link = href.split('/')[3];


                // Check filename
                if (link === 'courses') {

                    // Add active class
                    $(this).addClass('active');
                }
            });
        });
    </script>



@endsection

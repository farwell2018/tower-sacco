@extends('layouts.app_no_js')

@section('title', 'Groups')

@section('content')

    @php
    $details_panels = [['id' => '1', 'name' => 'All', 'level' => '0'], ['id' => '2', 'name' => 'AAC CoPs', 'level' => '0'], ['id' => '3', 'name' => 'Participants CoPs', 'level' => '0']];
    @endphp
    <div class="background-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/group_header.png'),
            'text' => 'Create Group',
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('pages', ['key' => 'groups']) }}"> Groups</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">Create Group</a>
                </li>
            </ol>
        @endcomponent

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
        @endif
        <div class="clearfix">
            <br />
        </div>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-12">

                <div class="row alignment-class-connect profile-top">

                    <div class="col-md-9 offset-md-2 col-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 d-none d-sm-block pr-3 mb-4 mt-2">
                                    <div class="card sidebar-card">
                                        <article>
                                            <form class="kt-form" method="POST" action="{{ route('group.store') }}"
                                                enctype="multipart/form-data" style="padding: 2rem;">
                                                <div class="kt-portlet__body">

                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="link" value="groups">

                                                    <div class="form-group">
                                                        <label for="name">{{ __('Title') }}</label>
                                                        <input id="title" type="title"
                                                            class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                            name="title" value="{{ old('title') }}" required autofocus>

                                                        @if ($errors->has('title'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('title') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label
                                                            for="email">{{ __('Confirm if Public or Private group') }}</label>
                                                        <select name="format" id="" class="form-control" required>
                                                            <option value="">Select Format</option>
                                                            @foreach ($formats as $format)
                                                                <option value="{{ $format['id'] }}">{{ $format['title'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('format'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('format') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="email">{{ __('Description') }}</label>
                                                        <textarea id="description" class="form-control" name="description"
                                                            required rows="10"></textarea>

                                                        @if ($errors->has('description'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('description') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>



                                                    <div class="form-group">
                                                        <label
                                                            for="Interview_referee_1">{{ __('Group Cover Image') }}</label>
                                                        <input type='file' class="form-control" name="image" required />
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="email">{{ __('Users to invite') }}</label>
                                                        <select name="users[]" id="" class="form-control" multiple>
                                                            @foreach ($users as $user)
                                                                <option value="{{ $user->id }}">{{ $user->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('users'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('users') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>


                                                </div>

                                                <div class="kt-portlet__foot">
                                                    <div class="kt-form__actions">
                                                        <button type="submit"
                                                            class="btn btn-overall btn_save">Submit</button>
                                                        <a href="{{ url()->previous() }}"
                                                            class="btn btn-overall btn_cancel "> Cancel </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        @else
            <div class="col-md-12">
                <div class="row ">

                    <div class="col-md-12 col-12">
                        <div class="card sidebar-card">
                            <article>
                                <form class="kt-form" method="POST" action="{{ route('group.store') }}"
                                    enctype="multipart/form-data" style="padding: 2rem;">
                                    <div class="kt-portlet__body">

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="link" value="groups">

                                        <div class="form-group">
                                            <label for="name">{{ __('Title') }}</label>
                                            <input id="title" type="title"
                                                class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                name="title" value="{{ old('title') }}" required autofocus>

                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="email">{{ __('Confirm if Public or Private group') }}</label>
                                            <select name="format" id="" class="form-control" required>
                                                <option value="">Select Format</option>
                                                @foreach ($formats as $format)
                                                    <option value="{{ $format['id'] }}">{{ $format['title'] }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('format'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('format') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="form-group">
                                            <label for="email">{{ __('Description') }}</label>
                                            <textarea id="description" class="form-control" name="description" required
                                                rows="10"></textarea>

                                            @if ($errors->has('description'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>



                                        <div class="form-group">
                                            <label for="Interview_referee_1">{{ __('Group Cover Image') }}</label>
                                            <input type='file' class="form-control" name="image" required />
                                        </div>


                                        <div class="form-group">
                                            <label for="email">{{ __('Users to invite') }}</label>
                                            <select name="users[]" id="" class="form-control" multiple>
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('users'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('users') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                    </div>

                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-overall btn_save">Submit</button>
                                            <a href="{{ url()->previous() }}" class="btn btn-overall btn_cancel "> Cancel
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </article>
                        </div>

                    </div>


                </div>

            </div>
        @endif
    @endsection

    @section('js')
        <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

        <script>
            CKEDITOR.replace('description');
        </script>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <script>
                $(document).ready(function() {

                    // Get current page URL
                    var url = window.location.href;

                    // remove # from URL
                    url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

                    // remove parameters from URL
                    url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

                    // select file name
                    url = url.split('/')[5];


                    console.log(url);

                    // Loop all menu items
                    $('.list-group .list-nav').each(function() {

                        // select href
                        var href = $(this).find('a').attr('href');



                        link = href.split('/')[4];


                        console.log(link);

                        // Check filename
                        if (link === url) {
                            console.log($(this))
                            // Add active class
                            $(this).addClass('active');
                            $(this).find('a').addClass('active')

                        }
                    });
                });
            </script>
        @else
            <script>
                $(document).ready(function() {

                    // Get current page URL
                    var url = window.location.href;

                    // remove # from URL
                    url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

                    // remove parameters from URL
                    url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

                    // select file name
                    url = url.split('/')[5];


                    console.log(url);

                    // Loop all menu items
                    $('.connect_mobile_menu .list-nav').each(function() {

                        // select href
                        var href = $(this).find('a').attr('href');



                        link = href.split('/')[4];


                        console.log(link);

                        // Check filename
                        if (link === url) {
                            console.log($(this))
                            // Add active class
                            $(this).addClass('active');
                            $(this).find('a').addClass('active')

                        }
                    });
                });
            </script>
        @endif
    @endsection

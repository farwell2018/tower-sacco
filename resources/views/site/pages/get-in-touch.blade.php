
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app_no_js')

@section('title','Session')



@section('content')
<div class="background-page"> 
    @php 
    $text = 'Contact Us';
    @endphp
    
    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/contact_us.png"),
    'text'=> $text
    ])
    {{-- @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active"></a>
        </li>
    </ol>
    @endcomponent --}}
    @if(Session::has('course_success'))
    <script>
    jQuery(document).ready(function($){
      console.log("it has");
     $("#CourseSuccess").addClass('show');
  });
  </script>
  @elseif(Session::has('course_errors'))
  <script>
  jQuery(document).ready(function($){
   $("#CourseErrors").addClass('show');
  });
  </script>
  @else
      {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
  @endif
    <div class="clearfix">
        <br /> <br />
    </div>
    <div class="col-md-12">
        <div class="row alignment-class-sessions">

            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            
            <div class="col-3"></div>

             <div class="col-6">
               
                <div class="course-card sessions-card contact-card">
                  
                 <article>

                <h2 class="text-center"> Get in Touch</h2>
            <form method="POST" action="{{ route('general_messages.store') }}" class="grey">
                @csrf
              
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label for="name">{{ __('Name') }}<span class="asterisk">*</span></label>
                      <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
              
                      @if ($errors->has('name'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group ">
                      <label for="contact_phone">{{ __('Phone Number') }}<span class="asterisk">*</span></label>
                      <input id="contact_phone" type="text" class="form-control{{ $errors->has('contact_phone') ? ' is-invalid' : '' }}" name="contact_phone" value="{{ old('contact_phone') }}" required>
              
                      @if ($errors->has('contact_phone'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('contact_phone') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
              
                <div class="form-group ">
                  <label for="subject">{{ __('Subject') }}<span class="asterisk">*</span></label>
                  <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject') }}" required>
              
                  @if ($errors->has('subject'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('subject') }}</strong>
                  </span>
                  @endif
                </div>
              
                <div class="form-group ">
                  <label for="contact_message">{{ __('Message') }}<span class="asterisk">*</span></label>
                  <textarea id="contact_message" class="form-control" name="contact_message" rows="7" required>{{ old('contact_message') }}</textarea>
              
                  @if ($errors->has('contact_message'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('contact_message') }}</strong>
                  </span>
                  @endif
                </div>
              
                <br/>
                <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="form-group flex-group">
                      <button type="submit" class="btn btn-overall purple">
                        {{ __('Submit') }}
                      </button>
                    </div>
                  </div>
                </div>
              </form>

            </article>
                </div>
 

             </div>

             <div class="col-3"></div>
 

            @else 
            

            <div class="course-card sessions-card contact-card">
                  
                <article>
                    <h2 class="text-center"> Get in Touch</h2>
                    <form method="POST" action="{{ route('general_messages.store') }}" class="grey">
                        @csrf
                      
                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="name">{{ __('Name') }}<span class="asterisk">*</span></label>
                              <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                      
                              @if ($errors->has('name'))
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group ">
                              <label for="contact_phone">{{ __('Phone Number') }}<span class="asterisk">*</span></label>
                              <input id="contact_phone" type="text" class="form-control{{ $errors->has('contact_phone') ? ' is-invalid' : '' }}" name="contact_phone" value="{{ old('contact_phone') }}" required>
                      
                              @if ($errors->has('contact_phone'))
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('contact_phone') }}</strong>
                              </span>
                              @endif
                            </div>
                          </div>
                        </div>
                      
                        <div class="form-group ">
                          <label for="subject">{{ __('Subject') }}<span class="asterisk">*</span></label>
                          <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject') }}" required>
                      
                          @if ($errors->has('subject'))
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('subject') }}</strong>
                          </span>
                          @endif
                        </div>
                      
                        <div class="form-group ">
                          <label for="contact_message">{{ __('Message') }}<span class="asterisk">*</span></label>
                          <textarea id="contact_message" class="form-control" name="contact_message" rows="7" required>{{ old('contact_message') }}</textarea>
                      
                          @if ($errors->has('contact_message'))
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_message') }}</strong>
                          </span>
                          @endif
                        </div>
                      
                        <br/>
                        <div class="row">
                         
                          <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group flex-group">
                              <button type="submit" class="btn btn-overall purple">
                                {{ __('Submit') }}
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>

           </article>
               </div>


            @endif






              
        </div>
          
    </div>
@endsection


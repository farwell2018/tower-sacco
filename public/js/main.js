(function($) {
    $(window).load(function() {

        'use strict';
        try {
          

            $('#sponsorsCarousel').owlCarousel({
                stagePadding: 100,
                loop: true,
                margin: 10,
                items: 1,
                lazyLoad: true,
                nav: true,
                navText: ["<div class='nav-button owl-prev'><img src='assets/img/left_long_arrow.png'/></div>", "<div class='nav-button owl-next'><img src='assets/img/right_long_arrow.svg'/></div>"],
                responsive: {
                    0: {
                        items: 1,
                        stagePadding: 60
                    },
                    600: {
                        items: 1,
                        stagePadding: 100
                    },
                    1000: {
                        items: 1,
                        stagePadding: 200
                    },
                    1200: {
                        items: 1,
                        stagePadding: 250
                    }
                    // 1400: {
                    //     items: 1,
                    //     stagePadding: 300
                    // }
                    // 1600: {
                    //     items: 1,
                    //     stagePadding: 350
                    // },
                    // 1800: {
                    //     items: 1,
                    //     stagePadding: 400
                    // }
                }
            });



        } catch (err) {}

        
    });


}(jQuery));

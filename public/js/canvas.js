function drawcanvas(ctx, owner, certcourse, dateissued, certid) {

      var alpha = ctx.globalAlpha;

      // layer1/Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(0.0, 792.0);
      ctx.lineTo(1122.1, 792.0);
      ctx.lineTo(1122.1, 0.0);
      ctx.lineTo(0.0, 0.0);
      ctx.lineTo(0.0, 792.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(245, 247, 248)";
      ctx.fill();

      // layer1/Group

      // layer1/Group/Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(919.3, 740.7);
      ctx.lineTo(872.4, 711.5);
      ctx.lineTo(919.3, 682.3);
      ctx.lineTo(919.3, 740.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(248, 237, 240)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 183.8);
      ctx.lineTo(1120.7, 213.5);
      ctx.lineTo(1068.6, 243.2);
      ctx.lineTo(1068.6, 183.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 243.2);
      ctx.lineTo(1120.7, 272.9);
      ctx.lineTo(1068.6, 302.6);
      ctx.lineTo(1068.6, 243.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 213.5);
      ctx.lineTo(1068.6, 243.2);
      ctx.lineTo(1120.7, 272.9);
      ctx.lineTo(1120.7, 213.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 302.6);
      ctx.lineTo(1120.7, 332.3);
      ctx.lineTo(1068.6, 362.1);
      ctx.lineTo(1068.6, 302.6);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 272.9);
      ctx.lineTo(1068.6, 302.6);
      ctx.lineTo(1120.7, 332.3);
      ctx.lineTo(1120.7, 272.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 362.1);
      ctx.lineTo(1120.7, 391.8);
      ctx.lineTo(1068.6, 421.5);
      ctx.lineTo(1068.6, 362.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1120.7, 332.3);
      ctx.lineTo(1068.6, 362.1);
      ctx.lineTo(1120.7, 391.8);
      ctx.lineTo(1120.7, 332.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1068.6, 421.5);
      ctx.lineTo(1120.7, 451.2);
      ctx.lineTo(1068.6, 480.9);
      ctx.lineTo(1068.6, 421.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 391.8);
      ctx.lineTo(1068.6, 421.5);
      ctx.lineTo(1120.7, 451.2);
      ctx.lineTo(1120.7, 391.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(1067.3, 5.5);
      ctx.lineTo(1119.3, 35.2);
      ctx.lineTo(1067.3, 64.9);
      ctx.lineTo(1067.3, 5.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1068.6, 64.9);
      ctx.lineTo(1120.7, 94.6);
      ctx.lineTo(1068.6, 124.3);
      ctx.lineTo(1068.6, 64.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1120.7, 35.2);
      ctx.lineTo(1068.6, 64.9);
      ctx.lineTo(1120.7, 94.6);
      ctx.lineTo(1120.7, 35.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1068.6, 124.3);
      ctx.lineTo(1120.7, 154.0);
      ctx.lineTo(1068.6, 183.8);
      ctx.lineTo(1068.6, 124.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1120.7, 94.6);
      ctx.lineTo(1068.6, 124.3);
      ctx.lineTo(1120.7, 154.0);
      ctx.lineTo(1120.7, 94.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 154.0);
      ctx.lineTo(1068.6, 183.8);
      ctx.lineTo(1120.7, 213.5);
      ctx.lineTo(1120.7, 154.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(1068.6, 480.9);
      ctx.lineTo(1120.7, 510.6);
      ctx.lineTo(1068.6, 540.4);
      ctx.lineTo(1068.6, 480.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1017.9, 35.4);
      ctx.lineTo(1069.9, 65.1);
      ctx.lineTo(1017.9, 94.8);
      ctx.lineTo(1017.9, 35.4);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1120.7, 451.2);
      ctx.lineTo(1068.6, 480.9);
      ctx.lineTo(1120.7, 510.6);
      ctx.lineTo(1120.7, 451.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 540.4);
      ctx.lineTo(1120.7, 570.1);
      ctx.lineTo(1068.6, 599.8);
      ctx.lineTo(1068.6, 540.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 510.6);
      ctx.lineTo(1068.6, 540.4);
      ctx.lineTo(1120.7, 570.1);
      ctx.lineTo(1120.7, 510.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1068.6, 599.8);
      ctx.lineTo(1120.7, 629.5);
      ctx.lineTo(1068.6, 659.2);
      ctx.lineTo(1068.6, 599.8);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1120.7, 570.1);
      ctx.lineTo(1068.6, 599.8);
      ctx.lineTo(1120.7, 629.5);
      ctx.lineTo(1120.7, 570.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(1068.6, 659.2);
      ctx.lineTo(1120.7, 688.9);
      ctx.lineTo(1068.6, 718.7);
      ctx.lineTo(1068.6, 659.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1120.7, 629.5);
      ctx.lineTo(1068.6, 659.2);
      ctx.lineTo(1120.7, 688.9);
      ctx.lineTo(1120.7, 629.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 718.7);
      ctx.lineTo(1120.7, 748.4);
      ctx.lineTo(1068.6, 778.1);
      ctx.lineTo(1068.6, 718.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1120.7, 688.9);
      ctx.lineTo(1068.6, 718.7);
      ctx.lineTo(1120.7, 748.4);
      ctx.lineTo(1120.7, 688.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 183.8);
      ctx.lineTo(1016.6, 213.5);
      ctx.lineTo(964.6, 243.2);
      ctx.lineTo(964.6, 183.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 243.2);
      ctx.lineTo(1016.6, 272.9);
      ctx.lineTo(964.6, 302.6);
      ctx.lineTo(964.6, 243.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 213.5);
      ctx.lineTo(964.6, 243.2);
      ctx.lineTo(1016.6, 272.9);
      ctx.lineTo(1016.6, 213.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 302.6);
      ctx.lineTo(1016.6, 332.3);
      ctx.lineTo(964.6, 362.1);
      ctx.lineTo(964.6, 302.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 272.9);
      ctx.lineTo(964.6, 302.6);
      ctx.lineTo(1016.6, 332.3);
      ctx.lineTo(1016.6, 272.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(964.6, 362.1);
      ctx.lineTo(1016.6, 391.8);
      ctx.lineTo(964.6, 421.5);
      ctx.lineTo(964.6, 362.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1016.6, 332.3);
      ctx.lineTo(964.6, 362.1);
      ctx.lineTo(1016.6, 391.8);
      ctx.lineTo(1016.6, 332.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 421.5);
      ctx.lineTo(1016.6, 451.2);
      ctx.lineTo(964.6, 480.9);
      ctx.lineTo(964.6, 421.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 391.8);
      ctx.lineTo(964.6, 421.5);
      ctx.lineTo(1016.6, 451.2);
      ctx.lineTo(1016.6, 391.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 480.9);
      ctx.lineTo(1016.6, 510.6);
      ctx.lineTo(964.6, 540.4);
      ctx.lineTo(964.6, 480.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 451.2);
      ctx.lineTo(964.6, 480.9);
      ctx.lineTo(1016.6, 510.6);
      ctx.lineTo(1016.6, 451.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 540.4);
      ctx.lineTo(1016.6, 570.1);
      ctx.lineTo(964.6, 599.8);
      ctx.lineTo(964.6, 540.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 510.6);
      ctx.lineTo(964.6, 540.4);
      ctx.lineTo(1016.6, 570.1);
      ctx.lineTo(1016.6, 510.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 599.8);
      ctx.lineTo(1016.6, 629.5);
      ctx.lineTo(964.6, 659.2);
      ctx.lineTo(964.6, 599.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(243, 244, 246)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(1016.6, 570.1);
      ctx.lineTo(964.6, 599.8);
      ctx.lineTo(1016.6, 629.5);
      ctx.lineTo(1016.6, 570.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(965.2, 655.5);
      ctx.lineTo(913.2, 685.2);
      ctx.lineTo(965.2, 714.9);
      ctx.lineTo(965.2, 655.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 659.2);
      ctx.lineTo(1016.6, 688.9);
      ctx.lineTo(964.6, 718.7);
      ctx.lineTo(964.6, 659.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 629.5);
      ctx.lineTo(964.6, 659.2);
      ctx.lineTo(1016.6, 688.9);
      ctx.lineTo(1016.6, 629.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(964.6, 124.3);
      ctx.lineTo(1016.6, 154.0);
      ctx.lineTo(964.6, 183.8);
      ctx.lineTo(964.6, 124.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(1015.9, 95.2);
      ctx.lineTo(963.9, 124.9);
      ctx.lineTo(1015.9, 154.7);
      ctx.lineTo(1015.9, 95.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1016.6, 154.0);
      ctx.lineTo(964.6, 183.8);
      ctx.lineTo(1016.6, 213.5);
      ctx.lineTo(1016.6, 154.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 688.9);
      ctx.lineTo(964.6, 718.7);
      ctx.lineTo(1016.6, 748.4);
      ctx.lineTo(1016.6, 688.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 183.8);
      ctx.lineTo(1016.6, 213.5);
      ctx.lineTo(1068.6, 243.2);
      ctx.lineTo(1068.6, 183.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 243.2);
      ctx.lineTo(1016.6, 272.9);
      ctx.lineTo(1068.6, 302.6);
      ctx.lineTo(1068.6, 243.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 213.5);
      ctx.lineTo(1068.6, 243.2);
      ctx.lineTo(1016.6, 272.9);
      ctx.lineTo(1016.6, 213.5);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 302.6);
      ctx.lineTo(1016.6, 332.3);
      ctx.lineTo(1068.6, 362.1);
      ctx.lineTo(1068.6, 302.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 272.9);
      ctx.lineTo(1068.6, 302.6);
      ctx.lineTo(1016.6, 332.3);
      ctx.lineTo(1016.6, 272.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 362.1);
      ctx.lineTo(1016.6, 391.8);
      ctx.lineTo(1068.6, 421.5);
      ctx.lineTo(1068.6, 362.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 332.3);
      ctx.lineTo(1068.6, 362.1);
      ctx.lineTo(1016.6, 391.8);
      ctx.lineTo(1016.6, 332.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 421.5);
      ctx.lineTo(1016.6, 451.2);
      ctx.lineTo(1068.6, 480.9);
      ctx.lineTo(1068.6, 421.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 391.8);
      ctx.lineTo(1068.6, 421.5);
      ctx.lineTo(1016.6, 451.2);
      ctx.lineTo(1016.6, 391.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 480.9);
      ctx.lineTo(1016.6, 510.6);
      ctx.lineTo(1068.6, 540.4);
      ctx.lineTo(1068.6, 480.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1016.6, 451.2);
      ctx.lineTo(1068.6, 480.9);
      ctx.lineTo(1016.6, 510.6);
      ctx.lineTo(1016.6, 451.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1070.0, 5.5);
      ctx.lineTo(1018.0, 35.2);
      ctx.lineTo(1070.0, 64.9);
      ctx.lineTo(1070.0, 5.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 64.9);
      ctx.lineTo(1016.6, 94.6);
      ctx.lineTo(1068.6, 124.3);
      ctx.lineTo(1068.6, 64.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 124.3);
      ctx.lineTo(1016.6, 154.0);
      ctx.lineTo(1068.6, 183.8);
      ctx.lineTo(1068.6, 124.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 94.6);
      ctx.lineTo(1068.6, 124.3);
      ctx.lineTo(1016.6, 154.0);
      ctx.lineTo(1016.6, 94.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 540.4);
      ctx.lineTo(1016.6, 570.1);
      ctx.lineTo(1068.6, 599.8);
      ctx.lineTo(1068.6, 540.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 510.6);
      ctx.lineTo(1068.6, 540.4);
      ctx.lineTo(1016.6, 570.1);
      ctx.lineTo(1016.6, 510.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 599.8);
      ctx.lineTo(1016.6, 629.5);
      ctx.lineTo(1068.6, 659.2);
      ctx.lineTo(1068.6, 599.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(1016.6, 570.1);
      ctx.lineTo(1068.6, 599.8);
      ctx.lineTo(1016.6, 629.5);
      ctx.lineTo(1016.6, 570.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 659.2);
      ctx.lineTo(1016.6, 688.9);
      ctx.lineTo(1068.6, 718.7);
      ctx.lineTo(1068.6, 659.2);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(1016.6, 629.5);
      ctx.lineTo(1068.6, 659.2);
      ctx.lineTo(1016.6, 688.9);
      ctx.lineTo(1016.6, 629.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1068.6, 718.7);
      ctx.lineTo(1016.6, 748.4);
      ctx.lineTo(1068.6, 778.1);
      ctx.lineTo(1068.6, 718.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(1016.6, 688.9);
      ctx.lineTo(1068.6, 718.7);
      ctx.lineTo(1016.6, 748.4);
      ctx.lineTo(1016.6, 688.9);
      ctx.closePath();
      ctx.fill();

      // layer1/Group
      ctx.restore();

      // layer1/Group/Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(48.6, 186.5);
      ctx.lineTo(0.7, 216.3);
      ctx.lineTo(48.6, 246.1);
      ctx.lineTo(48.6, 186.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(236, 235, 240)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 246.1);
      ctx.lineTo(0.7, 275.9);
      ctx.lineTo(48.6, 305.7);
      ctx.lineTo(48.6, 246.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(0.7, 216.3);
      ctx.lineTo(48.6, 246.1);
      ctx.lineTo(0.7, 275.9);
      ctx.lineTo(0.7, 216.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 305.7);
      ctx.lineTo(0.7, 335.4);
      ctx.lineTo(48.6, 365.2);
      ctx.lineTo(48.6, 305.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(0.7, 275.9);
      ctx.lineTo(48.6, 305.7);
      ctx.lineTo(0.7, 335.4);
      ctx.lineTo(0.7, 275.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 365.2);
      ctx.lineTo(0.7, 395.0);
      ctx.lineTo(48.6, 424.8);
      ctx.lineTo(48.6, 365.2);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(4.0, 335.4);
      ctx.lineTo(51.8, 365.2);
      ctx.lineTo(4.0, 395.0);
      ctx.lineTo(4.0, 335.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 424.8);
      ctx.lineTo(0.7, 454.6);
      ctx.lineTo(48.6, 484.4);
      ctx.lineTo(48.6, 424.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(0.7, 395.0);
      ctx.lineTo(48.6, 424.8);
      ctx.lineTo(0.7, 454.6);
      ctx.lineTo(0.7, 395.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(48.6, 7.8);
      ctx.lineTo(0.7, 37.6);
      ctx.lineTo(48.6, 67.4);
      ctx.lineTo(48.6, 7.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 67.4);
      ctx.lineTo(0.7, 97.2);
      ctx.lineTo(48.6, 127.0);
      ctx.lineTo(48.6, 67.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(0.7, 37.6);
      ctx.lineTo(48.6, 67.4);
      ctx.lineTo(0.7, 97.2);
      ctx.lineTo(0.7, 37.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(48.6, 127.0);
      ctx.lineTo(0.7, 156.8);
      ctx.lineTo(48.6, 186.5);
      ctx.lineTo(48.6, 127.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(0.7, 97.2);
      ctx.lineTo(48.6, 127.0);
      ctx.lineTo(0.7, 156.8);
      ctx.lineTo(0.7, 97.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(0.7, 156.8);
      ctx.lineTo(48.6, 186.5);
      ctx.lineTo(0.7, 216.3);
      ctx.lineTo(0.7, 156.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(48.6, 484.4);
      ctx.lineTo(0.7, 514.1);
      ctx.lineTo(48.6, 543.9);
      ctx.lineTo(48.6, 484.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(4.0, 454.6);
      ctx.lineTo(51.8, 484.4);
      ctx.lineTo(4.0, 514.1);
      ctx.lineTo(4.0, 454.6);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 543.9);
      ctx.lineTo(0.7, 573.7);
      ctx.lineTo(48.6, 603.5);
      ctx.lineTo(48.6, 543.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(0.7, 514.1);
      ctx.lineTo(48.6, 543.9);
      ctx.lineTo(0.7, 573.7);
      ctx.lineTo(0.7, 514.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(48.6, 603.5);
      ctx.lineTo(0.7, 633.3);
      ctx.lineTo(48.6, 663.1);
      ctx.lineTo(48.6, 603.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(0.7, 573.7);
      ctx.lineTo(48.6, 603.5);
      ctx.lineTo(0.7, 633.3);
      ctx.lineTo(0.7, 573.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(48.6, 663.1);
      ctx.lineTo(0.7, 692.8);
      ctx.lineTo(48.6, 722.6);
      ctx.lineTo(48.6, 663.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(4.5, 634.5);
      ctx.lineTo(52.3, 664.3);
      ctx.lineTo(4.5, 694.1);
      ctx.lineTo(4.5, 634.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 722.6);
      ctx.lineTo(0.7, 752.4);
      ctx.lineTo(48.6, 782.2);
      ctx.lineTo(48.6, 722.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(0.7, 692.8);
      ctx.lineTo(48.6, 722.6);
      ctx.lineTo(0.7, 752.4);
      ctx.lineTo(0.7, 692.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 186.5);
      ctx.lineTo(96.4, 216.3);
      ctx.lineTo(144.2, 246.1);
      ctx.lineTo(144.2, 186.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(245, 246, 246)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 246.1);
      ctx.lineTo(96.4, 275.9);
      ctx.lineTo(144.2, 305.7);
      ctx.lineTo(144.2, 246.1);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 216.3);
      ctx.lineTo(144.2, 246.1);
      ctx.lineTo(96.4, 275.9);
      ctx.lineTo(96.4, 216.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 305.7);
      ctx.lineTo(96.4, 335.4);
      ctx.lineTo(144.2, 365.2);
      ctx.lineTo(144.2, 305.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(245, 246, 246)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 275.9);
      ctx.lineTo(144.2, 305.7);
      ctx.lineTo(96.4, 335.4);
      ctx.lineTo(96.4, 275.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 365.2);
      ctx.lineTo(96.4, 395.0);
      ctx.lineTo(144.2, 424.8);
      ctx.lineTo(144.2, 365.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 335.4);
      ctx.lineTo(144.2, 365.2);
      ctx.lineTo(96.4, 395.0);
      ctx.lineTo(96.4, 335.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 424.8);
      ctx.lineTo(96.4, 454.6);
      ctx.lineTo(144.2, 484.4);
      ctx.lineTo(144.2, 424.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 395.0);
      ctx.lineTo(144.2, 424.8);
      ctx.lineTo(96.4, 454.6);
      ctx.lineTo(96.4, 395.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 484.4);
      ctx.lineTo(96.4, 514.1);
      ctx.lineTo(144.2, 543.9);
      ctx.lineTo(144.2, 484.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 454.6);
      ctx.lineTo(144.2, 484.4);
      ctx.lineTo(96.4, 514.1);
      ctx.lineTo(96.4, 454.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 543.9);
      ctx.lineTo(96.4, 573.7);
      ctx.lineTo(144.2, 603.5);
      ctx.lineTo(144.2, 543.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 514.1);
      ctx.lineTo(144.2, 543.9);
      ctx.lineTo(96.4, 573.7);
      ctx.lineTo(96.4, 514.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(144.2, 603.5);
      ctx.lineTo(96.4, 633.3);
      ctx.lineTo(144.2, 663.1);
      ctx.lineTo(144.2, 603.5);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(96.4, 573.7);
      ctx.lineTo(144.2, 603.5);
      ctx.lineTo(96.4, 633.3);
      ctx.lineTo(96.4, 573.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(144.2, 663.1);
      ctx.lineTo(96.4, 692.8);
      ctx.lineTo(144.2, 722.6);
      ctx.lineTo(144.2, 663.1);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 633.3);
      ctx.lineTo(144.2, 663.1);
      ctx.lineTo(96.4, 692.8);
      ctx.lineTo(96.4, 633.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(144.2, 127.0);
      ctx.lineTo(96.4, 156.8);
      ctx.lineTo(144.2, 186.5);
      ctx.lineTo(144.2, 127.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 97.2);
      ctx.lineTo(144.2, 127.0);
      ctx.lineTo(96.4, 156.8);
      ctx.lineTo(96.4, 97.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(96.4, 156.8);
      ctx.lineTo(144.2, 186.5);
      ctx.lineTo(96.4, 216.3);
      ctx.lineTo(96.4, 156.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(144.2, 722.6);
      ctx.lineTo(96.4, 752.4);
      ctx.lineTo(144.2, 782.2);
      ctx.lineTo(144.2, 722.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 692.8);
      ctx.lineTo(144.2, 722.6);
      ctx.lineTo(96.4, 752.4);
      ctx.lineTo(96.4, 692.8);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(47.1, 187.7);
      ctx.lineTo(96.4, 215.2);
      ctx.lineTo(50.0, 247.2);
      ctx.lineTo(47.1, 187.7);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 246.1);
      ctx.lineTo(96.4, 275.9);
      ctx.lineTo(48.6, 305.7);
      ctx.lineTo(48.6, 246.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 216.3);
      ctx.lineTo(48.6, 246.1);
      ctx.lineTo(96.4, 275.9);
      ctx.lineTo(96.4, 216.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(48.6, 305.7);
      ctx.lineTo(96.4, 335.4);
      ctx.lineTo(48.6, 365.2);
      ctx.lineTo(48.6, 305.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(96.4, 275.9);
      ctx.lineTo(48.6, 305.7);
      ctx.lineTo(96.4, 335.4);
      ctx.lineTo(96.4, 275.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(48.6, 365.2);
      ctx.lineTo(96.4, 395.0);
      ctx.lineTo(48.6, 424.8);
      ctx.lineTo(48.6, 365.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(96.4, 335.4);
      ctx.lineTo(48.6, 365.2);
      ctx.lineTo(96.4, 395.0);
      ctx.lineTo(96.4, 335.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(207, 208, 209)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 424.8);
      ctx.lineTo(96.4, 454.6);
      ctx.lineTo(48.6, 484.4);
      ctx.lineTo(48.6, 424.8);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 395.0);
      ctx.lineTo(48.6, 424.8);
      ctx.lineTo(96.4, 454.6);
      ctx.lineTo(96.4, 395.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 484.4);
      ctx.lineTo(96.4, 514.1);
      ctx.lineTo(48.6, 543.9);
      ctx.lineTo(48.6, 484.4);
      ctx.closePath();
      ctx.fillStyle = "rgb(64, 72, 156)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(96.4, 454.6);
      ctx.lineTo(48.6, 484.4);
      ctx.lineTo(96.4, 514.1);
      ctx.lineTo(96.4, 454.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.20;
      ctx.beginPath();
      ctx.moveTo(48.6, 7.8);
      ctx.lineTo(96.4, 37.6);
      ctx.lineTo(48.6, 67.4);
      ctx.lineTo(48.6, 7.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(48.6, 67.4);
      ctx.lineTo(96.4, 97.2);
      ctx.lineTo(48.6, 127.0);
      ctx.lineTo(48.6, 67.4);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 37.6);
      ctx.lineTo(48.6, 67.4);
      ctx.lineTo(96.4, 97.2);
      ctx.lineTo(96.4, 37.6);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 127.0);
      ctx.lineTo(96.4, 156.8);
      ctx.lineTo(48.6, 186.5);
      ctx.lineTo(48.6, 127.0);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 97.2);
      ctx.lineTo(48.6, 127.0);
      ctx.lineTo(96.4, 156.8);
      ctx.lineTo(96.4, 97.2);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 543.9);
      ctx.lineTo(96.4, 573.7);
      ctx.lineTo(48.6, 603.5);
      ctx.lineTo(48.6, 543.9);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 514.1);
      ctx.lineTo(48.6, 543.9);
      ctx.lineTo(96.4, 573.7);
      ctx.lineTo(96.4, 514.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 603.5);
      ctx.lineTo(96.4, 633.3);
      ctx.lineTo(48.6, 663.1);
      ctx.lineTo(48.6, 603.5);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 573.7);
      ctx.lineTo(48.6, 603.5);
      ctx.lineTo(96.4, 633.3);
      ctx.lineTo(96.4, 573.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 0.50;
      ctx.beginPath();
      ctx.moveTo(48.6, 663.1);
      ctx.lineTo(96.4, 692.8);
      ctx.lineTo(48.6, 722.6);
      ctx.lineTo(48.6, 663.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(49, 186, 172)";
      ctx.fill();

      // layer1/Group/Path
      ctx.globalAlpha = alpha * 1.00;
      ctx.beginPath();
      ctx.moveTo(192.0, 663.1);
      ctx.lineTo(239.8, 692.8);
      ctx.lineTo(192.0, 722.6);
      ctx.lineTo(192.0, 663.1);
      ctx.closePath();
      ctx.fillStyle = "rgb(6, 179, 202)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 633.3);
      ctx.lineTo(48.6, 663.1);
      ctx.lineTo(96.4, 692.8);
      ctx.lineTo(96.4, 633.3);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(48.6, 722.6);
      ctx.lineTo(96.4, 752.4);
      ctx.lineTo(48.6, 782.2);
      ctx.lineTo(48.6, 722.6);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(96.4, 692.8);
      ctx.lineTo(48.6, 722.6);
      ctx.lineTo(96.4, 752.4);
      ctx.lineTo(96.4, 692.8);
      ctx.closePath();
      ctx.fill();

      // layer1/Group/Path
      ctx.beginPath();
      ctx.moveTo(192.0, 692.8);
      ctx.lineTo(144.2, 722.6);
      ctx.lineTo(192.0, 752.4);
      ctx.lineTo(192.0, 692.8);
      ctx.closePath();
      ctx.fillStyle = "rgb(227, 228, 229)";
      ctx.fill();

      // layer1/Awarded On 5 July 2018
      ctx.restore();
      ctx.font = "17.8px 'Poppins'";
      ctx.fillStyle = "rgb(88, 88, 91)";
      ctx.fillText("Awarded On: " + dateissued, 243.0, 650.1);

      // layer1/Group

      // layer1/Group/CERTIFICATE
      ctx.save();
      ctx.font = "38.6px 'Poppins Medium'";
      ctx.textAlign = 'center'; 
      ctx.fillText("CERTIFICATE", 561.5, 283.6);

      // layer1/Group/Group

      // layer1/Group/Group/OF COMPLETION
      ctx.save();
      ctx.font = "26.6px 'Poppins Regular'";
      ctx.textAlign = 'center'; 
      ctx.fillText("OF COMPLETION", 561.5, 321.5);

      // layer1/This Certificate Is Awarded ToAnnie Wanjalafor successfully completingCourse Name
      ctx.restore();
      ctx.restore();
      ctx.textAlign = 'center'; 
      ctx.fillText("This Certificate Is Awarded To",561.5, 401.6);
      ctx.font = "32.3px 'Poppins Medium'";
      ctx.fillText(owner, 561.5, 438.4);
      ctx.font = "17.8px 'Poppins'";
      ctx.fillText("for successfully completing", 561.5, 487.3);
      ctx.font = "39.9px 'Poppins Medium'";
      ctx.fillText(certcourse, 561.5, 536.3);

      // layer1/Image
      ctx.drawImage(document.getElementById("image1"), 706.9, 547.2);

      // layer1/Path
      ctx.beginPath();
      ctx.setLineDash([5, 5]);
      ctx.moveTo(226.1, 348.4);
      ctx.lineTo(831.4, 348.4);
      // This artwork uses an unsupported dash style
      ctx.strokeStyle = "rgb(185, 187, 189)";
      ctx.stroke();

      // layer1/Group

      // layer1/Group/Group
      ctx.save();

      // layer1/Group/Group/Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(504.1, 56.6);
      ctx.lineTo(502.3, 53.8);
      ctx.lineTo(502.5, 50.1);
      ctx.bezierCurveTo(502.5, 50.1, 504.4, 47.0, 504.5, 47.1);
      ctx.bezierCurveTo(504.6, 47.2, 509.6, 43.4, 509.6, 43.4);
      ctx.lineTo(514.8, 42.5);
      ctx.lineTo(520.3, 44.5);
      ctx.lineTo(523.8, 48.4);
      ctx.lineTo(524.6, 51.5);
      ctx.lineTo(523.6, 54.2);
      ctx.lineTo(522.7, 54.3);
      ctx.lineTo(521.7, 56.0);
      ctx.lineTo(520.2, 56.2);
      ctx.lineTo(519.7, 56.3);
      ctx.lineTo(519.2, 57.2);
      ctx.lineTo(520.0, 58.4);
      ctx.lineTo(520.5, 58.9);
      ctx.lineTo(518.9, 59.6);
      ctx.lineTo(514.3, 56.5);
      ctx.lineTo(512.6, 55.9);
      ctx.lineTo(511.0, 57.3);
      ctx.lineTo(508.1, 57.1);
      ctx.lineTo(504.6, 57.0);
      ctx.lineTo(504.1, 56.6);
      ctx.closePath();
      ctx.lineWidth = 0.1;
      ctx.strokeStyle = "rgb(255, 255, 255)";
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(502.7, 49.9);
      ctx.lineTo(506.0, 50.7);
      ctx.lineTo(508.6, 53.5);
      ctx.lineTo(511.0, 57.3);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(509.6, 43.4);
      ctx.lineTo(512.6, 48.2);
      ctx.lineTo(514.8, 50.8);
      ctx.lineTo(518.8, 53.4);
      ctx.lineTo(521.7, 56.0);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(514.8, 42.5);
      ctx.lineTo(516.0, 45.5);
      ctx.lineTo(519.0, 49.9);
      ctx.lineTo(523.6, 54.0);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(502.3, 53.8);
      ctx.lineTo(506.0, 50.7);
      ctx.lineTo(508.5, 47.3);
      ctx.lineTo(509.7, 43.4);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(502.4, 53.7);
      ctx.lineTo(510.6, 57.2);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(523.9, 49.0);
      ctx.lineTo(509.7, 43.4);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(508.1, 57.1);
      ctx.lineTo(504.6, 47.1);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(504.3, 56.9);
      ctx.lineTo(506.0, 50.7);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(508.8, 53.8);
      ctx.lineTo(510.4, 50.0);
      ctx.lineTo(512.6, 48.2);
      ctx.lineTo(515.5, 45.7);
      ctx.lineTo(516.6, 45.3);
      ctx.lineTo(520.3, 44.5);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(512.2, 56.2);
      ctx.lineTo(508.6, 53.5);
      ctx.lineTo(513.8, 53.4);
      ctx.lineTo(514.9, 50.9);
      ctx.lineTo(516.0, 45.5);
      ctx.lineTo(520.2, 44.6);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(512.1, 56.3);
      ctx.lineTo(513.8, 53.4);
      ctx.lineTo(514.1, 56.2);
      ctx.lineTo(516.4, 55.7);
      ctx.lineTo(518.9, 53.7);
      ctx.lineTo(524.6, 51.5);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(517.0, 58.2);
      ctx.lineTo(515.1, 50.9);
      ctx.lineTo(523.5, 48.9);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(520.3, 44.5);
      ctx.lineTo(519.1, 49.9);
      ctx.lineTo(518.9, 53.7);
      ctx.lineTo(519.7, 56.3);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(522.7, 54.3);
      ctx.lineTo(519.3, 53.5);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(508.5, 47.3);
      ctx.lineTo(512.6, 48.2);
      ctx.lineTo(511.5, 46.3);
      ctx.lineTo(508.5, 47.3);
      ctx.closePath();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(515.9, 45.5);
      ctx.lineTo(511.5, 46.3);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(518.9, 59.6);
      ctx.lineTo(519.2, 57.2);
      ctx.lineTo(516.4, 55.7);
      ctx.lineTo(510.4, 50.0);
      ctx.lineTo(508.5, 47.3);
      ctx.lineTo(504.5, 47.1);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(517.0, 58.2);
      ctx.lineTo(519.2, 57.2);
      ctx.lineTo(519.0, 58.6);
      ctx.lineTo(517.0, 58.2);
      ctx.closePath();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(520.5, 58.9);
      ctx.lineTo(519.0, 58.6);
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(523.2, 48.8);
      ctx.lineTo(524.0, 49.1);
      ctx.lineTo(523.0, 52.1);
      ctx.lineTo(521.9, 52.5);
      ctx.lineTo(519.1, 49.9);
      ctx.lineTo(522.9, 51.1);
      ctx.lineTo(524.5, 51.5);
      ctx.stroke();

      // layer1/Group/Group
      ctx.restore();

      // layer1/Group/Group/Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(504.6, 56.7);
      ctx.bezierCurveTo(504.6, 56.9, 504.5, 57.1, 504.3, 57.2);
      ctx.bezierCurveTo(504.1, 57.2, 503.9, 57.1, 503.8, 56.8);
      ctx.bezierCurveTo(503.8, 56.6, 503.9, 56.4, 504.2, 56.4);
      ctx.bezierCurveTo(504.4, 56.3, 504.6, 56.5, 504.6, 56.7);
      ctx.closePath();
      ctx.fillStyle = "rgb(255, 255, 255)";
      ctx.fill();
      ctx.lineWidth = 0.1;
      ctx.strokeStyle = "rgb(255, 255, 255)";
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(508.6, 57.1);
      ctx.bezierCurveTo(508.6, 57.3, 508.5, 57.5, 508.2, 57.5);
      ctx.bezierCurveTo(508.0, 57.6, 507.8, 57.4, 507.8, 57.2);
      ctx.bezierCurveTo(507.8, 57.0, 507.9, 56.8, 508.1, 56.7);
      ctx.bezierCurveTo(508.3, 56.7, 508.5, 56.9, 508.6, 57.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(511.2, 57.1);
      ctx.bezierCurveTo(511.3, 57.3, 511.1, 57.5, 510.9, 57.6);
      ctx.bezierCurveTo(510.7, 57.6, 510.5, 57.5, 510.5, 57.2);
      ctx.bezierCurveTo(510.4, 57.0, 510.6, 56.8, 510.8, 56.8);
      ctx.bezierCurveTo(511.0, 56.8, 511.2, 56.9, 511.2, 57.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(512.6, 56.1);
      ctx.bezierCurveTo(512.7, 56.3, 512.5, 56.5, 512.3, 56.6);
      ctx.bezierCurveTo(512.1, 56.6, 511.9, 56.5, 511.9, 56.3);
      ctx.bezierCurveTo(511.8, 56.0, 512.0, 55.8, 512.2, 55.8);
      ctx.bezierCurveTo(512.4, 55.8, 512.6, 55.9, 512.6, 56.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(514.5, 56.1);
      ctx.bezierCurveTo(514.5, 56.3, 514.4, 56.5, 514.2, 56.6);
      ctx.bezierCurveTo(514.0, 56.6, 513.8, 56.5, 513.7, 56.2);
      ctx.bezierCurveTo(513.7, 56.0, 513.8, 55.8, 514.1, 55.8);
      ctx.bezierCurveTo(514.3, 55.8, 514.5, 55.9, 514.5, 56.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(517.5, 58.0);
      ctx.bezierCurveTo(517.5, 58.2, 517.4, 58.4, 517.2, 58.5);
      ctx.bezierCurveTo(517.0, 58.5, 516.8, 58.4, 516.7, 58.2);
      ctx.bezierCurveTo(516.7, 57.9, 516.8, 57.7, 517.0, 57.7);
      ctx.bezierCurveTo(517.3, 57.7, 517.5, 57.8, 517.5, 58.0);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(519.1, 59.6);
      ctx.bezierCurveTo(519.2, 59.8, 519.0, 60.0, 518.8, 60.1);
      ctx.bezierCurveTo(518.6, 60.1, 518.4, 60.0, 518.4, 59.7);
      ctx.bezierCurveTo(518.3, 59.5, 518.5, 59.3, 518.7, 59.3);
      ctx.bezierCurveTo(518.9, 59.2, 519.1, 59.4, 519.1, 59.6);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(520.8, 58.8);
      ctx.bezierCurveTo(520.8, 59.1, 520.7, 59.3, 520.5, 59.3);
      ctx.bezierCurveTo(520.3, 59.3, 520.0, 59.2, 520.0, 59.0);
      ctx.bezierCurveTo(520.0, 58.8, 520.1, 58.5, 520.3, 58.5);
      ctx.bezierCurveTo(520.6, 58.5, 520.8, 58.6, 520.8, 58.8);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(519.5, 57.1);
      ctx.bezierCurveTo(519.6, 57.3, 519.4, 57.5, 519.2, 57.6);
      ctx.bezierCurveTo(519.0, 57.6, 518.8, 57.5, 518.7, 57.3);
      ctx.bezierCurveTo(518.7, 57.0, 518.8, 56.8, 519.1, 56.8);
      ctx.bezierCurveTo(519.3, 56.8, 519.5, 56.9, 519.5, 57.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(522.2, 55.9);
      ctx.bezierCurveTo(522.2, 56.1, 522.1, 56.3, 521.9, 56.3);
      ctx.bezierCurveTo(521.6, 56.4, 521.4, 56.2, 521.4, 56.0);
      ctx.bezierCurveTo(521.4, 55.8, 521.5, 55.6, 521.7, 55.6);
      ctx.bezierCurveTo(521.9, 55.5, 522.1, 55.7, 522.2, 55.9);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(523.9, 54.0);
      ctx.bezierCurveTo(523.9, 54.2, 523.8, 54.4, 523.6, 54.5);
      ctx.bezierCurveTo(523.4, 54.5, 523.2, 54.3, 523.1, 54.1);
      ctx.bezierCurveTo(523.1, 53.9, 523.2, 53.7, 523.5, 53.7);
      ctx.bezierCurveTo(523.7, 53.6, 523.9, 53.8, 523.9, 54.0);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(524.9, 51.4);
      ctx.bezierCurveTo(525.0, 51.6, 524.8, 51.8, 524.6, 51.8);
      ctx.bezierCurveTo(524.4, 51.9, 524.2, 51.7, 524.2, 51.5);
      ctx.bezierCurveTo(524.1, 51.3, 524.3, 51.1, 524.5, 51.1);
      ctx.bezierCurveTo(524.7, 51.0, 524.9, 51.2, 524.9, 51.4);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(524.2, 48.8);
      ctx.bezierCurveTo(524.3, 49.0, 524.1, 49.2, 523.9, 49.2);
      ctx.bezierCurveTo(523.7, 49.3, 523.5, 49.1, 523.5, 48.9);
      ctx.bezierCurveTo(523.4, 48.7, 523.6, 48.5, 523.8, 48.4);
      ctx.bezierCurveTo(524.0, 48.4, 524.2, 48.5, 524.2, 48.8);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(520.8, 44.5);
      ctx.bezierCurveTo(520.8, 44.7, 520.7, 44.9, 520.4, 44.9);
      ctx.bezierCurveTo(520.2, 45.0, 520.0, 44.8, 520.0, 44.6);
      ctx.bezierCurveTo(519.9, 44.4, 520.1, 44.2, 520.3, 44.1);
      ctx.bezierCurveTo(520.5, 44.1, 520.7, 44.3, 520.8, 44.5);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(515.2, 42.4);
      ctx.bezierCurveTo(515.3, 42.6, 515.1, 42.8, 514.9, 42.8);
      ctx.bezierCurveTo(514.7, 42.9, 514.5, 42.7, 514.4, 42.5);
      ctx.bezierCurveTo(514.4, 42.3, 514.5, 42.1, 514.8, 42.0);
      ctx.bezierCurveTo(515.0, 42.0, 515.2, 42.2, 515.2, 42.4);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(510.1, 43.2);
      ctx.bezierCurveTo(510.1, 43.5, 510.0, 43.7, 509.8, 43.7);
      ctx.bezierCurveTo(509.6, 43.7, 509.4, 43.6, 509.3, 43.4);
      ctx.bezierCurveTo(509.3, 43.2, 509.4, 42.9, 509.6, 42.9);
      ctx.bezierCurveTo(509.9, 42.9, 510.1, 43.0, 510.1, 43.2);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(505.0, 47.0);
      ctx.bezierCurveTo(505.1, 47.2, 504.9, 47.4, 504.7, 47.4);
      ctx.bezierCurveTo(504.5, 47.5, 504.3, 47.3, 504.3, 47.1);
      ctx.bezierCurveTo(504.2, 46.9, 504.4, 46.7, 504.6, 46.6);
      ctx.bezierCurveTo(504.8, 46.6, 505.0, 46.8, 505.0, 47.0);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(503.2, 49.8);
      ctx.bezierCurveTo(503.2, 50.0, 503.1, 50.2, 502.9, 50.3);
      ctx.bezierCurveTo(502.6, 50.3, 502.4, 50.2, 502.4, 49.9);
      ctx.bezierCurveTo(502.4, 49.7, 502.5, 49.5, 502.7, 49.5);
      ctx.bezierCurveTo(502.9, 49.4, 503.1, 49.6, 503.2, 49.8);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(502.9, 53.7);
      ctx.bezierCurveTo(502.9, 53.9, 502.7, 54.1, 502.5, 54.1);
      ctx.bezierCurveTo(502.3, 54.2, 502.1, 54.0, 502.1, 53.8);
      ctx.bezierCurveTo(502.0, 53.6, 502.2, 53.4, 502.4, 53.4);
      ctx.bezierCurveTo(502.6, 53.3, 502.8, 53.5, 502.9, 53.7);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(509.1, 53.4);
      ctx.bezierCurveTo(509.1, 53.6, 508.9, 53.8, 508.7, 53.9);
      ctx.bezierCurveTo(508.5, 53.9, 508.3, 53.8, 508.3, 53.5);
      ctx.bezierCurveTo(508.2, 53.3, 508.4, 53.1, 508.6, 53.1);
      ctx.bezierCurveTo(508.8, 53.0, 509.0, 53.2, 509.1, 53.4);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(510.8, 49.9);
      ctx.bezierCurveTo(510.8, 50.1, 510.7, 50.4, 510.5, 50.4);
      ctx.bezierCurveTo(510.2, 50.4, 510.0, 50.3, 510.0, 50.1);
      ctx.bezierCurveTo(510.0, 49.8, 510.1, 49.6, 510.3, 49.6);
      ctx.bezierCurveTo(510.5, 49.6, 510.7, 49.7, 510.8, 49.9);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(513.0, 48.1);
      ctx.bezierCurveTo(513.0, 48.3, 512.9, 48.5, 512.7, 48.6);
      ctx.bezierCurveTo(512.5, 48.6, 512.3, 48.4, 512.2, 48.2);
      ctx.bezierCurveTo(512.2, 48.0, 512.3, 47.8, 512.5, 47.8);
      ctx.bezierCurveTo(512.8, 47.7, 513.0, 47.9, 513.0, 48.1);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(516.3, 45.5);
      ctx.bezierCurveTo(516.3, 45.7, 516.2, 45.9, 516.0, 45.9);
      ctx.bezierCurveTo(515.8, 46.0, 515.6, 45.8, 515.5, 45.6);
      ctx.bezierCurveTo(515.5, 45.4, 515.6, 45.2, 515.9, 45.2);
      ctx.bezierCurveTo(516.1, 45.1, 516.3, 45.3, 516.3, 45.5);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(520.0, 47.3);
      ctx.bezierCurveTo(520.1, 47.5, 519.9, 47.7, 519.7, 47.8);
      ctx.bezierCurveTo(519.5, 47.8, 519.3, 47.7, 519.2, 47.4);
      ctx.bezierCurveTo(519.2, 47.2, 519.3, 47.0, 519.6, 47.0);
      ctx.bezierCurveTo(519.8, 47.0, 520.0, 47.1, 520.0, 47.3);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(519.5, 49.9);
      ctx.bezierCurveTo(519.6, 50.1, 519.4, 50.3, 519.2, 50.3);
      ctx.bezierCurveTo(519.0, 50.3, 518.8, 50.2, 518.8, 50.0);
      ctx.bezierCurveTo(518.7, 49.8, 518.9, 49.6, 519.1, 49.5);
      ctx.bezierCurveTo(519.3, 49.5, 519.5, 49.6, 519.5, 49.9);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(519.4, 53.4);
      ctx.bezierCurveTo(519.5, 53.6, 519.3, 53.9, 519.1, 53.9);
      ctx.bezierCurveTo(518.9, 53.9, 518.7, 53.8, 518.6, 53.6);
      ctx.bezierCurveTo(518.6, 53.3, 518.8, 53.1, 519.0, 53.1);
      ctx.bezierCurveTo(519.2, 53.1, 519.4, 53.2, 519.4, 53.4);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(516.8, 55.7);
      ctx.bezierCurveTo(516.9, 55.9, 516.7, 56.1, 516.5, 56.1);
      ctx.bezierCurveTo(516.3, 56.2, 516.1, 56.0, 516.1, 55.8);
      ctx.bezierCurveTo(516.0, 55.6, 516.2, 55.4, 516.4, 55.3);
      ctx.bezierCurveTo(516.6, 55.3, 516.8, 55.4, 516.8, 55.7);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(511.8, 46.2);
      ctx.bezierCurveTo(511.9, 46.4, 511.7, 46.6, 511.5, 46.6);
      ctx.bezierCurveTo(511.3, 46.7, 511.1, 46.5, 511.1, 46.3);
      ctx.bezierCurveTo(511.0, 46.1, 511.2, 45.9, 511.4, 45.8);
      ctx.bezierCurveTo(511.6, 45.8, 511.8, 46.0, 511.8, 46.2);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(508.9, 47.3);
      ctx.bezierCurveTo(508.9, 47.5, 508.7, 47.7, 508.5, 47.8);
      ctx.bezierCurveTo(508.3, 47.8, 508.1, 47.7, 508.1, 47.5);
      ctx.bezierCurveTo(508.0, 47.2, 508.2, 47.0, 508.4, 47.0);
      ctx.bezierCurveTo(508.6, 47.0, 508.8, 47.1, 508.9, 47.3);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(506.3, 50.8);
      ctx.bezierCurveTo(506.3, 51.0, 506.2, 51.2, 506.0, 51.3);
      ctx.bezierCurveTo(505.8, 51.3, 505.6, 51.2, 505.5, 50.9);
      ctx.bezierCurveTo(505.5, 50.7, 505.6, 50.5, 505.8, 50.5);
      ctx.bezierCurveTo(506.1, 50.5, 506.3, 50.6, 506.3, 50.8);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(505.3, 54.7);
      ctx.bezierCurveTo(505.3, 54.9, 505.2, 55.2, 505.0, 55.2);
      ctx.bezierCurveTo(504.8, 55.2, 504.5, 55.1, 504.5, 54.9);
      ctx.bezierCurveTo(504.5, 54.6, 504.6, 54.4, 504.8, 54.4);
      ctx.bezierCurveTo(505.1, 54.4, 505.3, 54.5, 505.3, 54.7);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(515.4, 51.0);
      ctx.bezierCurveTo(515.4, 51.2, 515.3, 51.4, 515.1, 51.5);
      ctx.bezierCurveTo(514.9, 51.5, 514.7, 51.4, 514.6, 51.1);
      ctx.bezierCurveTo(514.6, 50.9, 514.7, 50.7, 514.9, 50.7);
      ctx.bezierCurveTo(515.2, 50.7, 515.4, 50.8, 515.4, 51.0);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(514.2, 53.3);
      ctx.bezierCurveTo(514.2, 53.5, 514.0, 53.7, 513.8, 53.7);
      ctx.bezierCurveTo(513.6, 53.8, 513.4, 53.6, 513.4, 53.4);
      ctx.bezierCurveTo(513.3, 53.2, 513.5, 53.0, 513.7, 52.9);
      ctx.bezierCurveTo(513.9, 52.9, 514.1, 53.1, 514.2, 53.3);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Group/Group/Path
      ctx.beginPath();
      ctx.moveTo(519.4, 58.5);
      ctx.bezierCurveTo(519.5, 58.7, 519.3, 58.9, 519.2, 58.9);
      ctx.bezierCurveTo(519.0, 58.9, 518.8, 58.8, 518.8, 58.6);
      ctx.bezierCurveTo(518.7, 58.4, 518.9, 58.3, 519.0, 58.2);
      ctx.bezierCurveTo(519.2, 58.2, 519.4, 58.3, 519.4, 58.5);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      // layer1/Certificate Id Numberda07ab93669e4c77b64550fc34c2134d
      ctx.restore();
      ctx.restore();
      ctx.font = "10.6px 'Poppins Medium'";
      ctx.fillStyle = "rgb(34, 30, 31)";
      ctx.fillText("Certificate Id Number:" + certid, 561.5, 767.1);

      // layer1/Group

      // layer1/Group/Clip Group
      ctx.save();

      // layer1/Group/Clip Group/Clipping Path
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(375.9, 54.9);
      ctx.lineTo(375.9, 174.6);
      ctx.lineTo(691.1, 174.6);
      ctx.lineTo(691.1, 54.9);
      ctx.lineTo(375.9, 54.9);
      ctx.closePath();
      ctx.clip();

      // layer1/Group/Clip Group/Image
      ctx.drawImage(document.getElementById("image2"), 375.9, 54.9);
      ctx.restore();
      ctx.restore();
      ctx.restore();
    }